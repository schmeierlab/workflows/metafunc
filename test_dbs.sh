#!/bin/bash

## dbs download and make script: MetaFunc
## First, download and install miniconda and snakemake. We also recommend singularity be used.
## numbering here corresponds to the respective numbering in the README.MD of the gitlab repository, under `Usage with nr_euk`.

## Microbiome Analyses

#2.1 download provided Kaiju Database
wget https://zenodo.org/record/5602178/files/kaijudb_nreuk_202001.tar.bz2
tar xvjf kaijudb_nreuk_202001.tar.bz2 && rm kaijudb_nreuk_202001.tar.bz2

#2.1 download provided nrgo Database

wget https://zenodo.org/record/5602157/files/nrgo_202001_updated.tar.bz2
tar xvjf nrgo_202001_updated.tar.bz2 && rm nrgo_202001_updated.tar.bz2

##3 Host Analyses
#optional

# download host genome
wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/GRCh38.primary_assembly.genome.fa.gz
gzip -d GRCh38.primary_assembly.genome.fa.gz

# download corresponding gene annotation
wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/gencode.v33.primary_assembly.annotation.gtf.gz
gzip -d gencode.v33.primary_assembly.annotation.gtf.gz

# build STAR index
conda create --yes -n star star=2.7.3a
conda activate star
mkdir genome
STAR --runMode genomeGenerate --runThreadN 12 --genomeDir genome --genomeFastaFiles GRCh38.primary_assembly.genome.fa
conda deactivate
