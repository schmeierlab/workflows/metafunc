resultdir: analyses

# Specify sample sheet here
# Expected are 5 columns:
# 1) Sample Name: Name of sample (required)
# 2) file_mate1: If paired-end, forward read
# 3) file_mate2: If paired-end, reverse read
# 4) file_singles: If single-end, OR if trimming/quality control of reads gave Paired-end and singles
# NOTE: Either columns 2 and 3, or column 4 is required (or all 3)
# 5) group: condition to be tested between/among samples (e.g. disease state)
samples: .test/samples.txt

# should trimming be performed (fastp)?
trimming:
    perform: True
    # any extra arguments submitted to fastp
    extra: ""

# If we perform mapping of raw reads to host genome
# the workflow enables the quantification of gene expression
# for this a genome index and a gtf annotation file needs to be provided
# if groups are used and are specified in the samplesheet, differential
# gene expression of the host is performed (see contrasts in groups option below)
mapping:
    perform: True
    index: genome
    annotation: gencode.v33.primary_assembly.annotation.gtf
    # stranedness of the RNA-seq data, options are None, yes, reverse
    # None: unstranded, yes: stranded, reverse: stranded reversed
    # based on htseq-count --stranded: https://htseq.readthedocs.io/en/release_0.11.1/count.html#options
    strandedness: None
    # any extra arguments submitted to STAR
    star_extra: ""
    # gmt file for gene set enrichment analysis
    # is only performed if groups are used and thus dgea is performed.
    # if this is an empty string "" GSEA is not performed
    gsea_gmt: data/go.v7.0.ensembl.gmt
    # dgea is done (see contrasts in groups option below)
    # minimum count theshold to retain a gene for diff expr analysis
    # see edgeR filterbyExp
    dgea_mincount: 1
    # correlation between degs and species determined to be differentially abundant (DA) is also done
    # maximum fdr for a gene or species to be used for correlation analysis
    cor_pv: 0.05
    # use top X deg/DA for correlation analysis
    # this parameter can have huge influence on runtime as correlation analysis takes time.
    cor_top: 100

# Should results be grouped according to samplesheet group column
# If True, in addition to per sample results, the workflow will generate results per group
groups: 
    use: True
    # contrasts for the edgeR exactTest (both for dgea and DA for microbes is used if "[groups][use]: True"
    # the results will be obtained with regards to contrast 1 as baseline, e.g. positive logFC is up in contrast 2
    # contrasts listed here need to be present in the "group" column of the samplesheet: samples
    #   - contrast 1
    #   - contrast 2
    contrasts:
        normal-vs-coloncancer:
            - normal
            - coloncancer
    # minimum count theshold to retain a species for diff expr analysis
    # see edgeR filterbyExp
    DA_mincount: 1

# Databases that the workflow needs to run are specified here
# These need to be set up upfront (see Building Kaiju database in README.md or ...
# ... see instructions in https://gitlab.com/schmeierlab/nrgo?nav_source=navbar#building-kaiju-database)
# Path to the Kaiju nr_euk database
kaijuDB:
    dir: kaijudb_nreuk_202001
    fmi: kaijudb_nreuk_202001/nr_euk/kaiju_db_nr_euk.fmi

# Path to the KaijuNRGO database
# see here: https://gitlab.com/schmeierlab/nrgo?nav_source=navbar#building-kaiju-database
kaiju_NRGO: nrgo_202001_updated

KaijuRun:
    # Specify here non-default Kaiju parameters you want to add for Kaiju runs.
    # If empty, Kaiju is run with these defaults: -a greedy -e 3 -s 65 -x
    # -a greedy mode
    # -e is number mismatches in greedy mode,
    # -s minimum match score in greedy mode,
    # -x enables seg filtering
    # see Kaiju: https://github.com/bioinformatics-centre/kaiju/blob/master/README.md
    kaijuOptions: ""

# Some taxids are present in very low amounts giving rise to doubts if these are just artifacts
# A taxid is therefore excluded if it is less than the percent abundance given here in all samples analyzed
Filters:
    abundance: 0.001

TaxChoices:
    # Extract results according to these taxa.
    # Possible choices are every taxa name in https://raw.githubusercontent.com/bioinformatics-centre/kaiju/master/util/kaiju-taxonlistEuk.tsv
    #- Bacteria
    #- Archaea
    #- Fungi
    - Viruses
