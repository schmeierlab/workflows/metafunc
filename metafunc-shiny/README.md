### MetaFunc Shiny App Instructions

#### Usage Instructions:
The MetaFunc App is designed for interactive display and exploration of host and microbial abundance data, as well as gene ontology (GO) data and microbial taxonomy data generated from the MetaFunc Pipeline.
Full usage instructions with examples are located at: https://metafunc.readthedocs.io/en/latest/rshiny.html
Latest MetaFunc code is available at: https://gitlab.com/schmeierlab/workflows/metafunc/-/tree/master/metafunc-shiny

#### Home Page:

- Select which database to load and press the load database button. Databases are stored in the `/data/` folder of the application and when running MetaFunc are created and placed there automatically after being built by the buildSQLiteDB_sm.R script on results stored in the `/results/` folder of the MetaFunc pipeline.
- The app includes a demo database: `small_demo_dataset_with_groups.db`, this dataset is a small subset of results data from MetaFunc pipeline run on the colorectal cancer RNA-seq data used in the MetaFunc gut microbiome publication.
- In the demo dataset when the `Individual` toggle is selected columns beginning with 'SRR' denote samples from different patients, when the `Grouped` toggle is selected 'coloncancer' and 'normal' columns represent matched sample groupings.
- Full MetaFunc output for the demo dataset's original run available at: https://doi.org/10.1017/gmb.2022.12, and raw sequence reads at: https://www.ncbi.nlm.nih.gov/bioproject/PRJNA413956
- Use the Workflow Diagram below to familiarize yourself with the analysis steps of the MetaFunc Pipeline.


#### Microbiome Tab:

- Explore microbial abundance data (displayed in %) for each sample or group under the 'Abundances' sub-tab.
- Use the 'Gene Ontology' sub-tab to view GO terms by (% reads mapped to GO ID) for each sample or group, with options to filter by Biological Process, Cellular Component, or Molecular Function.
- In the 'GO to TaxIDs' sub-tab, investigate relationships between GO terms and microbial taxa by selecting GO terms in the top table to view the associated taxonomy data in the table below.
- In the 'TaxIDs to GO' sub-tab, similar to 'Go to TaxIDs', but reversed - investigate relationships between microbial taxa and GO terms by selecting microbes in the top table to view the associated GO data in the table below.

#### Host Tab:

- Examine host gene expression abundances TPM (Transcripts-Per-Million) data under the 'Abundances' sub-tab.
- Similar to the Microbiome tab, this tab allows filtering, sorting, and searching of human RNA-seq count data.

#### Individual vs. Grouped Data:

- The MetaFunc App can show both individual and grouped data in the Abundances, Gene Ontology, GO to TaxIDs, and TaxIDs to GO tabs. If grouped analysis was specified in the MetaFunc pipeline, switch the toggle to grouped to view your data.

#### GO and TaxID Data Inclusivity:

- In the GO to TaxIDs and TaxIDs to GO tabs, the user can toggle 'In Any' vs 'Only in All' to view associated terms (in the bottom table) that are either in any of the highlighted rows in the top table, or only present in all of the rows.

#### Exporting Data:

- After filtering or selecting data of interest, use the download options provided to export the tables in .csv or .excel formats for further analysis.

### Author Information:
If you use the MetaFunc pipeline or MetaFunc App in your analysis, please cite our [paper](https://doi.org/10.1017/gmb.2022.12):

`Sulit AK, Kolisnik T, Frizelle FA, Purcell R, Schmeier S. MetaFunc: taxonomic and functional analyses of high throughput sequencing for microbiomes. Gut Microbiome. 2023;4:e4. doi:10.1017/gmb.2022.12`
