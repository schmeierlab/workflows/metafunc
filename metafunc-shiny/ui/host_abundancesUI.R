# Host Abundances Tab User Interface
tabItem(tabName = "hostabundances",
        fluidRow(
          column(2,
                 actionButton('resetSelectedInputFields_hostabundances', label = "Reset Selected Input Fields")  # Reset Selected Input Fields
          ),
          column(9, offset = 0,
                 selectizeInput('selectColumn_hostabundances', 'Add or Remove Columns', 
                                choices =  names(load_host_abundances_colnames()), 
                                selected =  names(load_host_abundances_colnames()),
                                multiple = TRUE, width="1000px")
          ),
          
          box(
            textOutput("noselectedrowshostabundancesTable"),
            actionButton('resetSelection_hostabundancesTable', label = "Reset Table Selection"),  # Reset table selection button
            title = "Host Abundances Data in Transcripts-Per-Million (TPM)",
            withSpinner(DT::dataTableOutput("hostabundancesTable")),
            style = "height:900px; overflow-y: scroll;overflow-x: scroll;",
            width = 12
          )
        ),
        fluidRow(
          column(4,
                 downloadButton("downloadhostabundancesData", "Download Full Table")),
          column(4,
                 downloadButton("downloadSelectedhostabundancesData", "Download Selected")),
          column(4,
                 selectInput("downloadFormat_hostabundances", "Select File Format", 
                             choices = c("CSV" = "csv", "TSV" = "tsv", "Excel" = "xlsx")))
        )
)
