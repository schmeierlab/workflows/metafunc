# Counts Tab User Interface
tabItem(tabName = "counts",
        fluidRow(
          column(2,
                 radioButtons(inputId="countDataGroupedVsIndiv",
                              "Toggle Display of Individual vs Grouped Data", 
                              choices = c('Individual', 'Grouped')),
                 actionButton('resetSelectedInputFields', label = "Reset Selected Input Fields")  # Reset Selected Input Fields
          ),
          column(9, offset = 0,
                 conditionalPanel(
                   condition = "input.countDataGroupedVsIndiv == 'Individual'",
                   selectInput('selectColumn', 'Add or Remove Columns (Select and press delete or backspace to remove, or click to re-add)', 
                               choices = names(loadCountTabData_colnames()), 
                               selected = names(loadCountTabData_colnames()),
                               multiple = TRUE, width="1000px")),
                 conditionalPanel(
                   condition = "input.countDataGroupedVsIndiv == 'Grouped'",
                   selectInput('selectColumn2', 'Add or Remove Columns (Select and press delete or backspace to remove, or click to re-add)', 
                               choices = names(loadCountTabDataGrouped_colnames()),
                               selected = names(loadCountTabDataGrouped_colnames()),
                               multiple = TRUE, width="1000px"))
          ),
          
          box(
            textOutput("noselectedrowscountTable"),
            actionButton('resetSelection_countTable', label = "Reset Table Selection"),  # Reset table selection button
            title = "Microbial Abundances (% reads mapped to Tax ID)",
            withSpinner(DT::dataTableOutput("countTable")),
            #withSpinner(DT::dataTableOutput("countTable")),
            style = "height:900px; overflow-y: scroll;overflow-x: scroll;",
            width = 12
          )
        ),
        fluidRow(
          column(4,
                 downloadButton("downloadCountTabData", "Download Full Table")),
          column(4,
                 downloadButton("downloadSelectedCountTabData", "Download Selected")),
          column(4,
                 selectInput("downloadFormat", "Select File Format", 
                             choices = c("CSV" = "csv", "TSV" = "tsv", "Excel" = "xlsx")))
        )
)

