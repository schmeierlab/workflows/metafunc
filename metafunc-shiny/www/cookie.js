// JavaScript to handle setting and getting the database path from cookies

Shiny.addCustomMessageHandler("setDbToCookie", function(message) {
    var dbPath = message.dbPath;
    // Set a cookie that expires in 7 days
    Cookies.set('dbPath', dbPath, { expires: 7 });
});

Shiny.addCustomMessageHandler("getDbFromCookie", function(message) {
    var dbPath = Cookies.get('dbPath');
    Shiny.setInputValue('cookieDbPath', dbPath);
});