# Host Abundances Server Code

# Reactive expression for select input
debouncedSelectColumn_hostabundances <- reactive({
  input$selectColumn_hostabundances
}) %>% debounce(500)

# Main back-end code for the host_abundances tab
output$hostabundancesTable <- DT::renderDataTable(server=TRUE, {
  datc <- load_host_abundances()
  if ("Message" %in% names(datc)) {
    return(datc)  # Show message if table doesn't exist
  } else {
    datc <- datc[,debouncedSelectColumn_hostabundances(), drop = FALSE]
    return(datc)
  }
}, extensions = c('Buttons','Select'), 
options = datatableoptions(),
filter = 'top', rownames=F, escape=FALSE)

# Button for resetting table selection
tableProxy_hostabundancesTable <- dataTableProxy('hostabundancesTable')
observeEvent(input$resetSelection_hostabundancesTable, {
  DT::selectRows(tableProxy_hostabundancesTable, NULL)
})

# Button for resetting selected input fields
observeEvent(input$resetSelectedInputFields_hostabundances, {
  updateSelectInput(session, "selectColumn_hostabundances", selected = names(load_host_abundances_colnames()))
})

# Download full table
output$downloadhostabundancesData <- downloadHandler(
  filename = function() {
    file_extension <- switch(input$downloadFormat_hostabundances,
                             "csv" = "csv",
                             "tsv" = "tsv",
                             "xlsx" = "xlsx")
    paste0(generateTimestamp(), "-MetaFunc-HostAbundancesData.", file_extension)
  },
  content = function(con) {
    data <- load_host_abundances()
    if ("Message" %in% names(data)) {
      write.csv(data.frame(), con)  # Write an empty file if the table doesnt exist
    } else {
      if (input$downloadFormat_hostabundances == "csv") {
        write.csv(data, con)
      } else if (input$downloadFormat_hostabundances == "tsv") {
        write.table(data, con, sep = "\t", row.names = FALSE)
      } else if (input$downloadFormat_hostabundances == "xlsx") {
        write.xlsx(data, con)
      }
    }
  }
)

# Download selected rows
output$downloadSelectedhostabundancesData <- downloadHandler(
  filename = function() {
    file_extension <- switch(input$downloadFormat_hostabundances,
                             "csv" = "csv",
                             "tsv" = "tsv",
                             "xlsx" = "xlsx")
    paste0(generateTimestamp(), "-MetaFunc-SelectedHostAbundancesData.", file_extension)
  },
  content = function(con) {
    selectedRows <- input$hostabundancesTable_rows_selected
    data <- load_host_abundances()
    
    if ("Message" %in% names(data) || length(selectedRows) == 0) {
      write.csv(data.frame(), con)  # Write an empty file if no rws are selected or table doesnt exist
    } else {
      data <- data[selectedRows, debouncedSelectColumn_hostabundances(), drop = FALSE]
      
      if (input$downloadFormat_hostabundances == "csv") {
        write.csv(data, con)
      } else if (input$downloadFormat_hostabundances == "tsv") {
        write.table(data, con, sep = "\t", row.names = FALSE)
      } else if (input$downloadFormat_hostabundances == "xlsx") {
        write.xlsx(data, con)
      }
    }
  }
)

output$noselectedrowshostabundancesTable <- renderText({
  numberofselectedrows <- paste0("Number of Rows Selected: ", as.numeric(length(input$hostabundancesTable_rows_selected)))
  numberofselectedrows
})
