#Go2TaxServer Code

# Generate timestamp for the output file
generateTimestamp <- function() {
  format(Sys.time(), "%Y-%m-%d-%H-%M-%S")
}

# Reactive expression for select input in the top table
debouncedSelectColumn_go2tax <- reactive({
  input$goselectColumn_go2tax
}) %>% debounce(500) 

# Reactive expression for select input in the bottom table
debouncedSelectColumn2_go2tax <- reactive({
  input$goselectColumn2_go2tax
}) %>% debounce(500) 

# Reactive expression for select input in the bottom table (Count Data)
debouncedSelectColumn_goCount <- reactive({
  input$selectColumn_goCount
}) %>% debounce(500) 

# Reactive expression for select input in the bottom table (Grouped Data)
debouncedSelectColumn2_goCount <- reactive({
  input$selectColumn2_goCount
}) %>% debounce(500) 

# Reactive expression for row selection in the top table
debouncedSelectedRows <- reactive({
  input$GOTable_go2tax_rows_selected
}) %>% debounce(500) 

# Back-end code for the GO to TaxIDs tab (top table)
output$GOTable_go2tax <- DT::renderDataTable(server=TRUE, {
  gonamespaceval <- case_when(
    (input$bpccmf_go2tax == "Biological Process") ~ "BP",
    (input$bpccmf_go2tax == "Molecular Function") ~ "MF",
    (input$bpccmf_go2tax == "Cellular Component") ~ "CC"
  )
  
  if(input$gotabDataGroupedVsIndiv_go2tax == 'Individual'){
    datgoc <- loadGOData_withbpccmftoggle(gonamespaceval)
    datgoc <- datgoc[,debouncedSelectColumn_go2tax(), drop = FALSE]
  } else {
    datgoc <- loadGOgroupedData_withbpccmftoggle(gonamespaceval)
    datgoc <- datgoc[,debouncedSelectColumn2_go2tax(), drop = FALSE]
  }
  
  datgoc
}, extensions = c('Buttons','Select'), 
options = datatableoptions(),
filter = 'top', rownames=F, escape=FALSE)

# Back-end code for the GO to TaxIDs tab (bottom table)
output$GOCountTable_go2tax <- DT::renderDataTable(server=TRUE, {
  gonamespaceval <- case_when(
    (input$bpccmf_go2tax == "Biological Process") ~ "BP",
    (input$bpccmf_go2tax == "Molecular Function") ~ "MF",
    (input$bpccmf_go2tax == "Cellular Component") ~ "CC"
  )
  
  s <- debouncedSelectedRows()  # Use the debounced row selection
  
  if(input$gotabDataGroupedVsIndiv_go2tax == 'Individual'){
    allGOdata <- loadGOData_withbpccmftoggle(gonamespaceval)
    selectedGOData <- allGOdata[s,'GO_ID']
    
    maptable <- loadGO2taxsingle()
    maptableLong <- cSplit(maptable, "Associated_TaxIDs", sep=",", direction="long")
    associatedtaxids <- maptableLong %>% filter(GO_ID %in% selectedGOData)
    datg <- loadCountTabData()
    
    inclusivity <- case_when(
      (input$inclusivity_go2tax == "In Any") ~ "in_any",
      (input$inclusivity_go2tax == "Only In All") ~ "in_all"
    )
    
    if(inclusivity == "in_any"){
      associatedtaxids2 <- associatedtaxids %>% distinct(Associated_TaxIDs)
    } else {
      associatedtaxids$combined <- paste0(associatedtaxids$GO_ID,associatedtaxids$Associated_TaxIDs)
      asti <- associatedtaxids %>% distinct(combined, .keep_all = T)
      asti2 <- asti %>% group_by(Associated_TaxIDs) %>% filter(n() == length(selectedGOData))
      associatedtaxids2 <- asti2 %>% distinct(Associated_TaxIDs)
    }
    
    datg <- datg %>% filter(TaxID %in% associatedtaxids2$Associated_TaxIDs)
    datg <- datg[, debouncedSelectColumn_goCount(), drop = FALSE]  # Use the appropriate field selector
    datg
  } else {
    allGOdata <- loadGOgroupedData_withbpccmftoggle(gonamespaceval)
    selectedGOData <- allGOdata[s,'GO_ID']
    
    maptable <- loadGO2taxgrouped()
    maptableLong <- cSplit(maptable, "Associated_TaxIDs", sep=",", direction="long")
    associatedtaxids <- maptableLong %>% filter(GO_ID %in% selectedGOData)
    datg <- loadCountTabDataGrouped()
    
    inclusivity <- case_when(
      (input$inclusivity_go2tax == "In Any") ~ "in_any",
      (input$inclusivity_go2tax == "Only In All") ~ "in_all"
    )
    
    if(inclusivity == "in_any"){
      associatedtaxids2 <- associatedtaxids %>% distinct(Associated_TaxIDs)
    } else {
      associatedtaxids$combined <- paste0(associatedtaxids$GO_ID,associatedtaxids$Associated_TaxIDs)
      asti <- associatedtaxids %>% distinct(combined, .keep_all = T)
      asti2 <- asti %>% group_by(Associated_TaxIDs) %>% filter(n() == length(selectedGOData))
      associatedtaxids2 <- asti2 %>% distinct(Associated_TaxIDs)
    }
    
    datg <- datg %>% filter(TaxID %in% associatedtaxids2$Associated_TaxIDs)
    datg <- datg[, debouncedSelectColumn2_goCount(), drop = FALSE]  # Use the appropriate field selector
    datg
  }
}, extensions = c('Buttons','Select'), 
options = datatableoptions(),
filter = 'top', rownames=F, escape=FALSE)

# Button for resetting table selection (top table)
tableProxy_go2tax <- dataTableProxy('GOTable_go2tax')
observeEvent(input$resetSelection_go2tax, {
  DT::selectRows(tableProxy_go2tax, NULL)
})

# Button for resetting table selection (bottom table)
tableProxy_goCount <- dataTableProxy('GOCountTable_go2tax')
observeEvent(input$resetSelection_goCount, {
  DT::selectRows(tableProxy_goCount, NULL)
})

# Button for resetting selected input fields (top table)
observeEvent(input$resetSelectedInputFields_go2tax, {
  updateSelectInput(session, "goselectColumn_go2tax", selected = names(loadGOData_withbpccmftoggle_colnames()))  # Reset for individual
  updateSelectInput(session, "goselectColumn2_go2tax", selected = names(loadGOgroupedData_withbpccmftoggle_colnames()))  # Reset for grouped
})

# Button for resetting selected input fields (bottom table)
observeEvent(input$resetSelectedInputFields_goCount, {
  updateSelectInput(session, "selectColumn_goCount", selected = names(loadCountTabData_colnames()))  # Reset for individual
  updateSelectInput(session, "selectColumn2_goCount", selected = names(loadCountTabDataGrouped_colnames()))  # Reset for grouped
})

# Download full table (top table)
output$downloadGOTabData_go2tax <- downloadHandler(
  filename = function() {
    file_extension <- switch(input$downloadFormat_go2tax,
                             "csv" = "csv",
                             "tsv" = "tsv",
                             "xlsx" = "xlsx")
    paste0(generateTimestamp(), "-MetaFunc-GOTableData.", file_extension)
  },
  content = function(con) {
    gonamespaceval <- case_when(
      (input$bpccmf_go2tax == "Biological Process") ~ "BP",
      (input$bpccmf_go2tax == "Molecular Function") ~ "MF",
      (input$bpccmf_go2tax == "Cellular Component") ~ "CC"
    )
    
    data <- if(input$gotabDataGroupedVsIndiv_go2tax == 'Individual'){
      loadGOData_withbpccmftoggle(gonamespaceval)
    } else {
      loadGOgroupedData_withbpccmftoggle(gonamespaceval)
    }
    
    if (input$downloadFormat_go2tax == "csv") {
      write.csv(data, con, row.names = FALSE)
    } else if (input$downloadFormat_go2tax == "tsv") {
      write.table(data, con, sep = "\t", row.names = FALSE)
    } else if (input$downloadFormat_go2tax == "xlsx") {
      openxlsx::write.xlsx(data, con)
    }
  }
)

# Download selected rows (top table)
output$downloadSelectedGOTabData_go2tax <- downloadHandler(
  filename = function() {
    file_extension <- switch(input$downloadFormat_go2tax,
                             "csv" = "csv",
                             "tsv" = "tsv",
                             "xlsx" = "xlsx")
    paste0(generateTimestamp(), "-MetaFunc-SelectedGOTableData.", file_extension)
  },
  content = function(con) {
    selectedRows <- debouncedSelectedRows()  # Use the debounced row selection
    gonamespaceval <- case_when(
      (input$bpccmf_go2tax == "Biological Process") ~ "BP",
      (input$bpccmf_go2tax == "Molecular Function") ~ "MF",
      (input$bpccmf_go2tax == "Cellular Component") ~ "CC"
    )
    
    if (length(selectedRows) > 0) {
      data <- if(input$gotabDataGroupedVsIndiv_go2tax == 'Individual'){
        loadGOData_withbpccmftoggle(gonamespaceval)[selectedRows, debouncedSelectColumn_go2tax(), drop = FALSE]
      } else {
        loadGOgroupedData_withbpccmftoggle(gonamespaceval)[selectedRows, debouncedSelectColumn2_go2tax(), drop = FALSE]
      }
      
      if (input$downloadFormat_go2tax == "csv") {
        write.csv(data, con, row.names = FALSE)
      } else if (input$downloadFormat_go2tax == "tsv") {
        write.table(data, con, sep = "\t", row.names = FALSE)
      } else if (input$downloadFormat_go2tax == "xlsx") {
        openxlsx::write.xlsx(data, con)
      }
    } else {
      write.csv(data.frame(), con)  # If no rows are selected, export an empty data frame
    }
  }
)

# Download full table (bottom table)
output$downloadCountTabData_go2tax <- downloadHandler(
  filename = function() {
    file_extension <- switch(input$downloadFormat_goCount,
                             "csv" = "csv",
                             "tsv" = "tsv",
                             "xlsx" = "xlsx")
    paste0(generateTimestamp(), "-MetaFunc-CountTableData.", file_extension)
  },
  content = function(con) {
    gonamespaceval <- case_when(
      (input$bpccmf_go2tax == "Biological Process") ~ "BP",
      (input$bpccmf_go2tax == "Molecular Function") ~ "MF",
      (input$bpccmf_go2tax == "Cellular Component") ~ "CC"
    )
    
    data <- if(input$gotabDataGroupedVsIndiv_go2tax == 'Individual'){
      allGOdata <- loadGOData_withbpccmftoggle(gonamespaceval)
      selectedGOData <- allGOdata[debouncedSelectedRows(), 'GO_ID']
      datg <- loadCountTabData()
      maptable <- loadGO2taxsingle()
    } else {
      allGOdata <- loadGOgroupedData_withbpccmftoggle(gonamespaceval)
      selectedGOData <- allGOdata[debouncedSelectedRows(), 'GO_ID']
      datg <- loadCountTabDataGrouped()
      maptable <- loadGO2taxgrouped()
    }
    
    maptableLong <- cSplit(maptable, "Associated_TaxIDs", sep=",", direction="long")
    associatedtaxids <- maptableLong %>% filter(GO_ID %in% selectedGOData)
    
    inclusivity <- case_when(
      (input$inclusivity_go2tax == "In Any") ~ "in_any",
      (input$inclusivity_go2tax == "Only In All") ~ "in_all"
    )
    
    if(inclusivity == "in_any"){
      associatedtaxids2 <- associatedtaxids %>% distinct(Associated_TaxIDs)
    } else {
      associatedtaxids$combined <- paste0(associatedtaxids$GO_ID,associatedtaxids$Associated_TaxIDs)
      asti <- associatedtaxids %>% distinct(combined, .keep_all = T)
      asti2 <- asti %>% group_by(Associated_TaxIDs) %>% filter(n() == length(selectedGOData))
      associatedtaxids2 <- asti2 %>% distinct(Associated_TaxIDs)
    }
    
    data <- datg %>% filter(TaxID %in% associatedtaxids2$Associated_TaxIDs)
    data <- data[, debouncedSelectColumn_goCount(), drop = FALSE]  # Ensure correct columns are selected
    
    if (input$downloadFormat_goCount == "csv") {
      write.csv(data, con, row.names = FALSE)
    } else if (input$downloadFormat_goCount == "tsv") {
      write.table(data, con, sep = "\t", row.names = FALSE)
    } else if (input$downloadFormat_goCount == "xlsx") {
      openxlsx::write.xlsx(data, con)
    }
  }
)

# Download selected rows (bottom table)
output$downloadSelectedCountTabData_go2tax <- downloadHandler(
  filename = function() {
    file_extension <- switch(input$downloadFormat_goCount,
                             "csv" = "csv",
                             "tsv" = "tsv",
                             "xlsx" = "xlsx")
    paste0(generateTimestamp(), "-MetaFunc-SelectedCountTableData.", file_extension)
  },
  content = function(con) {
    selectedRows <- input$GOCountTable_go2tax_rows_selected
    gonamespaceval <- case_when(
      (input$bpccmf_go2tax == "Biological Process") ~ "BP",
      (input$bpccmf_go2tax == "Molecular Function") ~ "MF",
      (input$bpccmf_go2tax == "Cellular Component") ~ "CC"
    )
    
    if(input$gotabDataGroupedVsIndiv_go2tax == 'Individual'){
      allGOdata <- loadGOData_withbpccmftoggle(gonamespaceval)
      selectedGOData <- allGOdata[debouncedSelectedRows(), 'GO_ID']
      datg <- loadCountTabData()
      maptable <- loadGO2taxsingle()
    } else {
      allGOdata <- loadGOgroupedData_withbpccmftoggle(gonamespaceval)
      selectedGOData <- allGOdata[debouncedSelectedRows(), 'GO_ID']
      datg <- loadCountTabDataGrouped()
      maptable <- loadGO2taxgrouped()
    }
    
    maptableLong <- cSplit(maptable, "Associated_TaxIDs", sep=",", direction="long")
    associatedtaxids <- maptableLong %>% filter(GO_ID %in% selectedGOData)
    
    inclusivity <- case_when(
      (input$inclusivity_go2tax == "In Any") ~ "in_any",
      (input$inclusivity_go2tax == "Only In All") ~ "in_all"
    )
    
    if(inclusivity == "in_any"){
      associatedtaxids2 <- associatedtaxids %>% distinct(Associated_TaxIDs)
    } else {
      associatedtaxids$combined <- paste0(associatedtaxids$GO_ID,associatedtaxids$Associated_TaxIDs)
      asti <- associatedtaxids %>% distinct(combined, .keep_all = T)
      asti2 <- asti %>% group_by(Associated_TaxIDs) %>% filter(n() == length(selectedGOData))
      associatedtaxids2 <- asti2 %>% distinct(Associated_TaxIDs)
    }
    
    data <- datg %>% filter(TaxID %in% associatedtaxids2$Associated_TaxIDs)
    data <- data[selectedRows, debouncedSelectColumn_goCount(), drop = FALSE]
    
    if (input$downloadFormat_goCount == "csv") {
      write.csv(data, con, row.names = FALSE)
    } else if (input$downloadFormat_goCount == "tsv") {
      write.table(data, con, sep = "\t", row.names = FALSE)
    } else if (input$downloadFormat_goCount == "xlsx") {
      openxlsx::write.xlsx(data, con)
    }
  }
)

output$noselectedrowsGOTable_go2tax <- renderText({
  numberofselectedrows <- paste0("Number of Rows Selected: ", as.numeric(length(input$GOTable_go2tax_rows_selected)))
  numberofselectedrows
})

output$noselectedrowsGOCountTable_go2tax <- renderText({
  numberofselectedrows <- paste0("Number of Rows Selected: ", as.numeric(length(input$GOCountTable_go2tax_rows_selected)))
  numberofselectedrows
})
