#CountTable Server Code (for the Abundances Tab)

# Generate timestamp for output
generateTimestamp <- function() {
  format(Sys.time(), "%Y-%m-%d-%H-%M-%S")
}

# Debounced reactive expression for select input
debouncedSelectColumn <- reactive({
  input$selectColumn
}) %>% debounce(500) 

# Back-end code for the Abundances tab
output$countTable <- DT::renderDataTable(server=TRUE, {
  req(input$selectColumn)
  if(input$countDataGroupedVsIndiv == 'Individual'){
    datc <- loadCountTabData()
    datc <- datc[,debouncedSelectColumn()]
  } else {
    datc <- loadCountTabDataGrouped()
    datc <- datc[,input$selectColumn2]
  }
  datc
}, extensions = c('Buttons','Select'), 
options = datatableoptions(),
filter = 'top', rownames=F, escape=FALSE
)

# Button for resetting table selection
tableProxy_countTable <- dataTableProxy('countTable')
observeEvent(input$resetSelection_countTable, {
  DT::selectRows(tableProxy_countTable, NULL)  # Reset the row selection in the table
})

# Button for resetting selected input fields
observeEvent(input$resetSelectedInputFields, {
  updateSelectInput(session, "selectColumn", selected = names(loadCountTabData_colnames()))  # Reset for individual
  updateSelectInput(session, "selectColumn2", selected = names(loadCountTabDataGrouped_colnames()))  # Reset for grouped
})

# Download full table
output$downloadCountTabData <- downloadHandler(
  filename = function() {
    file_extension <- switch(input$downloadFormat,
                             "csv" = "csv",
                             "tsv" = "tsv",
                             "xlsx" = "xlsx")
    paste0(generateTimestamp(), "-MetaFunc-TableData.", file_extension)
  },
  content = function(con) {
    data <- if(input$countDataGroupedVsIndiv == 'Individual'){
      loadCountTabData()
    } else {
      loadCountTabDataGrouped()
    }
    
    if (input$downloadFormat == "csv") {
      write.csv(data, con)
    } else if (input$downloadFormat == "tsv") {
      write.table(data, con, sep = "\t", row.names = FALSE)
    } else if (input$downloadFormat == "xlsx") {
      write.xlsx(data, con)
    }
  }
)

# Download selected rows
output$downloadSelectedCountTabData <- downloadHandler(
  filename = function() {
    file_extension <- switch(input$downloadFormat,
                             "csv" = "csv",
                             "tsv" = "tsv",
                             "xlsx" = "xlsx")
    paste0(generateTimestamp(), "-MetaFunc-SelectedData.", file_extension)
  },
  content = function(con) {
    selectedRows <- input$countTable_rows_selected
    if (length(selectedRows) > 0) {
      data <- if(input$countDataGroupedVsIndiv == 'Individual'){
        loadCountTabData()[selectedRows, debouncedSelectColumn()]
      } else {
        loadCountTabDataGrouped()[selectedRows, input$selectColumn2]
      }
      
      if (input$downloadFormat == "csv") {
        write.csv(data, con)
      } else if (input$downloadFormat == "tsv") {
        write.table(data, con, sep = "\t", row.names = FALSE)
      } else if (input$downloadFormat == "xlsx") {
        write.xlsx(data, con)
      }
    } else {
      write.csv(data.frame(), con)  # If no rows are selected, export an empty data frame
    }
  }
)

output$noselectedrowscountTable <- renderText({
  numberofselectedrows <- paste0("Number of Rows Selected: ", as.numeric(length(input$countTable_rows_selected)))
  numberofselectedrows
})
