## =============================================================================
## WORKFLOW PROJECT: MetaFunc
## INIT DATE: 2019
import pandas as pd
import glob, os, os.path, datetime, sys, csv
from os.path import join, abspath
from snakemake.utils import validate, min_version

## If SettingwithCopyWarning is ever generated, raise error instead of warning; makes code more strict
pd.set_option("mode.chained_assignment", "raise")

## =============================================================================
## set minimum snakemake version #####
## =============================================================================
min_version("5.4.0")


## =============================================================================
## SETUP
## =============================================================================

## DEFAULT PATHS
DIR_SCRIPTS = abspath("scripts")
DIR_ENVS    = abspath("envs")
DIR_SCHEMAS = abspath("schemas")
DIR_SHINY = abspath("metafunc-shiny")

timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
db_file = join(DIR_SHINY, f"data/results_{timestamp}.db")

## LOAD VARIABLES FROM CONFIGFILE
## submit on command-line via --configfile
if config=={}:
    sys.stderr.write("Please submit config-file with --configfile <file>. Exit.\n")
    sys.exit(1)

## Validate configfile with yaml-schema
validate(config, schema=join(DIR_SCHEMAS, "config.schema.yaml"))

## define global Singularity image for reproducibility
## USE: "--use-singularity" to run all jobs in container
## without need to install any tools
singularity: "shub://sschmeier/metafunc-container:0.0.8"

## DATABASE PATHS
KaijuDatabase = config["kaijuDB"]["dir"]
KaijuDatabase_fmi = config["kaijuDB"]["fmi"]
if not os.path.isfile(KaijuDatabase_fmi):
    sys.stderr.write("{} has not been found. Please check your Kaiju database build\n".format(join(KaijuDatabase, KaijuDatabase_fmi)))
    sys.exit(1)

Kaiju_NRGO_DB = config["kaiju_NRGO"]

for kaijudbfile in ["nodes.dmp", "names.dmp"]:
    if not os.path.isfile(join(KaijuDatabase, kaijudbfile)):
        sys.stderr.write("{} has not been found. Please check your Kaiju database build\n".format(join(KaijuDatabase, kaijudbfile)))
        sys.exit(1)

for neededfile in ["go-basic.obo", "groups.pickle", "nrgo.sqlite", "strain2species.pickle", "tmp/kaiju-taxonlistEuk.tsv"]:
    if not os.path.isfile(join(Kaiju_NRGO_DB, neededfile)):
        sys.stderr.write("{} has not been found. Please check your Kaiju NR-GO database build\n".format(join(Kaiju_NRGO_DB, neededfile)))
        sys.exit(1)

# check strandedness parameter if we map
fc_stranded = None
if config["mapping"]["perform"]:
    if config["mapping"]["strandedness"] not in ["None", "yes", "reverse"]:
        sys.stderr.write('Unknown strandedness values in config. Unstranded libraries assumed.\n')
    if config["mapping"]["strandedness"] == "reverse":
        # for featureCount
        fc_stranded = "-s 2"
    elif config["mapping"]["strandedness"] == "yes":
        fc_stranded = "-s 1"
    else:
        fc_stranded = "-s 0"
    
## Setup result dirs
DIR_BASE       = config["resultdir"]
DIR_LOGS       = join(DIR_BASE, "logs")
DIR_BENCHMARKS = join(DIR_BASE, "benchmarks")
DIR_RES        = join(DIR_BASE, "results")

# check mapping index exists if mapping is required
if config["mapping"]["perform"] == True:
    if not os.path.isdir(config["mapping"]["index"]):
        sys.stderr.write("{} mapping index directory does not exist. Has an STAR index been build?\n".format(config["mapping"]["index"]))
        sys.exit(1)


## =============================================================================
## SAMPLES
## =============================================================================

SAMPLES = pd.read_csv(abspath(config["samples"]), sep="\t", keep_default_na=False, na_values="").set_index("sample", drop=False)
validate(SAMPLES, schema=join(DIR_SCHEMAS, "samples.schema.yaml"))

## reading samplename from samplesheet
sys.stderr.write("Reading samples from samplesheet: '{}' ...\n".format(config["samples"]))

## test if sample configuration is correct
for fname in SAMPLES["sample"]:

    fname1 = pd.notnull(SAMPLES.loc[fname, "file_mate1"])  # True or False
    fname2 = pd.notnull(SAMPLES.loc[fname, "file_mate2"]) # True or False
    fname3 = pd.notnull(SAMPLES.loc[fname, "file_singles"])  # True or False
    
    if not fname1 and not fname2:
        if not fname3:
            sys.stderr.write("No input files indicated for sample {}. Input files are necessary. Exit\n".format(fname))
            sys.exit(1)

    if fname1:
        if not os.path.isfile(SAMPLES.loc[fname, "file_mate1"]):
            sys.stderr.write("File '{}: file_mate1' from samplesheet can not be found. Make sure the file exists. Exit\n".format(fname))
            sys.exit(1)
        if not fname2:
            sys.stderr.write("For {} PE, please specify path to forward and reverse reads in columns 1 and 2, respectively\n".format(fname))
            sys.exit(1)
        else:
            if not os.path.isfile(SAMPLES.loc[fname, "file_mate2"]):
                sys.stderr.write("File '{}: file_mate2' from samplesheet can not be found. Make sure the file exists. Exit\n".format(fname))
                sys.exit(1)
    
    ## NOTE: fname1 and fname2 MUST always be present together if fname1 is true, see above and this checks if fname2 is present as well
    # if fname2 is the only 1 present, an error would be given
    if fname2:
        if not fname1:
            sys.stderr.write("For {} PE, please specify path to forward and reverse reads in columns 1 and 2, respectively".format(fname))
            sys.exit(1)
    
    if fname3:
        if not os.path.isfile(SAMPLES.loc[fname, "file_singles"]):
            sys.stderr.write("File '{}: file_singles' from samplesheet can not be found. Make sure the file exists. Exit\n".format(fname))
            sys.exit(1)

## Combinations of Input Read Files Passed Checks     
sys.stderr.write("Combinations of Input Read Files Passed Checks\n")

NUM_SAMPLES = len(SAMPLES["sample"])
sys.stderr.write("{} samples to process\n".format(NUM_SAMPLES))
sys.stderr.flush()


## =============================================================================
## WORKFLOW SETUP
## =============================================================================

## KaijuRun
KaijuOptions = config["KaijuRun"]["kaijuOptions"]

##TaxID Cutoffs
# required as number in schema
filters = config["Filters"]["abundance"]
#numsam = config["Filters"]["sample_pct"]

##Prot2GO
TaxonChoices = config["TaxChoices"]
# schema requires chosing from taxonlist.tsv
allTaxChoices = TaxonChoices + ["all"]

##Use Groups
grouping = config["groups"]["use"]

#if grouping:
#    groupparams = ""
#else:
#    groupparams = "--nogroup"


## =============================================================================
## SETUP INPUT TARGETS
## =============================================================================

## TaxID and Protein Tables
Protfiles = expand(join(DIR_RES, "microbiome/source/taxon/{sample}.taxid.tsv"), sample=SAMPLES["sample"]), expand(join(DIR_RES, "microbiome/source/protein/{sample}.protacc.tsv"), sample=SAMPLES["sample"])


## =============================================================================
## FUNCTIONS
## =============================================================================

def get_singles_raw(wildcards):
    return SAMPLES.loc[wildcards.sample, "file_singles"]


def get_fq_pe(wildcards):
    return [SAMPLES.loc[wildcards.sample, "file_mate1"], 
            SAMPLES.loc[wildcards.sample, "file_mate2"]]


def get_data_for_mapping_pe(wildcards):
    # either raw or trimmed
    if config["trimming"]["perform"]:
        return [join(DIR_RES, "samples/trimmed/pe/{}.1.fastq.gz".format(wildcards.sample)),
                join(DIR_RES, "samples/trimmed/pe/{}.2.fastq.gz".format(wildcards.sample))]
    else:
        return get_fq_pe(wildcards) 


def get_data_for_mapping_se(wildcards):
    # either raw or trimmed
    if config["trimming"]["perform"]:
        return join(DIR_RES, "samples/trimmed/se/{}.fastq.gz".format(wildcards.sample))
    else:
        return SAMPLES.loc[wildcards.sample, "file_singles"]


def get_kaiju_data_pe(wildcards):
    if config["mapping"]["perform"]:
        # mapped
        return [join(DIR_RES, "samples/map/pe/{}/Unmapped.out.mate1".format(wildcards.sample)),
                join(DIR_RES, "samples/map/pe/{}/Unmapped.out.mate2".format(wildcards.sample))]
    elif config["trimming"]["perform"]:
        # trimmed
        return [join(DIR_RES, "samples/trimmed/pe/{}.1.fastq.gz".format(wildcards.sample)),
                join(DIR_RES, "samples/trimmed/pe/{}.2.fastq.gz".format(wildcards.sample))]
    else:
        # raw sorted
        return get_fq_pe(wildcards) 
        

def get_kaiju_data_se(wildcards):
    if config["mapping"]["perform"]:
        # mapped
        return join(DIR_RES, "samples/map/se/{}/Unmapped.out.mate1".format(wildcards.sample))
    elif config["trimming"]["perform"]:
        # trimmed
        return join(DIR_RES, "samples/trimmed/se/{}.fastq.gz".format(wildcards.sample))
    else:
        # raw 
        return get_singles_raw(wildcards)


def get_data(wildcards):
    fq1 = pd.notnull(SAMPLES.loc[wildcards.sample, "file_mate1"])  # True or False
    fq2 = pd.notnull(SAMPLES.loc[wildcards.sample, "file_mate2"]) # True or False
    fq3 = pd.notnull(SAMPLES.loc[wildcards.sample, "file_singles"])  # True or False
    
    if fq1 and fq2 and fq3:
        data = [join(DIR_RES, "microbiome/source/classification/{}_se.kaijuOut".format(wildcards.sample)), 
                join(DIR_RES, "microbiome/source/classification/{}_pe.kaijuOut".format(wildcards.sample))]
    elif fq1 and fq2 and not fq3:
        data = join(DIR_RES, "microbiome/source/classification/{}_pe.kaijuOut".format(wildcards.sample))
    elif fq3 and not fq1 and not fq2:
        data = join(DIR_RES, "microbiome/source/classification/{}_se.kaijuOut".format(wildcards.sample))
    else:
        sys.stderr.write("Wrong samplesheet.")
        sys.exit(1)

    return data 
    

def get_data_fcounts(wildcards):
    """ Return the correct count files from featureCounts depending on combination of input sample files """
    fq1 = pd.notnull(SAMPLES.loc[wildcards.sample, "file_mate1"])  # True or False
    fq2 = pd.notnull(SAMPLES.loc[wildcards.sample, "file_mate2"]) # True or False
    fq3 = pd.notnull(SAMPLES.loc[wildcards.sample, "file_singles"])  # True or False
    if fq1 and fq2 and fq3:
        data = [join(DIR_RES, "host/expression/fc/{}.se.tsv".format(wildcards.sample)),
                join(DIR_RES, "host/expression/fc/{}.pe.tsv".format(wildcards.sample))]
    elif fq1 and fq2 and not fq3:
        data = join(DIR_RES, "host/expression/fc/{}.pe.tsv".format(wildcards.sample))
    elif fq3 and not fq1 and not fq2:
        data = join(DIR_RES, "host/expression/fc/{}.se.tsv".format(wildcards.sample))
    else:
        sys.stderr.write("Wrong samplesheet.")
        sys.exit(1)
    return data


def get_contrast(wildcards):
    return config["groups"]["contrasts"][wildcards.contrast]

## check that contrasts given are in group columns of sample sheet if groups are used 
if grouping:
    checkcon = []
    con = config["groups"]["contrasts"]
    for conts in con:
        checkcon += con[conts]
    for checking in checkcon:
        if checking not in list(set(SAMPLES["group"].tolist())):
            sys.stderr.write("Error: contrast stated in config file not in grouping specified in sample sheet\n")
            sys.exit(1)

    del(checkcon, con, conts, checking)

##get_shiny_input:
shinyIn = [join(DIR_RES, "microbiome/taxonomy/per_sample/all_sptable.tsv"),
               join(DIR_RES, "microbiome/taxonomy/per_sample/all_sptableScaled.tsv"),
               expand([join(DIR_RES, "microbiome/source/go/per_sample/{alltaxon}_samples"), 
               join(DIR_RES, "microbiome/function/per_sample/{alltaxon}_samples_goPercent.tsv")],
               alltaxon = allTaxChoices),  
               ] #join(DIR_RES, "host/expression/all.tpm.tsv")
if grouping:
    shinyIn += [join(DIR_RES, "microbiome/taxonomy/per_group/grouped_sptable_pct.tsv"), 
               expand([join(DIR_RES, "microbiome/source/go/per_group/{alltaxon}_grouped"),
               join(DIR_RES, "microbiome/function/per_group/{alltaxon}_grouped_goPercent.tsv")],alltaxon=allTaxChoices)
               ]
if config["mapping"]["perform"]:
    shinyIn += [join(DIR_RES, "host/expression/all.tpm.tsv")]


#expand(join(DIR_RES, "microbiome/taxonomy/per_group/DA/{contrast}/diffab.tsv.gz"), 
                #contrast = config["groups"]["contrasts"])
## =============================================================================
## RULES
## =============================================================================

#### TEST RUN ####
#rule all:
#    input: expand(join(DIR_RES, "{sample}.test.done"), sample=SAMPLES["sample"])
#rule test:
#    output:
#        touch("{sample}.test.done")
#### END TEST RUN ####

## Final TARGETS
TARGETS=[join(DIR_RES, "microbiome/taxonomy/per_sample/all_sptable.tsv"),
         join(DIR_RES, "microbiome/taxonomy/per_sample/all_sptableScaled.tsv"),
         join(DIR_RES, "microbiome/taxonomy/ALL/ALL_spTable_pct.tsv"),
         expand([join(DIR_RES, "microbiome/function/per_sample/{alltaxon}_samples_goPercent.tsv"),
         join(DIR_RES, "microbiome/source/go/ALL/{alltaxon}"), 
         join(DIR_RES, "microbiome/function/ALL/{alltaxon}_ALLasGroup_goPercent.tsv")], 
         alltaxon=allTaxChoices),
	 join(DIR_SHINY, "shiny.done")
         ]

if grouping:
    TARGETS += [join(DIR_RES, "microbiome/taxonomy/per_group/grouped_sptable_pct.tsv"), 
                expand(join(DIR_RES, "microbiome/function/per_group/{alltaxon}_grouped_goPercent.tsv"), alltaxon=allTaxChoices), 
                expand(join(DIR_RES, "microbiome/taxonomy/per_group/DA/{contrast}/diffab.tsv.gz"), 
                contrast = config["groups"]["contrasts"])
                ]

COR_PV = 0.05
COR_TOP = 100  
if config["mapping"]["perform"]:
    TARGETS += [join(DIR_RES, "host/expression/all.counts.tsv"), 
                join(DIR_RES, "host/expression/all.tpm.tsv")]
    if config["groups"]["use"]:  # we have groups to compare
        TARGETS += [expand([join(DIR_RES, "host/contrasts/{contrast}/diffexp.tsv.gz"),
                            join(DIR_RES, "host-microbiome/correlation/{contrast}/cor.deg-tax.matrix.link")],
                    contrast=config["groups"]["contrasts"])]
        if "cor_pv" in config["mapping"]:
            COR_PV = config["mapping"]["cor_pv"]
        if "cor_top" in config["mapping"]:
            COR_TOP = config["mapping"]["cor_top"]
            
        if config["mapping"]["gsea_gmt"] != "":
            TARGETS += [expand([join(DIR_RES, "host/contrasts/{contrast}/diffexp.gsea-up.tsv.gz"),
                                join(DIR_RES, "host/contrasts/{contrast}/diffexp.gsea-dn.tsv.gz")],
                        contrast=config["groups"]["contrasts"])]

## Pseudo-rule to state the final targets, so that the whole runs
rule all:
    input:
        TARGETS

rule trim_pe:
    input:
        fq = get_fq_pe
    output:
        trimmed1=temp(join(DIR_RES, "samples/trimmed/pe/{sample}.1.fastq.gz")), 
        trimmed2=temp(join(DIR_RES, "samples/trimmed/pe/{sample}.2.fastq.gz")),
        html=join(DIR_RES, "samples/trimmed/report/pe/{sample}.fastp.html"),
        json=join(DIR_RES, "samples/trimmed/report/pe/{sample}.fastp.json")
    log:
        join(DIR_LOGS, "samples/trimming/{sample}_pe.log")
    priority: 20
    benchmark:
        join(DIR_BENCHMARKS, "samples/trimming/{sample}_trim_pe.txt")
    params:
        extra=config["trimming"]["extra"]
    threads: 8
    conda:
        join(DIR_ENVS, "fastp.yaml")
    shell:
        "fastp --thread {threads} {params.extra} "
        "--in1 {input.fq[0]} --in2 {input.fq[1]} "
        "--out1 {output.trimmed1} --out2 {output.trimmed2} "
        "--html {output.html} --json {output.json} > {log} 2>&1"


rule trim_se:
    input:
        sample=get_singles_raw
    output:
        trimmed=temp(join(DIR_RES, "samples/trimmed/se/{sample}.fastq.gz")),
        html=join(DIR_RES, "samples/trimmed/report/se/{sample}.fastp.html"),
        json=join(DIR_RES, "samples/trimmed/report/se/{sample}.fastp.json")
    log:
        join(DIR_LOGS, "samples/trimming/{sample}_se.log")
    priority: 20
    benchmark:
        join(DIR_BENCHMARKS, "samples/trimming/{sample}_trim_se.txt")
    params:
        extra=config["trimming"]["extra"]
    threads: 8
    conda:
        join(DIR_ENVS, "fastp.yaml")
    shell:
        "fastp --thread {threads} {params.extra} --in1 {input.sample} "
        "--out1 {output.trimmed} "
        "--html {output.html} --json {output.json} > {log} 2>&1"


rule star_mapping_pe:
    input:
        sample=get_data_for_mapping_pe
    output:
        unmapped1=temp(join(DIR_RES, "samples/map/pe/{sample}/Unmapped.out.mate1")),
        unmapped2=temp(join(DIR_RES, "samples/map/pe/{sample}/Unmapped.out.mate2")),
        sam=temp(join(DIR_RES, "samples/map/pe/{sample}/Aligned.out.sam"))
    priority: 10
    log:
        join(DIR_LOGS, "samples/star_mapping/{sample}_pe.log")
    benchmark:
        join(DIR_BENCHMARKS, "samples/star_mapping/{sample}_pe.txt")
    conda:
        join(DIR_ENVS, "star.yaml")
    params:
        # path to STAR reference genome index
        index=config["mapping"]["index"],
        annotation=config["mapping"]["annotation"],
        extra=config["mapping"]["star_extra"]
    threads: 8
    wrapper:
        "file://scripts/wrapper/star"


rule star_mapping_se:
    input:
        sample=get_data_for_mapping_se
    output:
        unmapped=temp(join(DIR_RES, "samples/map/se/{sample}/Unmapped.out.mate1")),
        sam=temp(join(DIR_RES, "samples/map/se/{sample}/Aligned.out.sam"))
    priority: 10
    log:
        join(DIR_LOGS, "samples/star_mapping/{sample}_se.log")
    benchmark:
        join(DIR_BENCHMARKS, "samples/star_mapping/{sample}_se.txt")
    conda:
        join(DIR_ENVS, "star.yaml")
    params:
        # path to STAR reference genome index
        index=config["mapping"]["index"],
        annotation=config["mapping"]["annotation"],
        extra=config["mapping"]["star_extra"]
    threads: 8
    wrapper:
        "file://scripts/wrapper/star"

#-----------------
# HOST expression 
#-----------------

rule featurecount_se:
    input:
        join(DIR_RES, "samples/map/se/{sample}/Aligned.out.sam") 
    output:
        temp(join(DIR_RES, "host/expression/fc/{sample}.se.tsv"))
    conda:
        join(DIR_ENVS, "subread.yaml")
    log:
        join(DIR_LOGS, "host/expression/featurecount/featurecount_se_{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS, "host/featurecount/featurecount_se_{sample}.txt")
    threads: 8
    priority: 30
    params:
        anno=config["mapping"]["annotation"],
        stranded=fc_stranded
    shell:
        "featureCounts {params.stranded} -T {threads} "
        "-a {params.anno} "
        "-t exon -g gene_id "
        "-o {output[0]} {input} > {log} 2>&1"


rule featurecount_pe:
    input:
        join(DIR_RES, "samples/map/pe/{sample}/Aligned.out.sam") 
    output:
        temp(join(DIR_RES, "host/expression/fc/{sample}.pe.tsv"))
    conda:
        join(DIR_ENVS, "subread.yaml")
    log:
        join(DIR_LOGS, "host/expression/featurecount/featurecount_pe_{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS, "host/featurecount/featurecount_pe_{sample}.txt")
    threads: 8
    priority: 30
    params:
        anno=config["mapping"]["annotation"],
        stranded=fc_stranded
    shell:
        "featureCounts {params.stranded} -p -B -C -T {threads} "
        "-a {params.anno} "
        "-t exon -g gene_id "
        "-o {output[0]} {input} > {log} 2>&1"


rule combine_pe_se_counts:
    input:
        get_data_fcounts
    output:
        counts=temp(join(DIR_RES, "host/expression/{sample}.counts.txt")),
        tpm=temp(join(DIR_RES, "host/expression/{sample}.tpm.txt"))
    conda:
        join(DIR_ENVS, "Pandas.yaml")
    log:
        join(DIR_LOGS, "host/expression/featurecount/combine_pe_se_counts_{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS, "host/featurecount/combine_pe_se_counts_{sample}.txt")
    params:
        script=join(DIR_SCRIPTS, "combine_fcounts.py"),
        sample="{sample}"
    shell:
        "python {params.script} "
        "--counts {output.counts} --tpm {output.tpm} "
        "{params.sample} {input} > {log} 2>&1"

    
rule get_expression_count_table:
    input:
        expand(join(DIR_RES, "host/expression/{sample}.counts.txt"), sample=SAMPLES["sample"])
    output:
        join(DIR_RES, "host/expression/all.counts.tsv")
    benchmark:
        join(DIR_BENCHMARKS, "host/expression_tables/all_counts.txt")
    conda:
        join(DIR_ENVS, "Pandas.yaml")
    script:
        join(DIR_SCRIPTS, "count_matrix.py")


rule get_expression_tpm_table:
    input:
        expand(join(DIR_RES, "host/expression/{sample}.tpm.txt"), sample=SAMPLES["sample"])
    output:
        join(DIR_RES, "host/expression/all.tpm.tsv")
    benchmark:
        join(DIR_BENCHMARKS, "host/expression_tables/all_tpm.txt")
    conda:
        join(DIR_ENVS, "Pandas.yaml")
    script:
        join(DIR_SCRIPTS, "count_matrix.py")


rule subset_sample_group:
    input:
        config["samples"]
    output:
        temp(join(DIR_RES, "samples/groups/{contrast}/sample_groups.tsv"))
    benchmark:
        join(DIR_BENCHMARKS, "samples/groups/{contrast}_subset_sample.txt") 
    shell:
        "cat {input} | cut -f 1,5 > {output}"


rule dgea:
    input:
        counts=join(DIR_RES, "host/expression/all.counts.tsv"),
        samples=join(DIR_RES, "samples/groups/{contrast}/sample_groups.tsv")
    output:
        table=join(DIR_RES, "host/contrasts/{contrast}/diffexp.tsv.gz"),
        #ma_plot=join(DIR_RES, "samples/expression/comparisons/{contrast}/ma-plot.pdf")
    conda:
        join(DIR_ENVS, "edgeR.yaml")
    singularity:
        "shub://sschmeier/container-dgea"
    log:
        join(DIR_LOGS, "host/contrasts/{contrast}_diffexp.log")
    benchmark:
        join(DIR_BENCHMARKS, "host/contrasts/{contrast}_diffexp.txt")
    params:
        contrast=get_contrast,
        min_count=config["mapping"]["dgea_mincount"]
    script:
        join(DIR_SCRIPTS, "edgeR.R")


rule prepare_file_for_gsea:
    input:
        join(DIR_RES, "host/contrasts/{contrast}/diffexp.tsv.gz")
    output:
        temp(join(DIR_RES, "host/contrasts/{contrast}/diffexp.rnk"))
    benchmark:
        join(DIR_BENCHMARKS, "host/contrasts/{contrast}_gsea_prep.txt")
    conda:
        join(DIR_ENVS, "Pandas.yaml")
    script:
        join(DIR_SCRIPTS, "prepare_ranks.py")


rule gsea_gmt:
    input:
        join(DIR_RES, "host/contrasts/{contrast}/diffexp.rnk")
    output:
        join(DIR_RES, "host/contrasts/{contrast}/diffexp.gsea-up.tsv.gz"),
        join(DIR_RES, "host/contrasts/{contrast}/diffexp.gsea-dn.tsv.gz")
    log:
        join(DIR_LOGS, "host/contrasts/{contrast}_gsea.log")
    benchmark:
        join(DIR_BENCHMARKS, "host/contrasts/{contrast}_gsea.txt")
    conda:
        join(DIR_ENVS, "clusterprofiler.yaml")
    singularity:
        "shub://sschmeier/container-gsea"
    params:
        script=join(DIR_SCRIPTS, "gsea_msigdb_edgeR.R"),
        gmt=config["mapping"]["gsea_gmt"],
        nperms=10000
    shell:
        "cat {input} | Rscript --vanilla --slave {params.script} "
        "{params.gmt} {params.nperms} {output[0]} {output[1]} > {log} 2>&1"

#-----------------
# Microbiome
#-----------------

## Kaiju Run (actual Kaiju run)
rule kaijupe:
    input:
        data=get_kaiju_data_pe
    output:
        temp(join(DIR_RES, "microbiome/source/classification/{sample}_pe.kaijuOut"))
    params:
        nodes = join(KaijuDatabase, "nodes.dmp"),
        kaijufmi = KaijuDatabase_fmi,
        extra = KaijuOptions
    log:
        kaijuout = join(DIR_LOGS, "microbiome/source/classification/{sample}_kaijupe.out"),
        kaijuerr = join(DIR_LOGS, "microbiome/source/classification/{sample}_kaijupe.err")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/classification/{sample}_kaijupe.txt")
    conda:
        join(DIR_ENVS, "Kaiju.yaml")
    threads: 8
    priority: 20
    shell:
        "kaiju -z {threads} -t {params.nodes} -f {params.kaijufmi} -i {input.data[0]} -j {input.data[1]} {params.extra} -v -o {output} 1> {log.kaijuout} 2> {log.kaijuerr}"


rule kaijuse:
    input:
        get_kaiju_data_se
    output:
        temp(join(DIR_RES, "microbiome/source/classification/{sample}_se.kaijuOut"))
    params:
        nodes = join(KaijuDatabase, "nodes.dmp"),
        kaijufmi = KaijuDatabase_fmi,
        extra = KaijuOptions
    log:
        kaijuout = join(DIR_LOGS, "microbiome/source/classification/{sample}_kaijuse.out"),
        kaijuerr = join(DIR_LOGS, "microbiome/source/classification/{sample}_kaijuse.err")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/classification/{sample}_kaijuse.txt")
    conda:
        join(DIR_ENVS, "Kaiju.yaml")
    threads: 8
    priority: 20
    shell:
        "kaiju -z {threads} -t {params.nodes} -f {params.kaijufmi} -i {input} {params.extra} -v -o {output} 1> {log.kaijuout} 2> {log.kaijuerr}"


rule kaijucombine:
    input:
        get_data
    output:
        join(DIR_RES, "microbiome/source/classification/{sample}_final_kaiju.out")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/classification/{sample}_combinekaiju.txt")
    shell:
        "cat {input} > {output}"


## Extract TaxID and Protein Tables from Kaiju Results
rule metatables:
    input:
        join(DIR_RES, "microbiome/source/classification/{sample}_final_kaiju.out")
    output:
        outtax = join(DIR_RES, "microbiome/source/taxon/{sample}.taxid.tsv"),
        outprot = join(DIR_RES, "microbiome/source/protein/{sample}.protacc.tsv")
    params:
        summaryscript = join(DIR_SCRIPTS, "extract_metainfo.py"),
        taxconfig = join(Kaiju_NRGO_DB, "tmp/kaiju-taxonlistEuk.tsv"),
        taxdict = join(Kaiju_NRGO_DB, "groups.pickle"),
        str2sp = join(Kaiju_NRGO_DB, "strain2species.pickle"),
    log:
        sumout = join(DIR_LOGS, "microbiome/source/prottaxFiles/{sample}.prottax.out"),
        sumerr = join(DIR_LOGS, "microbiome/source/prottaxFiles/{sample}.prottax.err")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/prottax_files/{sample}.prottax.txt")
    conda:
        join(DIR_ENVS, "Pandas.yaml")
    shell:
        "python {params.summaryscript} --kaiju_file {input} --tax_config {params.taxconfig} --tax_dict {params.taxdict} --str2sp {params.str2sp} --taxFile {output.outtax} --protFile {output.outprot} 1> {log.sumout} 2> {log.sumerr}"


## Create Config File as input to TaxID 
rule createconfig:
    input: 
        Protfiles
    output: 
        orig = join(DIR_RES, "microbiome/.config/ProtConfig.tsv"),
        ALL = join(DIR_RES, "microbiome/.config/ProtConfigAll.tsv")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/config/createConfigInputs.txt")
    run:
        config1=open(output.orig, "w")
        config2=open(output.ALL, "w")
        header="SAMPLES\tPROTFILE\tTAXIDfile\tGROUP\n"
        config1.write(header)
        config2.write(header)
        for sam in SAMPLES["sample"]:
            #grp=SAMPLES.loc[sam, "group"]
            taxonFile=join(DIR_RES, "microbiome/source/taxon/{}.taxid.tsv".format(sam))
            proteinFile=join(DIR_RES, "microbiome/source/protein/{}.protacc.tsv".format(sam))
            rw = "{}\t{}\t{}\tnogroup\n".format(sam, proteinFile, taxonFile)
            otherrow="{}\t{}\t{}\tALL\n".format(sam, proteinFile, taxonFile)
            config1.write(rw)
            config2.write(otherrow)
        config1.close()
        config2.close()

rule createconfiggroup:
    input:
        Protfiles
    output: 
        orig = temp(join(DIR_RES, "microbiome/.config/group_protConfig.tsv")),
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/config/creategroup_ConfigInputs.txt")
    run:
        config1=open(output.orig, "w")
        header="SAMPLES\tPROTFILE\tTAXIDfile\tGROUP\n"
        config1.write(header)
        for sam in SAMPLES["sample"]:
            grp=SAMPLES.loc[sam, "group"]
            taxonFile=join(DIR_RES, "microbiome/source/taxon/{}.taxid.tsv".format(sam))
            proteinFile=join(DIR_RES, "microbiome/source/protein/{}.protacc.tsv".format(sam))
            rw = "{}\t{}\t{}\t{}\n".format(sam, proteinFile, taxonFile, grp)
            config1.write(rw)
        config1.close()


## Get Combined Table of TaxIDs
rule taxid:
    input: 
        join(DIR_RES, "microbiome/.config/ProtConfig.tsv")
    output:
        taxorig = temp(join(DIR_RES, "microbiome/taxonomy/noLineall_spTable.tsv")),
        taxscaled = temp(join(DIR_RES, "microbiome/taxonomy/noLineall_spTableScaled.tsv"))
    params:
        cutoffab = filters,
        #cutoffsam = numsam,
        taxscript = join(DIR_SCRIPTS, "taxid_sptables.py")
        #extra = groupparams
    log:
        taxlog=join(DIR_LOGS, "microbiome/taxonomy/all.tax.out"),
        taxerr=join(DIR_LOGS, "microbiome/taxonomy/all.tax.err")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/taxonomy/all.taxidTable.txt")
    conda:
        join(DIR_ENVS, "Pandas.yaml")
    shell:
        "python {params.taxscript} --TaxConfig {input} --cutoff {params.cutoffab} --minsam 1 --taxon all --nogroup --outfile {output.taxorig} --outfileSc {output.taxscaled} 1> {log.taxlog} 2> {log.taxerr}"

## Get temporary file with all ids from 5
rule filtered_ids:
    input: 
        join(DIR_RES, "microbiome/taxonomy/noLineall_spTable.tsv")
    output: 
        join(DIR_RES, "microbiome/.config/taxids.txt")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/config/create_ids.txt")
    run:
        outids=open(output[0], "w")
        taxfile=pd.read_csv(input[0], sep="\t", dtype=object).set_index("TaxID")
        ids=list(taxfile.index.values)
        for i in ids:
            row="{}\n".format(i)
            outids.write(row)

## Get lineage file based on 5.1
rule getlinefile:
    input:
        join(DIR_RES, "microbiome/.config/taxids.txt")
    output:
        temp(join(DIR_RES, "microbiome/.config/taxidsline.txt"))
    params:
        taxDirfile=KaijuDatabase
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/config/createtempline.txt")
    conda:
        join(DIR_ENVS, "TaxonKit.yaml")
    threads: 8
    shell:
        "cat {input} | taxonkit lineage --threads {threads} --data-dir {params.taxDirfile} | taxonkit reformat --threads {threads} --data-dir {params.taxDirfile} | cut -f 1,3 >> {output}"
    
## Get dataframe with lineage
rule getlinedf:
    input:
        origdf=join(DIR_RES, "microbiome/taxonomy/noLineall_spTable.tsv"),
        scaleddf=join(DIR_RES, "microbiome/taxonomy/noLineall_spTableScaled.tsv"),
        linefile=join(DIR_RES, "microbiome/.config/taxidsline.txt")
    output:
        outorig = join(DIR_RES, "microbiome/taxonomy/per_sample/all_sptable.tsv"),
        outscaled = join(DIR_RES, "microbiome/taxonomy/per_sample/all_sptableScaled.tsv")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/taxonomy/createlinedf.txt")
    run:
        linecsv = csv.reader(open(input.linefile, "r"), delimiter="\t")
        linedict = {"TaxID": [], "Kingdom": [], "Phylum": [], "Class": [], "Order": [], "Family": [], "Genus": []}
        for eachline in linecsv:
            wantline=[l.strip() for l in eachline[1].split(";")]
            linedict["TaxID"].append(eachline[0])
            linedict["Kingdom"].append(wantline[0])
            linedict["Phylum"].append(wantline[1])
            linedict["Class"].append(wantline[2])
            linedict["Order"].append(wantline[3])
            linedict["Family"].append(wantline[4])
            linedict["Genus"].append(wantline[5])

        linedf=pd.DataFrame.from_dict(linedict, dtype=object)
        df=pd.read_csv(input.origdf, sep="\t", dtype=object, keep_default_na=False)
        dfscaled=pd.read_csv(input.scaleddf, sep="\t", dtype=object, keep_default_na=False)

        dfline=pd.merge(linedf, df, on="TaxID", how="outer").set_index("TaxID").fillna("NA")
        scaledline=pd.merge(linedf, dfscaled, on="TaxID", how="outer").set_index("TaxID").fillna("NA")

        dfline=dfline.sort_values(by="Species")
        scaledline=scaledline.sort_values(by="Species")
        #dfindexing=["group"]
        #scaledidx=["group"]
        #exclude=["group", "Higher_Taxon_Levels", "Others", "Unclassified", "Total"]
        #for idx in dfline.index.values:
        #    if idx not in exclude:
        #        dfindexing.append(idx)
        #        scaledidx.append(idx)
        #for idx2 in exclude:
        #    if idx2 != "group":
        #        dfindexing.append(idx2)
        #scaledidx.append("Total")
                
        #dfline=dfline.reindex(dfindexing)
        #scaledline=scaledline.reindex(scaledidx)

        dfline.to_csv(output.outorig, sep="\t")
        scaledline.to_csv(output.outscaled, sep="\t")

## Add a per group scaled percent taxID table if grouping is wanted 
rule taxid_ALL:
    input: 
        join(DIR_RES, "microbiome/taxonomy/per_sample/all_sptableScaled.tsv")
    output:
        join(DIR_RES, "microbiome/taxonomy/ALL/ALL_spTable_pct.tsv")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/taxonomy/ALL_spTable_pct.txt")
    run:
        ##create group to sample dataframe
        groupdict = {"TaxID":"group", "Kingdom":"NA", "Phylum":"NA", "Class":"NA", "Order":"NA", "Family":"NA", "Genus":"NA", "Species":"NA", "RootTaxon":"NA"}
        for grpsam in SAMPLES["sample"]:
            if grpsam not in groupdict:
                groupdict[grpsam]=[]
                groupdict[grpsam].append("ALL")
            else:
                sys.stderr.write("Error: taxID_ALL rule. Non-unique samples detected\n")
                sys.exit(1)

        groupDF=pd.DataFrame.from_dict(groupdict, dtype=object).set_index("TaxID")
        idscaled = pd.read_csv(input[0], sep="\t", dtype=object, keep_default_na=False).set_index("TaxID")
        idscaled = pd.concat([groupDF, idscaled], sort=False)
        idline = idscaled.iloc[:, :8].copy()
        idline=idline.drop(["group"])
        dfsams=idscaled.iloc[:, 8:].copy()
        dfsams.columns=dfsams.iloc[0]
        dfsams=dfsams.drop(["group"])
        dfsams=dfsams.apply(pd.to_numeric)
        dfsams=dfsams.T.groupby(["group"]).agg(lambda x: x.mean()).T
        #col=list(dfsams.columns.values)
        #for c in col:
        #    dfsams[c + "_%"]=(dfsams[c]/dfsams[c].sum())*100
        #    dfsams[c + "_%"] = dfsams[c + "_%"].astype(object)
        #dfsams=dfsams.drop(columns=col)
        dfsams=dfsams.sort_index(axis=1)
        dfsams=dfsams.reset_index()
        grpscaled=pd.merge(idline, dfsams, on="TaxID", how="outer").set_index("TaxID")
        grpscaled=grpscaled.sort_values(by="Species")
        grpscaled.to_csv(output[0], sep="\t")

## Add a per group scaled percent taxID table if grouping is wanted 
rule taxid_group:
    input: 
        join(DIR_RES, "microbiome/taxonomy/per_sample/all_sptableScaled.tsv")
    output:
        join(DIR_RES, "microbiome/taxonomy/per_group/grouped_sptable_pct.tsv")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/taxonomy/groupedSpTable.txt")
    run:
        ##create group to sample dataframe

        groupdict = {"TaxID":"group", "Kingdom":"NA", "Phylum":"NA", "Class":"NA", "Order":"NA", "Family":"NA", "Genus":"NA", "Species":"NA", "RootTaxon":"NA"}
        for grpsam in SAMPLES["sample"]:
            samgrp = SAMPLES.loc[grpsam, "group"]
            if grpsam not in groupdict:
                groupdict[grpsam]=[]
                groupdict[grpsam].append(samgrp)
            else:
                sys.stderr.write("Error: taxID_group rule. Non-unique samples detected\n")
                sys.exit(1)

        groupDF=pd.DataFrame.from_dict(groupdict, dtype=object).set_index("TaxID")
        idscaled = pd.read_csv(input[0], sep="\t", dtype=object, keep_default_na=False).set_index("TaxID")
        idscaled = pd.concat([groupDF, idscaled], sort=False)
        idline = idscaled.iloc[:, :8].copy()
        idline=idline.drop(["group"])
        dfsams=idscaled.iloc[:, 8:].copy()
        dfsams.columns=dfsams.iloc[0]
        dfsams=dfsams.drop(["group"])
        dfsams=dfsams.apply(pd.to_numeric)
        dfsams=dfsams.T.groupby(["group"]).agg(lambda x: x.mean()).T
        #col=list(dfsams.columns.values)
        #for c in col:
        #    dfsams[c + "_%"]=(dfsams[c]/dfsams[c].sum())*100
        #    dfsams[c + "_%"] = dfsams[c + "_%"].astype(object)
        #dfsams=dfsams.drop(columns=col)
        dfsams=dfsams.sort_index(axis=1)
        dfsams=dfsams.reset_index()
        grpscaled=pd.merge(idline, dfsams, on="TaxID", how="outer").set_index("TaxID")
        grpscaled=grpscaled.sort_values(by="Species")
        grpscaled.to_csv(output[0], sep="\t")

rule tableDA:
    input:
        join(DIR_RES, "microbiome/taxonomy/per_sample/all_sptable.tsv")
    output:
        temp(join(DIR_RES, "microbiome/.config/{contrast}_spTableDA.tsv"))
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/config/{contrast}_spTableDA.txt")
    run:
        idDA = pd.read_csv(input[0], sep="\t", dtype=object, keep_default_na=False)
        idDA["Taxon"] = idDA["TaxID"] + "_" + idDA["Species"]
        idDA = idDA.iloc[:, 9:].copy()
        idDA = idDA.set_index("Taxon")
        idDA.to_csv(output[0], sep="\t")

rule DA:
    input:
        counts=join(DIR_RES, "microbiome/.config/{contrast}_spTableDA.tsv"),
        samples=join(DIR_RES, "samples/groups/{contrast}/sample_groups.tsv")
    output:
        table=join(DIR_RES, "microbiome/taxonomy/per_group/DA/{contrast}/diffab.tsv.gz"),
        #ma_plot=join(DIR_RES, "Meta_Func/TaxIDTables/Grouped/DA/{contrast}/ma-plot.pdf")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/DA/{contrast}_diffab.txt")
    conda:
        join(DIR_ENVS, "edgeR.yaml")
    singularity:
        "shub://sschmeier/container-dgea"
    log:
        join(DIR_LOGS, "microbiome/taxonomy/DA/{contrast}_diffab.log")
    params:
        contrast=get_contrast,
        min_count=config["groups"]["DA_mincount"]
    script:
        join(DIR_SCRIPTS, "edgeR.R")        
    

## Get GOs of the Protein files
rule protgo:
    input: 
        join(DIR_RES, "microbiome/.config/ProtConfig.tsv"), 
        join(DIR_RES, "microbiome/.config/taxids.txt")
    output:
        directory(join(DIR_RES, "microbiome/source/go/per_sample/{alltaxon}_samples"))
    params:
        accessGO = join(Kaiju_NRGO_DB, "nrgo.sqlite"),
        goObo = join(Kaiju_NRGO_DB, "go-basic.obo"),
        taxint = "{alltaxon}",
        GO_script = join(DIR_SCRIPTS, "prot2go.py"),
    log:
        GOout = join(DIR_LOGS, "microbiome/source/go/per_sample/{alltaxon}.samples.go.out"),
        GOerr = join(DIR_LOGS, "microbiome/source/go/per_sample/{alltaxon}.samples.go.err")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/protgo/{alltaxon}.samples.go.txt")
    conda:
        join(DIR_ENVS, "GOAtools.yaml")
    resources:
        const=1
    shell:
        "python {params.GO_script} --ProtConfig {input[0]} --cutoff {input[1]} --acc2go {params.accessGO} --goObo {params.goObo} --taxon {params.taxint} --prop --nogroup --outdir {output} 1> {log.GOout} 2> {log.GOerr}"

## If with grouping is wanted
rule protgogroup:
    input: 
        join(DIR_RES, "microbiome/.config/group_protConfig.tsv"), join(DIR_RES, "microbiome/.config/taxids.txt")
    output:
        directory(join(DIR_RES, "microbiome/source/go/per_group/{alltaxon}_grouped"))
    params:
        accessGO = join(Kaiju_NRGO_DB, "nrgo.sqlite"),
        goObo = join(Kaiju_NRGO_DB, "go-basic.obo"),
        taxint = "{alltaxon}",
        GO_script = join(DIR_SCRIPTS, "prot2go.py")
    log:
        GOout = join(DIR_LOGS, "microbiome/source/go/per_group/{alltaxon}.grouped.go.out"),
        GOerr = join(DIR_LOGS, "microbiome/source/go/per_group/{alltaxon}.grouped.go.err")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/protgogroup/{alltaxon}.group.go.txt")
    conda:
        join(DIR_ENVS, "GOAtools.yaml")
    resources:
        const=1
    shell:
        "python {params.GO_script} --ProtConfig {input[0]} --cutoff {input[1]} --acc2go {params.accessGO} --goObo {params.goObo} --taxon {params.taxint} --prop --outdir {output} 1> {log.GOout} 2> {log.GOerr}"


## Output is GO Files for all files as one group
rule protgoall:
    input: 
        join(DIR_RES, "microbiome/.config/ProtConfigAll.tsv"), join(DIR_RES, "microbiome/.config/taxids.txt")
    output:
        directory(join(DIR_RES, "microbiome/source/go/ALL/{alltaxon}"))
    params:
        accessGO = join(Kaiju_NRGO_DB, "nrgo.sqlite"),
        goObo = join(Kaiju_NRGO_DB, "go-basic.obo"),
        taxint = "{alltaxon}",
        GO_script = join(DIR_SCRIPTS, "prot2go.py")
    log:
        GOout = join(DIR_LOGS, "microbiome/source/go/ALL/{alltaxon}.all_as_grp.go.out"),
        GOerr = join(DIR_LOGS, "microbiome/source/go/ALL/{alltaxon}.all_as_grp.go.err")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/protgoall/{alltaxon}.all_as_grp.go.txt")
    conda:
        join(DIR_ENVS, "GOAtools.yaml")
    resources:
        const=1
    shell:
        "python {params.GO_script} --ProtConfig {input[0]} --cutoff {input[1]} --acc2go {params.accessGO} --goObo {params.goObo} --taxon {params.taxint} --prop --outdir {output} 1> {log.GOout} 2> {log.GOerr}"

## get summary GO percentages for NoGroup (individual)
rule gopercenttables:
    input:
        join(DIR_RES, "microbiome/source/go/per_sample/{alltaxon}_samples")
    output:
        join(DIR_RES, "microbiome/function/per_sample/{alltaxon}_samples_goPercent.tsv")
    params:
        join(DIR_RES, "microbiome/source/go/per_sample/{alltaxon}_samples/samples_{alltaxon}_prop.gofile.tsv")
    log:
        sumGOerr=join(DIR_LOGS, "microbiome/function/per_sample/{alltaxon}.goPercent_samples.err")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/protgo/{alltaxon}_perSample_summarizeGOs.txt")
    run:
        godf = pd.read_csv(params[0], sep="\t", dtype=object, keep_default_na=False)
        gosumdf=pd.DataFrame(columns=["GO_ID", "Description", "GO"])
        for gosam in sorted(list(godf["sample"].unique())):
            gosubdf=godf.loc[godf["sample"]==gosam].copy()
            gosubdf=gosubdf[["GO_ID", "Description", "GO", "sp_Percent"]]
            gosubdf.rename(columns={"sp_Percent":gosam}, inplace=True)
            gosumdf=gosumdf.merge(gosubdf, on=["GO_ID", "Description", "GO"], how="outer")
        gosumdf=gosumdf.fillna("0")
        gosumdf=gosumdf.set_index("GO_ID")
        ##to check if values for each GO_ID is unique:
        logfile = open(log.sumGOerr, "w")
        checkDesc={}
        checkNS={}
        for i in list(gosumdf.index.values):
            if i not in checkDesc:
                checkDesc[i]=[]
                checkNS[i]=[]
            checkDesc[i].append(gosumdf.at[i, "Description"])
            checkNS[i].append(gosumdf.at[i, "GO"])
        for key1 in checkDesc:
            if len(checkDesc[key1])!=1:
                logfile.write("Error: multiple descriptions for {}: {}\n".format(key1, ",".join(checkDesc[key1])))
                sys.exit(1)
        for key2 in checkNS:
            if len(checkNS[key2])!=1:
                logfile.write("Error: multiple namespaces for {}: {}\n".format(key2, ",".join(checkNS[key2])))
                sys.exit(1)
        logfile.close()
        gosumdf=gosumdf.sort_values(by="Description")
        gosumdf.to_csv(output[0], sep="\t")

## get summary GO percentages for ALL as group
rule ALL_gopercenttables:
    input:
        join(DIR_RES, "microbiome/source/go/ALL/{alltaxon}")
    output:
        join(DIR_RES, "microbiome/function/ALL/{alltaxon}_ALLasGroup_goPercent.tsv")
    params:
        join(DIR_RES, "microbiome/source/go/ALL/{alltaxon}/group_{alltaxon}_prop.gofile.tsv")
    log:
        sumGOerr=join(DIR_LOGS, "microbiome/function/ALL/{alltaxon}.goPercent_ALLasOne.err")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/protgo/{alltaxon}_ALLasOne_summarizeGOs.txt")
    run:
        godf = pd.read_csv(params[0], sep="\t", dtype=object, keep_default_na=False, usecols=['GO_ID', 'Description', 'GO', 'sp_Percent']).set_index('GO_ID')
        ##to check if values for each GO_ID is unique:
        logfile = open(log.sumGOerr, "w")
        if not godf.index.is_unique:
            logfile.write("Error: multiple GO IDs for All as One summary\n")
            sys.exit(1)
        logfile.close()

        godf.rename(columns={"sp_Percent":"ALL"}, inplace=True)
        godf.to_csv(output[0], sep="\t")

## if grouping, additional rule with changed input using grouped config files
rule group_gopercenttables:
    input:
        join(DIR_RES, "microbiome/source/go/per_group/{alltaxon}_grouped")
    output:
        join(DIR_RES, "microbiome/function/per_group/{alltaxon}_grouped_goPercent.tsv")
    params:
        #sumscript = join(DIR_SCRIPTS, "GO_PercentTables.py")
        join(DIR_RES, "microbiome/source/go/per_group/{alltaxon}_grouped/group_{alltaxon}_prop.gofile.tsv")
    log:
        #sumGOout=join(DIR_LOGS, "Meta_Func/GOSummaries/Grouped/{alltaxon}.GOPercentGrouped.out"),
        sumGOerr=join(DIR_LOGS, "microbiome/function/per_group/{alltaxon}.goPercent_grouped.err")
    benchmark:
        join(DIR_BENCHMARKS, "microbiome/protgogroup/{alltaxon}_grouped_summarizeGOs.txt")
    run:
        gogrpdf=pd.read_csv(params[0], sep="\t", dtype=object, keep_default_na=False)
        gogrpsumdf=pd.DataFrame(columns=["GO_ID", "Description", "GO"])
        for gogrp in sorted(list(gogrpdf["group"].unique())):
            gogrpsubdf=gogrpdf.loc[gogrpdf["group"]==gogrp].copy()
            gogrpsubdf=gogrpsubdf[["GO_ID", "Description", "GO", "sp_Percent"]]
            gogrpsubdf.rename(columns={"sp_Percent":gogrp}, inplace=True)
            gogrpsumdf=gogrpsumdf.merge(gogrpsubdf, on=["GO_ID", "Description", "GO"], how="outer")
        gogrpsumdf=gogrpsumdf.fillna("0")
        gogrpsumdf=gogrpsumdf.set_index("GO_ID")
        ##to check if values for each GO_ID is unique:
        grplogfile = open(log.sumGOerr, "w")
        checkgrpDesc={}
        checkgrpNS={}
        for grpi in list(gogrpsumdf.index.values):
            if grpi not in checkgrpDesc:
                checkgrpDesc[grpi]=[]
                checkgrpNS[grpi]=[]
            checkgrpDesc[grpi].append(gogrpsumdf.at[grpi, "Description"])
            checkgrpNS[grpi].append(gogrpsumdf.at[grpi, "GO"])
        for grpkey1 in checkgrpDesc:
            if len(checkgrpDesc[grpkey1])!=1:
                grplogfile.write("Error: multiple descriptions for {}: {}\n".format(grpkey1, ",".join(checkgrpDesc[grpkey1])))
                sys.exit(1)
        for grpkey2 in checkgrpNS:
            if len(checkgrpNS[grpkey2])!=1:
                logfile.write("Error: multiple namespaces for {}: {}\n".format(grpkey2, ",".join(checkgrpNS[grpkey2])))
                sys.exit(1)
        grplogfile.close()
        gogrpsumdf=gogrpsumdf.sort_values(by="Description")
        gogrpsumdf.to_csv(output[0], sep="\t")


#------------------------
# Correlation DEG vs TAX
#------------------------

rule cor:
    input:
        deg=join(DIR_RES, "host/contrasts/{contrast}/diffexp.tsv.gz"),
        tpm=join(DIR_RES, "host/expression/all.tpm.tsv"),
        da=join(DIR_RES, "microbiome/taxonomy/per_group/DA/{contrast}/diffab.tsv.gz"),
        tax=join(DIR_RES, "microbiome/taxonomy/per_sample/all_sptableScaled.tsv")
    output:
        join(DIR_RES, "host-microbiome/correlation/{contrast}/cor.deg-tax.tsv.gz")
    log:
        join(DIR_LOGS, "host-microbiome/cor/{contrast}_cor.log")
    benchmark:
        join(DIR_BENCHMARKS, "host-microbiome/cor/{contrast}_cor.txt")
    conda:
        join(DIR_ENVS, "cor.yaml")
    params:
        script=join(DIR_SCRIPTS, "cor.py"),
        extra="--threshold 0 --pv 1 --deg_pv {} --deg_top {}".format(COR_PV, COR_TOP)
    shell:
        "python {params.script} {params.extra} "
        "{input.deg} {input.tpm} {input.da} {input.tax} -o {output} > {log} 2>&1"


rule cor_matrix:
    input:
        join(DIR_RES, "host-microbiome/correlation/{contrast}/cor.deg-tax.tsv.gz")
    output:
        join(DIR_RES, "host-microbiome/correlation/{contrast}/cor.deg-tax.matrix.tsv")
    log:
        join(DIR_LOGS, "host-microbiome/cor/{contrast}_matrix.log")
    benchmark:
        join(DIR_BENCHMARKS, "host-microbiome/cor/{contrast}_matrix.txt")
    params:
        script=join(DIR_SCRIPTS, "make_matrix.py"),
        extra="--header --rows 1 --cols 3 2 --values 4"
    shell:
        "python {params.script} {params.extra} {input} -o {output} > {log} 2>&1"


rule get_clustergrammer_link:
    input:
        join(DIR_RES, "host-microbiome/correlation/{contrast}/cor.deg-tax.matrix.tsv")
    output:
        join(DIR_RES, "host-microbiome/correlation/{contrast}/cor.deg-tax.matrix.link")
    benchmark:
        join(DIR_BENCHMARKS, "host-microbiome/cor/{contrast}_cluster.txt")
    run:
        import requests
        upload_url = "http://amp.pharm.mssm.edu/clustergrammer/matrix_upload/"
        r = requests.post(upload_url, files={"file": open(input[0], "rb")})
        link = r.text
        with open(output[0], 'w') as out:
            out.write("{}\n".format(link))

#------------------------
# Shiny App Build
#------------------------

rule shiny_makedb:
    input:
        shinyIn
    output:
        db_file, temp(touch(join(DIR_SHINY, "shiny.done")))
    params:
        inp='{}/'.format(DIR_RES)
    log:
        join(DIR_LOGS, "shiny/makedb.log")
    benchmark:
        join(DIR_BENCHMARKS, "shiny/makedb.txt")
    conda:
        join(DIR_ENVS, "rshiny.yaml")
    singularity:
        "shub://sschmeier/container-metafunc-shiny:0.0.3"
    script:
        join(DIR_SCRIPTS, "buildSQLiteDB_sm.R")
