import pandas as pd
# read a STAR count table
counts = [pd.read_table(f, index_col=0, header=0) for f in snakemake.input]
matrix = pd.concat(counts, axis=1)
matrix.to_csv(snakemake.output[0], sep="\t")
