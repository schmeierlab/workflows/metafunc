#!/usr/bin/env python
"""
NAME: cor.py
============

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

LICENCE
=======
2020, copyright Sebastian Schmeier, (s.schmeier@gmail.com), https://sschmeier.com

template version: 1.9 (2017/12/08)
"""
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import gzip
import bz2
import zipfile
import time

## TURN OFF NUMPY BUILTIN MULTITHREADING
## DO BEFORE NUMPY IMPORT
#import mkl
#mkl.set_num_threads(1)
os.environ["OMP_NUM_THREADS"] = "1" # export OMP_NUM_THREADS=4
os.environ["OPENBLAS_NUM_THREADS"] = "1" # export OPENBLAS_NUM_THREADS=4 
os.environ["MKL_NUM_THREADS"] = "1" # export MKL_NUM_THREADS=6
os.environ["VECLIB_MAXIMUM_THREADS"] = "1" # export VECLIB_MAXIMUM_THREADS=4
os.environ["NUMEXPR_NUM_THREADS"] = "1" # export NUMEXPR_NUM_THREADS=6        
import numpy as np
import pandas as pd
import scipy.stats as ss

#from numba import jit

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)

__version__ = '0.0.4'
__date__ = '20200328'
__email__ = 's.schmeier@pm.me'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore, Style
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset=Fore.RESET
    colors = {'success': Fore.GREEN, 'error': Fore.RED, 'warning': Fore.YELLOW, 'info':''}
except ImportError:
    sys.stderr.write('colorama lib desirable. Install with "conda install colorama".\n\n')
    reset=''
    colors = {'success': '', 'error': '', 'warning': '', 'info':''}

def alert(atype, text, log):
    textout = '%s [%s] %s\n' % (time.strftime('%Y%m%d-%H:%M:%S'),
                                atype.rjust(7),
                                text)
    log.write('%s%s%s' % (colors[atype], textout, reset))
    if atype=='error': sys.exit(1)
        
def success(text, log=sys.stderr):
    alert('success', text, log)
    
def error(text, log=sys.stderr):
    alert('error', text, log)
    
def warning(text, log=sys.stderr):
    alert('warning', text, log)
    
def info(text, log=sys.stderr):
    alert('info', text, log)
    
##----------------------------------------------------------


def parse_cmdline():
    # parse cmd-line -----------------------------------------------------------
    sDescription = 'Correlation'
    sVersion='version %s, date %s' %(__version__,__date__)
    sEpilog = 'Copyright %s (%s)' %(__author__, __email__)

    parser = argparse.ArgumentParser(description=sDescription,
                                      epilog=sEpilog)
    parser.add_argument('--version',
                        action='version',
                        version='%s' % (sVersion))
    parser.add_argument('str_fileDEG',
                         metavar='FILE',
                         help='Differential expression table (edgeR).')
    parser.add_argument('str_file1',
                         metavar='FILE',
                         help='Expression matrix (host).')
    parser.add_argument('str_fileDA',
                         metavar='FILE',
                         help='Differential abundance table (edgeR - microbes).')
    parser.add_argument('str_file2',
                         metavar='FILE',
                         help='Abundance matrix (species - MetaFunc).')
    
    parser.add_argument('--deg_pv',
                         type = float,
                         metavar='FLOAT',
                         default=0.05,
                         help='Use diffexp gene/species with fdr < X for correlation calculation. [default: 0.05]')

    parser.add_argument('--deg_top',
                         type = int,
                         metavar='INT',
                         default=0,
                         help='Use top X diffexp gene/species for correlation calculation. [default: all]')
        
    parser.add_argument('--threshold',
                         type = float,
                         metavar='FLOAT',
                         default=0.0,
                         help='Correlation theshold to print correlation. [default: 0]')
    parser.add_argument('--pv',
                         type = float,
                         metavar='FLOAT',
                         default=1.0,
                         help='PValue correlation theshold to print correlation. [default: all]')
    parser.add_argument('-o',
                        '--out',
                        metavar='STRING',
                        dest='outfile_name',
                        default=None,
                        help='Out-file. [default: "stdout"]')
    parser.add_argument('--verbose',
                        action="store_true",
                        default=False,
                        help='Info. [default: "False"]')
    
    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.BZFile(filename)
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.Zipfile(filename)
    else:
        filehandle = open(filename)
    return filehandle

#@jit(nopython=True, parallel = False)
#def calc_cor(array, matrix):
#    c = np.corrcoef(array, matrix)
#    return list(c[0,1:])

def calc_cor_rho(array, matrix):
    rho, pval = ss.spearmanr(array, matrix, axis=1)
    rho = list(rho[0,1:])
    pval = list(pval[0,1:])
    return rho, pval

def main():
    args, parser = parse_cmdline()

    try:
        fileobjDEG = load_file(args.str_fileDEG)
    except:
        error('Could not load file {}. EXIT.'.format(args.str_fileDEG))
    
    try:
        fileobj1 = load_file(args.str_file1)
    except:
        error('Could not load file 1 {}. EXIT.'.format(args.str_file1))

    # create outfile object
    if not args.outfile_name:
        outfileobj = sys.stdout
    elif args.outfile_name in ['-', 'stdout']:
        outfileobj = sys.stdout
    elif args.outfile_name.split('.')[-1] == 'gz':
        outfileobj = gzip.open(args.outfile_name, 'wt')
    else:
        outfileobj = open(args.outfile_name, 'w')


    csv_reader =  csv.reader(fileobjDEG, delimiter = "\t")
    header = next(csv_reader)

    aDEG = []
    for a in csv_reader:
        if float(a[4]) < args.deg_pv:
            aDEG.append(a[0])

    # use top
    if args.deg_top > 0:
        aDEG = aDEG[:args.deg_top]
                        
    dDEG = dict.fromkeys(aDEG, None)
    
    ## do same for fileda
    try:
        fileobjDA = load_file(args.str_fileDA)
    except:
        error('Could not load file {}. EXIT.'.format(args.str_fileDA))

    ## header in file 2
    #df = pd.read_csv(args.str_file2, sep='\t', header=0, index_col=0)
    try:
        fileobj2 = load_file(args.str_file2)
    except:
        error('Could not load file 2: {}. EXIT.'.format(args.str_file2))
    
    csv_reader =  csv.reader(fileobjDA, delimiter = "\t")
    header = next(csv_reader)

    aDA = []
    for b in csv_reader:
        if float(b[4]) < args.deg_pv:
            aDA.append([tid.strip() for tid in b[0].split('_')][0])

    # use top
    if args.deg_top > 0:
        aDA = aDA[:args.deg_top]
                        
    dDA = dict.fromkeys(aDA, None)

    csv_reader =  csv.reader(fileobj2, delimiter = "\t")
    header = next(csv_reader)
    colnames = list(header[9:])
    idx = []
    names = {}
    data = []
    for a in csv_reader:
        ## only if TaxIDs are part of dDA
        if a[0] in dDA:
            idx.append(a[0])
            names[a[0]] = a[7]  # species name
            data.append(list(a[9:]))
    df = pd.DataFrame(data, columns=colnames, index=idx)
    ids = list(df.index)
    numMat = len(ids)
    mat = df.to_numpy()
    del(df)

    # expression data
    t1 = time.time()
    csv_reader =  csv.reader(fileobj1, delimiter = "\t")
    header = next(csv_reader)

    outfileobj.write("geneid\ttaxid\tspecies\trho\tpvalue\tsiglevel\n")
    
    iNum = 0
    for a in csv_reader:
        id1 = a[0]
        if id1 not in dDEG:
            continue
        
        x1 = np.array(a[1:], dtype="float")

        iNum += 1
        if args.verbose:
            if iNum % 1000 == 0:
                t2 = time.time()
                t = t2 - t1
                iNumReal = iNum * numMat
                iLast = numMat * 1000
                info(f"{iNumReal}: Run last {iLast} in {t} sec.\r")
                sys.stderr.flush()
                t1 = time.time()

        # pearson, no pvalue for now
        #res = calc_cor(x1, mat)
        # spearman
        res, pval = calc_cor_rho(x1, mat)
        
        for i in range(len(res)):
            id2 = ids[i]
            cor = res[i]
            pv = pval[i]
            sig = "-"
            if pv < 0.05:
                sig = "*"
            if pv < 0.01:
                sig = "**"
            if pv < 0.001:
                sig = "***"
                
            # print only if thresholds are satisfied
            if abs(cor) >= args.threshold:
                if pv <= args.pv:
                    outfileobj.write("{}\t{}\t{}\t{:.6f}\t{:10e}\t{}\n".format(id1, id2, names[id2], cor, pv, sig))
                    outfileobj.flush()
        del(res)
                
    return

if __name__ == '__main__':
    sys.exit(main())
