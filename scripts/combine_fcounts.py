#!/usr/bin/env python3
"""
NAME: combine_fcounts.py
========================

DESCRIPTION
-----------

INSTALLATION
------------

USAGE
-----

VERSION HISTORY
---------------

0.1.0    2020    Initial version.

LICENCE
-------
2020, copyright Sebastian Schmeier
s.schmeier@pm.me // https://www.sschmeier.com

template version: 2.2 (2020/02/08)
"""
__version__ = "0.1.0"
__date__ = "2020"
__email__ = "s.schmeier@pm.me"
__author__ = "Sebastian Schmeier"

# Generate your own AsciiArt at:
# https://patorjk.com/software/taag/#f=Calvin%20S&t=pyscript
__banner__ = r"""
    combine_fcounts
        by Sebastian Schmeier
"""

import sys
import os
import argparse
import csv
import gzip
import bz2
import zipfile
import time
import logging
import subprocess

# set up logging
logger = logging.getLogger()

_programpath = os.path.realpath(__file__)


def print_logo(filehandle=sys.stderr):
    text = f"{__banner__}\n"
    filehandle.write("{}\n".format("*" * 60))
    filehandle.write(text)
    filehandle.write("version: {}  date: {}\n".format(__version__, __date__))
    filehandle.write("Using executable at: {}\n".format(_programpath))
    filehandle.write("{}\n\n".format("*" * 60))


def parse_cmdline():
    """ Parse command-line args. """
    # parse cmd-line ----------------------------------------------------------
    description = "Read delimited file."
    version = "version {}, date {}".format(__version__, __date__)
    epilog = "Copyright {} ({})".format(__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument("--version", action="version", version="{}".format(version))

    parser.add_argument(
        "sample", metavar="NAME", help="Samplename.",
    )

    parser.add_argument(
        "files", metavar="FILE", nargs="+", help="Delimited file to combine.",
    )

    parser.add_argument(
        "--counts",
        metavar="STRING",
        dest="counts",
        default="counts.tsv",
        help='Counts out-file. [default: "counts.tsv"]',
    )

    parser.add_argument(
        "--tpm",
        metavar="STRING",
        dest="tpm",
        default="tpm.tsv",
        help='TPM out-file. [default: "tpm.tsv"]',
    )

    log = parser.add_argument_group("logging arguments")
    log.add_argument(
        "--log",
        metavar="STRING",
        dest="loglevel",
        default="INFO",
        help='Logging level. [default: "INFO"]',
    )

    # if no arguments supplied print help
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args, parser


def run(data, out1, out2, samplename):
    try:
        import pandas as pd
    except ImportError:
        logger.error("pandas library needed. Exit.")
        sys.exit(1)

    try:
        import numpy as np
    except ImportError:
        logger.error("numpy library needed. Exit.")
        sys.exit(1)

    def countToTpm(counts, effLen):
        # numpy matrix/arrays
        rate = counts / effLen
        tpm = rate / np.sum(rate) * 1e6
        return tpm

    # read a featureCounts count tables and combine
    counts = [
        pd.read_table(f, index_col=0, usecols=[0, 6], header=None, skiprows=2)
        for f in data
    ]
    # add column name = samplename
    for t, sample in zip(counts, [samplename] * len(counts)):
        t.columns = [sample]

    matrix = pd.concat(counts, axis=1)
    matrix.index.name = "gene"

    # collapse pe and se by summing
    matrix = matrix.groupby(matrix.columns, axis=1).sum()
    matrix.to_csv(out1, sep="\t")

    # calc TPM
    lens = pd.read_table(data[0], index_col=0, usecols=[0, 5], header=None, skiprows=2)
    lens = lens.to_numpy()
    mat = matrix.to_numpy()
    tpm = countToTpm(mat, lens)
    # better log transform
    # tpm = np.arcsinh(tpm)
    # or the usual nonsense
    # tpm = np.log2(tpm+1)
    tpm = pd.DataFrame(tpm)
    tpm.index = matrix.index
    tpm.columns = [samplename]
    tpm.to_csv(out2, sep="\t")


def main():
    """ The main funtion. """
    # comment out to remove logo printing
    print_logo(filehandle=sys.stderr)
    # parse commandline args
    args, parser = parse_cmdline()

    # set up logger
    numeric_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f"Invalid log level: {args.loglevel}")
    logging.basicConfig(
        level=numeric_level,
        format="%(asctime)s [%(levelname)8s] (%(filename)s:%(funcName)15s():%(lineno)4s): %(message)s ",
        datefmt="%Y%m%d-%H:%M:%S",
    )
    logger.info("Program start.")
    # test the logging system
    # logger.debug("Debug event")
    # logger.critical("Critical event")

    print(args.files)
    run(args.files, args.counts, args.tpm, args.sample)

    return


if __name__ == "__main__":
    sys.exit(main())
