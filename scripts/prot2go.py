#!/usr/bin/env python

"""
NAME: Prot2GO.py
=========

DESCRIPTION
===========

This script gives out gene ontology (GO) annotations for the protein accession numbers in Protein Accession-Based read counts files. Only Proteins from TaxIDs whose relative abundances matches a cutoff are included. 

GO-based Tables:

For each Sample/Group and for separate taxa, there are 2 tables generated - 1 for GO annotations of proteins and 1 'None' file containing information about the protein accession numbers without GO annotations.

1) The GO-based tables per sample/group per namespace has the following columns:

1. GO_ID: The Gene Ontology Number Annotations.
2. Description: Description/Name of the GO_ID.
3. GO: One of BP('biological_process'), MF('molecular_functional'), CC('cellular_component').
4. Number of reads (could be uniquely matching, proportional, raw): Sum of the reads of all protein accession numbers annotated with the GO_ID, depending on which read types was chosen from the protein accession file. Average number of reads is used if within groups.
5. Number of reads(scaled): Sum of the scaled reads of all protein accession numbers annotated with the GO_ID. Average number of (scaled) reads is used if within groups.
6. Percent: Percent of (scaled) reads from (5) covering the GO_ID, calculated by namespace.
5-a (if unique and allranks are false). Number of reads(sp_scaled): Sum of species scaled reads of all protein accession numbers annotated with the GO_ID. Average number of (species scaled) reads is used if within groups.
6-a (if unique and allranks are false). sp_Percent: Percent of (species-scaled) proportional reads from *(5-a)* covering the GO_ID, calculated per namespace.
7. GO_Depth: Depth of the GO_ID in the DAG.
8. Accession_Associated: Protein accession numbers annotated with the GO_ID.
9. Number_of_proteins: Number of protein accessions annotated with the GO_ID (count of accessions in (8).
10. Associated_TaxIDs: TaxIDs of the proteins annotated with the GO_ID.
11. Number_of_TaxIDs: Number of the TaxIDs of the proteins annotated with the GO_ID (count of TaxIDs in (10)).
12. group/sample: which sample/group the particular Gene Ontology information relates to

2) The None tab-separated file contains the following columns:

1. Accession: The protein accession number without a GO annotation.
2. Reason: (Reason why there is no annotation) 'Accession does not have GO annotations' wherein the accession number is not in the sqlite database and therefore has no GO annotations.
3. Associated_TaxIDs: TaxIDs of the protein accession.
4. Number_of_Associated_TaxIDs: Number of TaxIDs in (3).
5. Number of reads: read counts of the accession number (for groups, average of the read counts of the accession number among samples in the same group). As above, can be proportional, unique, raw depending on user choice.
6. Number of reads(scaled): scaled read counts of the accession number (for groups, average of the scaled read counts of the accession number among samples in the same group).
6-a (if unique and allranks are false). Number of reads(sp_scaled): species scaled read counts of the accession number (for groups, average of the species scaled read counts of the accession number among samples in the same group). 
7. group/sample: which sample/group the particular accession number information relates to

VERSION HISTORY
===============

July 09, 2019 - v.1.0.0
January 2020 - updates

LICENCE
=======
2019, copyright Arielle Sulit, (a.sulit@massey.ac.nz)

"""

import csv
import pandas as pd
from collections import Counter
import argparse
import sys
import os  ##for makedirs
from functools import reduce
import time
from goatools import obo_parser
from goatools.anno.update_association import update_association
import sqlite3 as lite

## If SettingwithCopyWarning is ever generated, raise error instead of warning; makes code more strict
pd.set_option("mode.chained_assignment", "raise")


def main():

    GOtable = argparse.ArgumentParser(
        description="constructs tables with gene ontology information for each accession number from a protein file for species level matches"
    )
    GOtable.add_argument(
        "--ProtConfig",
        action="store",
        type=str,
        help="tab separated file giving path to each input file and a grouping it belongs to; note: Line[0] = sample, Line[1] = protein accession based file, Line[2] = taxID based file, Line[3] = group; with headers",
    )
    GOtable.add_argument(
        "--cutoffs",
        action="store",
        type=str,
        help="file containing TaxIDs of interest, one per line. If given, will filter for proteins only coming from this set of inputs",
    )
    GOtable.add_argument(
        "--acc2go", action="store", type=str, help="sqlite for accession to GO"
    )
    GOtable.add_argument(
        "--goObo", action="store", type=str, help="obo file of the go DAG tree"
    )
    GOtable.add_argument(
        "--taxon",
        action="store",
        type=str,
        help="gather results by particular taxa or all",
    )
    GOtable.add_argument(
        "--all_ranks",
        action="store_true",
        help="if set, will give all levels; otherwise, only species level will be set",
    )  ##thinking of making obsolete
    GOtable.add_argument(
        "--prop",
        action="store_true",
        help="if set, will use proportional reads instead of whole reads",
    )
    GOtable.add_argument(
        "--unique",
        action="store_true",
        help="if set, will get uniquely matching reads only",
    )
    GOtable.add_argument(
        "--nogroup", action="store_true", help="indicate if no groupings is required"
    )
    GOtable.add_argument("--outdir", action="store", type=str, help="output directory")
    GOtable.add_argument(
        "--Direct_checks",
        action="store",
        type=str,
        help="Output File Name. If set, will output table with only direct GO Annotations to file (as opposed to propagated counts)",
    )

    args = GOtable.parse_args()

    ## define variables and corresponding errors if they are not present

    if args.ProtConfig is None:
        GOtable.error(
            "Error: please indicate where to find configuration for protein files"
        )
    else:
        ## segregate each protein file by corresponding group
        Filegroups = {}
        filed = open(args.ProtConfig, "r")
        csv_file = csv.reader(filed, delimiter="\t")
        next(csv_file)
        ## if no group is chosen, group according to filename (per file)
        if args.nogroup:
            nogroup = True
            grpcol = "sample"
            for protline in csv_file:
                if protline[0] in Filegroups:
                    sys.stderr.write(
                        "Error: Please check your configuration file: you may have doubled an input\n"
                    )
                    sys.exit(1)
                else:
                    Filegroups[protline[0]] = []
                    ## protline[1] is file path and protline[0] is sample name
                    samplestring = (
                        protline[1] + "," + protline[0]
                    )  # + "," + protline[2]
                    sampleinfo = [info.strip() for info in samplestring.split(",")]
                    Filegroups[protline[0]].append(sampleinfo)
        else:
            nogroup = False
            grpcol = "group"
            for protline in csv_file:
                if protline[3] not in Filegroups:
                    Filegroups[protline[3]] = []
                samplestring = protline[1] + "," + protline[0]  # + "," + protline[2]
                sampleinfo = [info.strip() for info in samplestring.split(",")]
                Filegroups[protline[3]].append(sampleinfo)
        filed.close()

    ## specify cutoffs for taxID abundances
    if args.cutoffs is None:
        taxfilters = None
    else:
        taxfilters = []
        cutoff = open(args.cutoffs, "r")
        for linya in cutoff.readlines():
            if linya.strip("\n") != "":
                taxfilters.append(linya.strip("\n"))

    ## specify if species only; no per level yet - if say, genus must include everything underneath and unsure what to do with 'no ranks' yet
    # might be obsolete
    if args.all_ranks:
        all_ranks = True
        if taxfilters is not None:
            sys.stderr.write(
                "Warning: all ranks was chosen and a filter file is given\n"
            )
            sys.stderr.flush()
    else:
        all_ranks = False

    ## specify what taxa is  wanted or all
    if args.taxon is None:
        GOtable.error("Error: please choose a taxon or indicate all\n")
    else:
        taxon = args.taxon

    ## specify where go.obo file is located
    if args.goObo is None:
        GOtable.error("Error: please indicate where GO obo table is located\n")
    else:
        golist = obo_parser.GODag(args.goObo, optional_attrs={"relationship"})
        relas = {"part_of"}
        sys.stdout.write(
            "Propagating additional counts for {} relationships\n".format(relas)
        )
        sys.stdout.flush()

    if args.acc2go is None:
        GOtable.error("Error: please input an accession to GO database\n")
    else:
        ## connect to sqlite
        conn = lite.connect(args.acc2go)
        acc2go = conn.cursor()
        sys.stdout.write("Connected to Acc2GO Sqlite\n")
        sys.stdout.flush()

    ## specify if only reads that matched to 1 protein is needed (only true for species level; cannot be all ranks)
    if args.unique:
        unique = True
        if all_ranks:
            sys.stderr.write(
                "Error: Scaling of uniquely matching reads is only done on species level. Getting all ranks and uniquely matching reads only will give no informative result. Exiting...\n"
            )
            sys.exit(1)
    else:
        unique = False

    ## specify if proportional reads should be used
    if args.prop:
        prop = True
        if unique:
            sys.stderr.write(
                "Error: Choose only one between uniquely matching reads and proportional reads\n"
            )
            sys.exit(1)
    else:
        prop = False

    ## specify output directory
    if args.outdir is None:
        outdir = "GOFiles"
    else:
        outdir = args.outdir

    if args.Direct_checks is None:
        checkGO = "None"
    else:
        checkGO = args.Direct_checks

    ## if output directory isn't there, make one
    try:
        if not os.path.exists(outdir):
            os.makedirs(outdir)
    except OSError:
        sys.stderr.write("Error: Creating directory: {}\n".format(outdir))
        sys.exit(1)

    ## for each group in Filegroups; if no group was selected, 1 group will be 1 sample
    # run through the functions (if nogroup was NOT selected, analyses will be on a set of protein files under 1 group);
    # if nogroup was selected analyses will be done individually per sample)
    stats = {
        "Basic_Information": [
            "Accessions_with_GO",
            "Accessions_without_GO",
            "Reads_with_GO",
            "Reads_without_GO",
            "Reads_below_cutoff_file",
        ]
    }
    wholedf = pd.DataFrame()
    wholenonedf = pd.DataFrame()
    if checkGO != "None":
        wholecheckdf = pd.DataFrame()
    # c=0
    for group_name in Filegroups:
        # c+=1
        stats[group_name] = []
        ## FILES is a list of lists each containing list[0]=path to file, and list[1]=filename
        # if nogroup, list should be of length 1
        t1 = time.time()
        FILES = Filegroups[group_name]
        ##get size of group; should be 1 for --nogroup [will be used to average counts later in groupby]
        grpsize = len(FILES)
        if grpcol == "sample":
            if grpsize != 1:
                sys.stderr.write("Error: More than 1 file for {}\n".format(group_name))
                sys.exit(1)
        ## function subsets
        group_df, belowrelab = GOTable(
            grpsize,
            FILES,
            group_name,
            all_ranks,
            taxon,
            unique,
            grpcol,
            prop,
            taxfilters,
        )
        sys.stdout.write("Finished subsetting dataframe for {}\n".format(group_name))
        ## CHECK if group_df is empty (already a problem)
        if group_df.empty:
            sys.stderr.write("Warning: Group/Sample {} is EMPTY!\n".format(group_name))
            ## all that comes from group_df is therefore empty
            # group_AllDF = pd.DataFrame()
            # NoneDF = pd.DataFrame()
            withGO = 0
            withoutGO = 0
            readsGO = 0
            readsnotGO = 0
            # if checkGO != "None":
            #    check_AllDF = pd.DataFrame()
        else:
            if checkGO != "None":
                (
                    group_AllDF,
                    NoneDF,
                    check_AllDF,
                    withGO,
                    withoutGO,
                    readsGO,
                    readsnotGO,
                ) = getGOannot(
                    grpsize,
                    group_df,
                    group_name,
                    acc2go,
                    all_ranks,
                    taxon,
                    golist,
                    unique,
                    grpcol,
                    relas,
                    checkGO,
                )
            else:
                (
                    group_AllDF,
                    NoneDF,
                    withGO,
                    withoutGO,
                    readsGO,
                    readsnotGO,
                ) = getGOannot(
                    grpsize,
                    group_df,
                    group_name,
                    acc2go,
                    all_ranks,
                    taxon,
                    golist,
                    unique,
                    grpcol,
                    relas,
                    checkGO,
                )
            ## only concatenate if group_df is not empty
            wholedf = pd.concat([wholedf, group_AllDF])
            wholenonedf = pd.concat([wholenonedf, NoneDF])
            if checkGO != "None":
                wholecheckdf = pd.concat([wholecheckdf, check_AllDF])

        stats[group_name].append(str(withGO))
        stats[group_name].append(str(withoutGO))
        stats[group_name].append(str(readsGO))
        stats[group_name].append(str(readsnotGO))
        stats[group_name].append(str(belowrelab))
        # if c==5:
        #    break

        t2 = time.time()
        sys.stdout.write(
            "Time to make GO Tables for {}: {} secs\n".format(group_name, t2 - t1)
        )

    if checkGO != "None":
        if not wholecheckdf.empty:
            wholecheckdf = wholecheckdf.sort_values(by=["Description", grpcol])
            wholecheckdf["GO"] = wholecheckdf["GO"].replace("biological_process", "BP")
            wholecheckdf["GO"] = wholecheckdf["GO"].replace("molecular_function", "MF")
            wholecheckdf["GO"] = wholecheckdf["GO"].replace("cellular_component", "CC")
            # print(wholecheckdf)
            wholecheckdf.to_csv("{}/{}".format(outdir, checkGO), sep="\t")
            sys.stdout.write(
                "Output Direct GO Annotations to {}/{}\n".format(outdir, checkGO)
            )
        else:
            sys.stderr.write(
                "Warning: No proteins have annotations. Check your results\n"
            )

    if unique:
        if nogroup:
            grpfilename = "{}/samples_{}_unique.gofile.tsv".format(outdir, taxon)
            Nonefile = "{}/samples_none_{}_unique.gofile.tsv".format(outdir, taxon)
        else:
            grpfilename = "{}/group_{}_unique.gofile.tsv".format(outdir, taxon)
            Nonefile = "{}/group_none_{}_unique.gofile.tsv".format(outdir, taxon)
    elif prop:
        if nogroup:
            grpfilename = "{}/samples_{}_prop.gofile.tsv".format(outdir, taxon)
            Nonefile = "{}/samples_none_{}_prop.gofile.tsv".format(outdir, taxon)
        else:
            grpfilename = "{}/group_{}_prop.gofile.tsv".format(outdir, taxon)
            Nonefile = "{}/group_none_{}_prop.gofile.tsv".format(outdir, taxon)
    else:
        if nogroup:
            grpfilename = "{}/samples_{}.gofile.tsv".format(outdir, taxon)
            Nonefile = "{}/samples_none_{}.gofile.tsv".format(outdir, taxon)
        else:
            grpfilename = "{}/group_{}.gofile.tsv".format(outdir, taxon)
            Nonefile = "{}/group_none_{}_.gofile.tsv".format(outdir, taxon)

    if not wholedf.empty:
        wholedf = wholedf.sort_values(by=["Description", grpcol])
        wholedf["GO"] = wholedf["GO"].replace("biological_process", "BP")
        wholedf["GO"] = wholedf["GO"].replace("molecular_function", "MF")
        wholedf["GO"] = wholedf["GO"].replace("cellular_component", "CC")
        # print(wholedf)
    else:
        sys.stderr.write("Warning: No proteins have annotations. Check your results\n")

    if not wholenonedf.empty:
        wholenonedf = wholenonedf.sort_values(by=["Accession", grpcol])
        wholenonedf = wholenonedf.set_index("Accession")
        # print(wholenonedf)
    else:
        sys.stderr.write("Warning: No proteins in None file. Check your results\n")

    wholedf.to_csv(grpfilename, sep="\t")
    wholenonedf.to_csv(Nonefile, sep="\t")

    sys.stdout.write("write GO tables to:\n{}\n{}\n".format(grpfilename, Nonefile))

    StatsDF = pd.DataFrame.from_dict(stats, dtype=object).set_index("Basic_Information")
    sys.stdout.write("\n{}\n".format(StatsDF.to_string()))
    sys.stdout.flush()

    # break

    conn.close()

    return


def GOTable(
    grpsize, FILES, group_name, all_ranks, taxon, unique, grpcol, prop, taxfilters
):

    ## print out group name for reference in outputs
    sys.stdout.write("{}\n".format(group_name))
    sys.stdout.flush()

    ## THIS IS NOT NECESSARY AS A FILE WITH TAXIDs of INTEREST WILL BE GIVEN AS INPUT
    # I want to filter for TaxIDs with a relative abundance indicated in cutoffs. Depending on input wanted, this could be scaled, sp_scaled, or uniquely matching (scaled)
    # Note that this is for the Taxon tables (no proportional columns)
    # if all_ranks:
    #    readscol = "Number_of_reads(scaled)"
    # else:
    #    if unique:
    #        readscol = "Number_of_uniquely_matching_reads(scaled)"
    #    else:
    #        readscol = "Number_of_reads(sp_scaled)"

    ## initialize combined data frame for all files in the group
    group_df = pd.DataFrame()
    groupfiltered_df = pd.DataFrame()
    ## for each file in the group (should only be 1 file if nogroup)
    for protfile in FILES:
        ## if with grouping, print out sample names for output reference
        if grpcol == "group":
            sys.stdout.write("{}\n".format(protfile[1]))
            sys.stdout.flush()

        ## NOT NECESSARY BECAUSE OF CUTOFFS FILE
        ## open taxon file to filter only for TaxIDs that have abundances of greater than cutoff
        # taxonfile = protfile[2]
        # taxdf = pd.read_csv(
        #    taxonfile,
        #    sep="\t",
        #    dtype=object,
        #    keep_default_na=False,
        #    usecols=["TaxID", readscol],
        # ).set_index("TaxID")
        ## drop 'NAs'; would drop all NA (which should be non-species level if all_ranks NOT chosen, and includes added information at table's end)
        # taxdf = taxdf.loc[taxdf[readscol] != "NA"].copy()
        # taxdf[readscol] = taxdf[readscol].astype(float)
        # taxdf = taxdf.loc[taxdf[readscol] > cutoff]
        # taxfilters = list(taxdf.index.values)

        ## on to protein tables
        proteinfile = protfile[0]
        ## open each file, dtype as object, reset NAs, do not set index to Protein Accession
        df = pd.read_csv(proteinfile, sep="\t", dtype=object)  # keep_default_na=False)

        ## subset dataframe according species or all_ranks and taxon of interest
        # if all_ranks is false (default) and taxon is all, just get species levels and drop all else
        # since species level, no 'NA'
        if all_ranks == False and taxon == "all":
            df = df.loc[df["Rank"] == "species"].copy()
        # if all_ranks is false (default) and taxon is given, subset according to taxon and species levels
        # since species level, no 'NA'
        elif all_ranks == False and taxon != "all":
            df = df.loc[(df["Rank"] == "species") & (df["RootTaxon"] == taxon)].copy()
        # if all_ranks is True and a taxon is specified, get taxon (may be obsolete)
        # will have NA but will be dropped later on
        elif all_ranks and taxon != "all":
            df = df.loc[df["RootTaxon"] == taxon].copy()
        ## if interested in all data, no dropping of rows (no more of the previous Classifed_Total, Others, and Unclassified as these wouldn't add up in a proteinfile anyway)
        # will have NA but will be dropped later on
        elif all_ranks and taxon == "all":
            df = df
        ## need to think about how necessary this is
        else:
            sys.stderr.write(
                "Error: Please check your combination for taxon and ranks\n"
            )
            sys.exit(1)

        ## for species, check that each item in Associated TaxIDs are just 1
        if all_ranks == False:
            for tryix in list(df.index.values):
                trytaxlist = [
                    t.strip() for t in df.at[tryix, "Associated_TaxIDs"].split(",")
                ]
                if len(trytaxlist) != 1:
                    # print(df.at[tryix, "ProteinAccession"])
                    # print(trytaxlist)
                    sys.stderr.write(
                        "Error: More than 1 tax ID for a protein in sample {}".format(
                            protfile[1]
                        )
                    )
                    sys.exit(1)

        ## NOTES: as index was not set to protein accession, depending on what was chosen for ranks and taxon, some accession numbers is possibly doubled (esp for all_ranks)
        ## if all_ranks is False, and only species level is wanted (default), for species level, no columns should have NA
        if all_ranks == False:
            ## define that the columns containing read numbers are of type float
            # even if empty df, would still have columns from output of Extract_MetaInfo.py
            df["Proportional_Reads"] = df["Proportional_Reads"].astype(float)
            df["Proportional_Reads(scaled)"] = df["Proportional_Reads(scaled)"].astype(
                float
            )
            df["Proportional_Reads(sp_scaled)"] = df[
                "Proportional_Reads(sp_scaled)"
            ].astype(float)
            df["Number_of_uniquely_matching_reads"] = df[
                "Number_of_uniquely_matching_reads"
            ].astype(float)
            df["Number_of_reads"] = df["Number_of_reads"].astype(float)
            df["Number_of_reads(scaled)"] = df["Number_of_reads(scaled)"].astype(float)
            df["Number_of_reads(sp_scaled)"] = df["Number_of_reads(sp_scaled)"].astype(
                float
            )
            df["Number_of_uniquely_matching_reads(scaled)"] = df[
                "Number_of_uniquely_matching_reads(scaled)"
            ].astype(float)
        ## if all_ranks==True was chosen, columns with species scaled have NA and will be dropped;
        # uniquely matching reads are also only considered for species level (it will have a value for other ranks but not informative for us)
        # these columns should not have NA
        else:
            df["Number_of_reads"] = df["Number_of_reads"].astype(float)
            df["Number_of_reads(scaled)"] = df["Number_of_reads(scaled)"].astype(float)
            df["Proportional_Reads"] = df["Proportional_Reads"].astype(float)
            df["Proportional_Reads(scaled)"] = df["Proportional_Reads(scaled)"].astype(
                float
            )

        ## if only reads matching to 1 protein is wanted
        if unique:
            # if all_ranks==True, will have given an error at the start already
            ## only retain uniquely matching columns; drop the columns for Number of reads and taxon names
            df = df.drop(
                columns=[
                    "Number_of_reads",
                    "Number_of_reads(scaled)",
                    "Number_of_reads(sp_scaled)",
                    "Associated_TaxNames",
                    "Proportional_Reads",
                    "Proportional_Reads(scaled)",
                    "Proportional_Reads(sp_scaled)",
                ]
            ).copy()
        ## OTHERWISE, drop uniquely matching reads
        elif prop:
            # if all_ranks is true only keep number of reads and number of reads (scaled) for proportional reads
            if all_ranks:
                df = df.drop(
                    columns=[
                        "Number_of_uniquely_matching_reads",
                        "Number_of_uniquely_matching_reads(scaled)",
                        "Number_of_reads(sp_scaled)",
                        "Number_of_reads",
                        "Number_of_reads(scaled)",
                        "Associated_TaxNames",
                        "Proportional_Reads(sp_scaled)",
                    ]
                ).copy()
            else:
                df = df.drop(
                    columns=[
                        "Number_of_uniquely_matching_reads",
                        "Number_of_uniquely_matching_reads(scaled)",
                        "Associated_TaxNames",
                        "Number_of_reads(sp_scaled)",
                        "Number_of_reads",
                        "Number_of_reads(scaled)",
                    ]
                ).copy()
        else:
            # if all_ranks is true only keep number of reads and number of reads (scaled) for whole reads
            if all_ranks:
                df = df.drop(
                    columns=[
                        "Number_of_uniquely_matching_reads",
                        "Number_of_uniquely_matching_reads(scaled)",
                        "Number_of_reads(sp_scaled)",
                        "Associated_TaxNames",
                        "Proportional_Reads",
                        "Proportional_Reads(scaled)",
                        "Proportional_Reads(sp_scaled)",
                    ]
                ).copy()
            else:
                df = df.drop(
                    columns=[
                        "Number_of_uniquely_matching_reads",
                        "Number_of_uniquely_matching_reads(scaled)",
                        "Associated_TaxNames",
                        "Proportional_Reads",
                        "Proportional_Reads(scaled)",
                        "Proportional_Reads(sp_scaled)",
                    ]
                ).copy()

        ## filter tables for taxids in cutoff
        newix = []
        notix = []
        for INDEX in list(df.index.values):
            if taxfilters is not None:
                afilters = set(
                    [
                        ass.strip()
                        for ass in df.at[INDEX, "Associated_TaxIDs"].split(",")
                    ]
                )
                if len(afilters.intersection(set(taxfilters))) > 0:
                    newix.append(INDEX)
                else:
                    notix.append(INDEX)
            else:
                newix.append(INDEX)

        filtereddf = df.reindex(notix)
        ##subset for indices (should be in numbers as index is not set yet)
        df = df.reindex(newix)

        ## combine all dataframes from all files in the particular group (one on top of the other);
        # note that for non-grouped, this is just one dataframe from the sample
        group_df = pd.concat([group_df, df])
        groupfiltered_df = pd.concat([groupfiltered_df, filtereddf])

    ## confirm no 'na' in group_df
    if group_df.isna().any().any():
        sys.stderr.write(
            "Error: Unexpected NaN values upon opening {} tables\n".format(group_name)
        )
        sys.exit(1)
    # nacols=group_df.columns[group_df.isna().any()].tolist()
    # print(nacols)
    # if len(nacols)>0:

    ## for protein accessions that did not make the filters of cutoff
    # groupby kingdom, rank, and accession... add all of type float (read counts) [average for group] and catenate all else (taxIDs);
    reads_filtsummed = list(groupfiltered_df.head(0))[1]
    groupfiltinfo = groupfiltered_df.groupby(
        ["RootTaxon", "Rank", "ProteinAccession"], as_index=False
    ).agg(lambda y: y.sum() / grpsize if y.dtype == "float64" else ",".join(y))

    if groupfiltinfo.empty:
        belowrelab = float(0)
    else:
        belowrelab = groupfiltinfo[reads_filtsummed].sum()

    return group_df, belowrelab


def getGOannot(
    grpsize,
    group_df,
    group_name,
    acc2go,
    all_ranks,
    taxon,
    golist,
    unique,
    grpcol,
    relas,
    checkGO,
):
    ## define reads to be summed as the second column (could be uniquely matching reads, or proportional, or all read counts)
    # if unique, will get uniquely matching; if not unique (whether all ranks or not, number of reads or proportional reads);
    # if not all_ranks and not unique, there are additional columns of species scaled
    reads_summed = list(group_df.head(0))[1]
    reads_scaled = list(group_df.head(0))[2]
    if unique == False and all_ranks == False:
        reads_spscaled = list(group_df.head(0))[3]

    # CONFIRM DTYPES OF COLUMNS
    float64cols = sorted(list(group_df.select_dtypes(include=["float64"]).columns))
    if unique == False and all_ranks == False:
        float64comp = sorted([reads_summed, reads_scaled, reads_spscaled])
    else:
        float64comp = sorted([reads_summed, reads_scaled])
    if float64cols != float64comp:
        sys.stderr.write(
            "Error: Float64 setting for group/sample {} protein table is compromised\n".format(
                group_name
            )
        )
        sys.exit(1)

    ## float64 lists aren't necessary anymore
    del (float64cols, float64comp)

    ## groupby kingdom, rank, and accession... add all of type float (read counts) [average for group] and catenate all else (taxIDs);
    # kingdom and rank shouldn't matter if all and all_ranks, respectively are not chosen
    # note: groupby "works" on empty df BUT it removes column names --> see above for empty df
    grouped = group_df.groupby(
        ["RootTaxon", "Rank", "ProteinAccession"], as_index=False
    ).agg(lambda x: x.sum() / grpsize if x.dtype == "float64" else ",".join(x))
    ##drop rows wherein reads_summed is equal to 0 (note, this is mostly for uniquely matching reads); and if 0 in uniquely matching, should be 0 in scaled anyway
    grouped = grouped.loc[grouped[reads_summed] != 0.0].copy()

    #### note: list is a python object in a dataframe and you have to be careful!!!
    for row in list(grouped.index.values):
        ## for ','.join instead of list(x), list comprehension is enough to split records into individual IDs
        TaxID = grouped.at[row, "Associated_TaxIDs"]  # .replace("'", "")
        TaxID = list(set([w.strip() for w in TaxID.split(",")]))
        grouped.at[row, "Associated_TaxIDs"] = TaxID

    ## if direct GO annotation is necessary:
    if checkGO != "None":
        ##initialize GO to accession numbers dictionary (could end up with double accession numbers)
        # include propagated dictionary
        GOacc = {}  # accession associated with GO
        ##initialize GO to taxID dicionary
        GOtax = {}  # taxID associated with GO
        ##initialize GO to description dictionary; emtpy values for now
        GOnames = {}  # description of GO numbers
        ##initialize GO to corresponding readcounts dictionary [could be uniquely matching or not]
        GOreadcount = {}  # number of reads associated with GO
        ##initialize GO to corresponding scaled readcounts dictionary
        GOscaledcount = {}
        if unique == False and all_ranks == False:
            ## This is for with GO annotation, species-level scaled counts of non-unique reads
            GOScspCount = {}

    # for propagated dictionaries (equivalent of above)
    propaAcc = {}
    propaID = {}
    propanames = {}
    propaGOcount = {}
    propaGOscaled = {}
    if unique == False and all_ranks == False:
        propaGOspscaled = {}

    ##Files for those with no GO annotation; this is "reason" (could be not in database, or not in GO-Obo)
    GONoneAcc = {}
    ##Files for those with no GO annotation;; this is for taxIDs associated with GOs
    GONoneTax = {}
    ##Files for those with no GO annotation; this is for count of the reads of the proteins
    GONoneCount = {}
    ##Files for those with no GO annotation; this is for count of the scaled reads of proteins
    GONoneScCount = {}
    ##if non-unique at species level, species scaled is considered
    if unique == False and all_ranks == False:
        ## Files for those with no GO annotation; this is for species level scaled count of non-unique reads of proteins
        GONoneScspCount = {}

    withGO = 0
    withoutGO = 0
    readsGO = 0
    readsnotGO = 0
    ##for each line in the grouped dataframe:
    for ix in list(grouped.index.values):
        ## define the protein accession number; should be unique already because of above groupby EXCEPT if all_ranks==True
        accession = grouped.at[ix, "ProteinAccession"]
        # print(accession)
        ## define the number of reads matching to the accession (could be uniquely matching or not depending on whether --unique was chosen)
        # note that as of here, read_summed is of type float64
        reads = grouped.at[ix, reads_summed]
        ## define scaled reads matching to accession (could be uniquely matching or not (if not, scaled to everything else))
        #  note that as of here, read_scaled is of type float64
        scaled = grouped.at[ix, reads_scaled]
        if unique == False and all_ranks == False:
            ## if non-unique reads are wanted, extra column of reads scaled to species level
            scaledsp = grouped.at[ix, reads_spscaled]
        ## define the taxIDs associated with the accession (would be a list)
        taxIDs = grouped.at[ix, "Associated_TaxIDs"]
        ## Initialize GOs associated with each accession number
        GO = []
        for lookup in acc2go.execute(
            "SELECT DISTINCT go.goid from go INNER JOIN nr2go ON go.id=nr2go.go_id WHERE nr2go.nr_id=?",
            (accession,),
        ):
            GO.append("GO:" + str(lookup[0]).zfill(7))

        ## if empty list (meaning no GO matches), place accession into "None" file dictionaries
        if not GO:
            withoutGO += 1
            readsnotGO += reads
            ## initialize dictionary values if accession not yet found in dictionaries
            if accession not in GONoneAcc:
                ## as these all started at the same time, can be populated at the same time
                GONoneAcc[accession] = []
                GONoneTax[accession] = []
                GONoneCount[accession] = 0
                GONoneScCount[accession] = 0
                if unique == False and all_ranks == False:
                    GONoneScspCount[accession] = 0
            ## populate dictionary values
            ## could be multiple in all_ranks; for 'all' taxon, meanwhile, should be okay because accession number is linked to a taxID which should be in 1 kingdom only anyway
            GONoneAcc[accession].append("Accession does not have GO annotations")
            GONoneCount[accession] = GONoneCount[accession] + reads
            GONoneScCount[accession] = GONoneScCount[accession] + scaled
            if unique == False and all_ranks == False:
                GONoneScspCount[accession] = GONoneScspCount[accession] + scaledsp
            for d in taxIDs:
                ## make it unique
                if d not in GONoneTax[accession]:
                    GONoneTax[accession].append(d)

        ## else, accession number has associated GOs
        else:
            withGO += 1
            readsGO += reads
            ## get a list of values that might be obsolete
            obs = [ob for ob in GO if ob not in golist]
            ## if obs list is NOT empty, there are obsolete values and gets printed to error
            if len(obs) > 0:
                sys.stderr.write(
                    "Warning for group/sample {}: {} for {} in rank {} is not in go-obo\n".format(
                        group_name, ",".join(obs), accession, grouped.at[ix, "Rank"]
                    )
                )
            ## get a list of GOs that are not obsolete and place in an accession to GO set dictionary
            ## would get reset to accession and set of GOs for each row
            updatedGO = {accession: (set(GO) - set(obs))}

            ## if we want direct GO annotations, parse through original set of GOs
            if checkGO != "None":
                for go in list(updatedGO[accession]):
                    if go not in GOacc:
                        ## as these should have started at the same time, can be populated at the same time
                        GOacc[go] = []
                        GOtax[go] = []
                        ## will stay empty; only initialize keys (GO numbers) - will be populated with values later
                        GOnames[go] = []
                        GOreadcount[go] = 0
                        GOscaledcount[go] = 0
                        if unique == False and all_ranks == False:
                            GOScspCount[go] = 0
                    ## populate dictionary values
                    ## corresponding accession number; if all_ranks could be multiple
                    GOacc[go].append(accession)
                    ## total readcount matching to accession number and therefore matching to GO (additive)
                    GOreadcount[go] = GOreadcount[go] + reads
                    ## total readcount(scaled) matching to accession number and therefore matching to GO (additive)
                    GOscaledcount[go] = GOscaledcount[go] + scaled
                    if unique == False and all_ranks == False:
                        GOScspCount[go] = GOScspCount[go] + scaledsp
                    ## corresponding taxIDs that matched to accession number (therefore match to GOs)
                    for d in taxIDs:
                        ## make it unique
                        if d not in GOtax[go]:
                            GOtax[go].append(d)

            ## update associations using part_of relationship; note that is_a is default
            update_association(updatedGO, golist, relationships=relas, prt=None)
            ## parse through each GO of the updated list
            for upgo in list(updatedGO[accession]):
                if upgo not in propaGOcount:
                    propaGOcount[upgo] = 0
                    propaGOscaled[upgo] = 0
                    if unique == False and all_ranks == False:
                        propaGOspscaled[upgo] = 0
                    propanames[upgo] = []
                    propaAcc[upgo] = []
                    propaID[upgo] = []
                propaGOcount[upgo] = propaGOcount[upgo] + reads
                propaGOscaled[upgo] = propaGOscaled[upgo] + scaled
                if unique == False and all_ranks == False:
                    propaGOspscaled[upgo] = propaGOspscaled[upgo] + scaledsp
                ## corresponding accession number; if all_ranks could be multiple
                propaAcc[upgo].append(accession)
                for s in taxIDs:
                    if s not in propaID[upgo]:
                        propaID[upgo].append(s)
    # break

    if unique:
        sys.stdout.write(
            "For unique reads in {} species of {}, there are {} accessions with GOs and {} without GOs. There are {} reads with GOs and {} reads without GOs.\n".format(
                taxon, group_name, withGO, withoutGO, readsGO, readsnotGO
            )
        )
        sys.stdout.flush()
    elif unique == False and all_ranks == False:
        sys.stdout.write(
            "For {} species in {}, there are {} accessions with GOs and {} without GOs. There are {} reads with GOs and {} reads without GOs.\n".format(
                taxon, group_name, withGO, withoutGO, readsGO, readsnotGO
            )
        )
        sys.stdout.flush()
    else:
        sys.stdout.write(
            "For {} organisms in {}, there are {} accessions with GOs and {} without GOs. There are {} reads with GOs and {} reads without GOs.\n".format(
                taxon, group_name, withGO, withoutGO, readsGO, readsnotGO
            )
        )
        sys.stdout.flush()

    if checkGO != "None":
        ## get counts of how many taxIDs are associated with the GO in original table
        GOtaxcounts = {}
        for tax in GOtax:
            GOtaxcounts[tax] = str(len(GOtax[tax]))
            ## get the string equivalent of the sorted list of taxIDs for that GO
            GOtax[tax] = ", ".join(sorted(GOtax[tax]))
        ## counting how many times an accession number was associated with a GO;
        # would usually vary above species level (i.e. that is an accession number is counted twice because of association with different ranks)
        Counted = {}
        for geneont in GOacc:
            ## counts how many times an accession number is associated to the GO
            counts = dict(Counter(GOacc[geneont]))
            ##note: dictionary within dictionary
            Counted[geneont] = counts

        ## don't need GOacc anymore
        del GOacc

        ## separate accession number and corresponding counts into two separate dictionaries; note these should be respective of one another
        GOacc2 = {}
        GOacccounts = {}
        ## countDict is expected to be unique because it is dictionary
        for countDict in Counted:
            for access in Counted[countDict]:
                if countDict not in GOacc2:
                    ## populate dictionary with gene ontology keys
                    GOacc2[countDict] = []
                    GOacccounts[countDict] = []
                ## populate with accession numbers
                GOacc2[countDict].append(access)
                ## populate with count of accession numbers
                GOacccounts[countDict].append(str(Counted[countDict][access]))

        ## theoretically, if species level, accession numbers are unique (this is an internal check)
        if all_ranks == False:
            for counting in GOacccounts:
                for i in GOacccounts[counting]:
                    if int(i) != 1:
                        sys.stderr.write(
                            "Error for group/sample {}. DOUBLE ACCESSION FOR GO!:{}-{}\n".format(
                                group_name, counting, GOacccounts[counting]
                            )
                        )
                        sys.exit(1)

        ## don't need GOacccounts anymore
        del GOacccounts

        ## count how many accession numbers are associated with a GO
        # since GOacc2 was supposed to have been unique-d, use that
        GOlen = {}
        for acclen in GOacc2:
            GOlen[acclen] = str(len(GOacc2[acclen]))
            ## get string equivalent of sorted list
            GOacc2[acclen] = ", ".join(sorted(GOacc2[acclen]))

        ## get string equivalent of the readcounts (scaled and not scaled)
        for count1 in GOreadcount:
            GOreadcount[count1] = str(GOreadcount[count1])

        for count3 in GOscaledcount:
            GOscaledcount[count3] = str(GOscaledcount[count3])

        if unique == False and all_ranks == False:
            for count6 in GOScspCount:
                GOScspCount[count6] = str(GOScspCount[count6])

        ## initialize GO to depth dictionary
        GOdepth = {}
        ## initialize GO to namespace dictionary
        GOnsp = {}
        for naming in GOnames:
            ## if item is in the parsed go obo file (which it should be!!!)
            if naming in golist:
                ## get name, depth, and namespace
                Name = golist[naming].name
                Depth = golist[naming].depth
                Namespace = golist[naming].namespace
                ## populate dictionary; note that since same keys as GOnames, should be unique
                # will overwrite '[]' in GOnames
                GOnames[naming] = Name
                GOdepth[naming] = str(Depth)
                GOnsp[naming] = Namespace
            else:
                sys.stderr.write(
                    "Error for {}: {} NOT IN go-obo!\n".format(group_name, naming)
                )
                sys.exit(1)

        ##get dataframe and labels for each dictionary, coverted to series first
        GOacc2DF = pd.DataFrame(
            {
                "GO_ID": pd.Series(GOacc2, dtype=object).index,
                "Accession_Associated": pd.Series(GOacc2, dtype=object).values,
            }
        )
        GOlenDF = pd.DataFrame(
            {
                "GO_ID": pd.Series(GOlen, dtype=object).index,
                "Number_of_proteins": pd.Series(GOlen, dtype=object).values,
            }
        )
        GOnamesDF = pd.DataFrame(
            {
                "GO_ID": pd.Series(GOnames, dtype=object).index,
                "Description": pd.Series(GOnames, dtype=object).values,
            }
        )
        GOdepthDF = pd.DataFrame(
            {
                "GO_ID": pd.Series(GOdepth, dtype=object).index,
                "GO_Depth": pd.Series(GOdepth, dtype=object).values,
            }
        )
        GOtaxDF = pd.DataFrame(
            {
                "GO_ID": pd.Series(GOtax, dtype=object).index,
                "Associated_TaxIDs": pd.Series(GOtax, dtype=object).values,
            }
        )
        GOtaxcountsDF = pd.DataFrame(
            {
                "GO_ID": pd.Series(GOtaxcounts, dtype=object).index,
                "Number_of_TaxIDs": pd.Series(GOtaxcounts, dtype=object).values,
            }
        )
        GOreadsDF = pd.DataFrame(
            {
                "GO_ID": pd.Series(GOreadcount, dtype=object).index,
                reads_summed: pd.Series(GOreadcount, dtype=object).values,
            }
        )
        GOscaledDF = pd.DataFrame(
            {
                "GO_ID": pd.Series(GOscaledcount, dtype=object).index,
                reads_scaled: pd.Series(GOscaledcount, dtype=object).values,
            }
        )
        # else:
        #    GOreadsDF=pd.DataFrame({'GO_Number':pd.Series(GOreadcount).index, 'Number_of_Reads':pd.Series(GOreadcount).values})
        #    GOscaledDF=pd.DataFrame({'GO_Number':pd.Series(GOscaledcount).index, 'Number_of_Reads(Scaled)':pd.Series(GOscaledcount).values})
        GOnspDF = pd.DataFrame(
            {
                "GO_ID": pd.Series(GOnsp, dtype=object).index,
                "GO": pd.Series(GOnsp, dtype=object).values,
            }
        )

        ## if non-unique, there is an extra column
        if unique == False and all_ranks == False:
            GOScSpDF = pd.DataFrame(
                {
                    "GO_ID": pd.Series(GOScspCount, dtype=object).index,
                    reads_spscaled: pd.Series(GOScspCount, dtype=object).values,
                }
            )
            group_df_list = [
                GOacc2DF,
                GOlenDF,
                GOtaxDF,
                GOtaxcountsDF,
                GOreadsDF,
                GOscaledDF,
                GOScSpDF,
                GOnamesDF,
                GOdepthDF,
                GOnspDF,
            ]
        else:
            ## make a list of all the dataframe to combine to one whole dataframe, in order (note that if unique is chosen, less than 1 column)
            group_df_list = [
                GOacc2DF,
                GOlenDF,
                GOtaxDF,
                GOtaxcountsDF,
                GOreadsDF,
                GOscaledDF,
                GOnamesDF,
                GOdepthDF,
                GOnspDF,
            ]

        ## merge the data frames in above list
        check_AllDF = reduce(
            lambda left, right: pd.merge(left, right, on="GO_ID", how="outer"),
            group_df_list,
        ).set_index("GO_ID")
        check_AllDF[grpcol] = group_name

        ## what if check_AllDF is empty??? Thinking that propagated DF would also be empty because you would have no GOs to propagate; cannot have a propagated GO table that is NOT empty
        # but check_AllDF or subsets of it being empty because propagated GO table is from check_AllDF table and you can't get propagated counts from nowhere

    ##FOR PROPAGATED COUNTS

    ## get counts of how many taxIDs are associated with the GO in propagated table
    propaGOtaxcounts = {}
    for propatax in propaID:
        propaGOtaxcounts[propatax] = str(len(propaID[propatax]))
        ## get the string equivalent of the sorted list of taxIDs for that GO
        propaID[propatax] = ", ".join(sorted(propaID[propatax]))

    ## counting how many times an accession number was associated with a GO;
    propacounted = {}
    for propaont in propaAcc:
        ## counts how many times an accession number is associated to the GO
        propagatedcts = dict(Counter(propaAcc[propaont]))
        ## this is a dictionary within dictionary
        propacounted[propaont] = propagatedcts

    ## don't need propaAcc
    del propaAcc

    ## separate accession number and corresponding counts into two separate dictionaries; note these should be respective of one another

    propaAcc2 = {}
    propaAcccounts = {}
    ## propacountDict is expected to be unique because it is dictionary
    for propacountDict in propacounted:
        for propaccess in propacounted[propacountDict]:
            if propacountDict not in propaAcc2:
                ## populate dictionary with gene ontology keys
                propaAcc2[propacountDict] = []
                propaAcccounts[propacountDict] = []
            ## populate with accession numbers
            propaAcc2[propacountDict].append(propaccess)
            ## populate with count of accession numbers
            propaAcccounts[propacountDict].append(
                str(propacounted[propacountDict][propaccess])
            )

    ## theoretically, if species level, accession numbers are unique (this is an internal check)
    if all_ranks == False:
        for propacounting in propaAcccounts:
            for k in propaAcccounts[propacounting]:
                if int(k) != 1:
                    sys.stderr.write(
                        "Error for group/sample {}. DOUBLE ACCESSION FOR PROPAGATED GO!:{}-{}\n".format(
                            group_name, propacounting, propaAcccounts[propacounting]
                        )
                    )
                    sys.exit(1)

    ## don't need GOacccounts anymore
    del propaAcccounts

    propaGOlen = {}
    for propalen in propaAcc2:
        propaGOlen[propalen] = str(len(propaAcc2[propalen]))
        propaAcc2[propalen] = ", ".join(sorted(propaAcc2[propalen]))

    ##get string equivalents for propagated counts
    for propacount1 in propaGOcount:
        propaGOcount[propacount1] = str(propaGOcount[propacount1])

    for propacount2 in propaGOscaled:
        propaGOscaled[propacount2] = str(propaGOscaled[propacount2])

    if unique == False and all_ranks == False:
        for propacount3 in propaGOspscaled:
            propaGOspscaled[propacount3] = str(propaGOspscaled[propacount3])

    ##get information of GOs from the names of propagated GOs (did this separately because some GOs may have been added because of the propagation)
    propadepth = {}
    propans = {}
    for propanaming in propanames:
        if propanaming in golist:
            propaname = golist[propanaming].name
            propaDepth = golist[propanaming].depth
            propaNS = golist[propanaming].namespace
            ## populate dictionary; note that since same keys as GOnames, should be unique
            # will overwrite '[]' in GOnames
            propanames[propanaming] = propaname
            propadepth[propanaming] = str(propaDepth)
            propans[propanaming] = propaNS
        else:
            sys.stderr.write(
                "Error for {}: {} NOT IN go-obo!\n".format(group_name, propanaming)
            )
            sys.exit(1)

    ## for propagated DataFrame
    propaAccDF = pd.DataFrame(
        {
            "GO_ID": pd.Series(propaAcc2, dtype=object).index,
            "Accession_Associated": pd.Series(propaAcc2, dtype=object).values,
        }
    )
    propaGOlenDF = pd.DataFrame(
        {
            "GO_ID": pd.Series(propaGOlen, dtype=object).index,
            "Number_of_proteins": pd.Series(propaGOlen, dtype=object).values,
        }
    )
    propanamesDF = pd.DataFrame(
        {
            "GO_ID": pd.Series(propanames, dtype=object).index,
            "Description": pd.Series(propanames, dtype=object).values,
        }
    )
    propadepthDF = pd.DataFrame(
        {
            "GO_ID": pd.Series(propadepth, dtype=object).index,
            "GO_Depth": pd.Series(propadepth, dtype=object).values,
        }
    )
    propaIDDF = pd.DataFrame(
        {
            "GO_ID": pd.Series(propaID, dtype=object).index,
            "Associated_TaxIDs": pd.Series(propaID, dtype=object).values,
        }
    )
    propaGOtaxcountsDF = pd.DataFrame(
        {
            "GO_ID": pd.Series(propaGOtaxcounts, dtype=object).index,
            "Number_of_TaxIDs": pd.Series(propaGOtaxcounts, dtype=object).values,
        }
    )
    propaGOcountDF = pd.DataFrame(
        {
            "GO_ID": pd.Series(propaGOcount, dtype=object).index,
            reads_summed: pd.Series(propaGOcount, dtype=object).values,
        }
    )
    propaGOscaledDF = pd.DataFrame(
        {
            "GO_ID": pd.Series(propaGOscaled, dtype=object).index,
            reads_scaled: pd.Series(propaGOscaled, dtype=object).values,
        }
    )
    # else:
    #    GOreadsDF=pd.DataFrame({'GO_Number':pd.Series(GOreadcount).index, 'Number_of_Reads':pd.Series(GOreadcount).values})
    #    GOscaledDF=pd.DataFrame({'GO_Number':pd.Series(GOscaledcount).index, 'Number_of_Reads(Scaled)':pd.Series(GOscaledcount).values})
    propansDF = pd.DataFrame(
        {
            "GO_ID": pd.Series(propans, dtype=object).index,
            "GO": pd.Series(propans, dtype=object).values,
        }
    )

    ## if non-unique, there is an extra column
    if unique == False and all_ranks == False:
        propaGOspscaledDF = pd.DataFrame(
            {
                "GO_ID": pd.Series(propaGOspscaled, dtype=object).index,
                reads_spscaled: pd.Series(propaGOspscaled, dtype=object).values,
            }
        )
        propa_df_list = [
            propaAccDF,
            propaGOlenDF,
            propaIDDF,
            propaGOtaxcountsDF,
            propaGOcountDF,
            propaGOscaledDF,
            propaGOspscaledDF,
            propanamesDF,
            propadepthDF,
            propansDF,
        ]
    else:
        ## make a list of all the dataframe to combine to one whole dataframe, in order (note that if unique is chosen, less than 1 column)
        propa_df_list = [
            propaAccDF,
            propaGOlenDF,
            propaIDDF,
            propaGOtaxcountsDF,
            propaGOcountDF,
            propaGOscaledDF,
            propanamesDF,
            propadepthDF,
            propansDF,
        ]

    ##get propagated GO table
    group_AllDF = reduce(
        lambda left, right: pd.merge(left, right, on="GO_ID", how="outer"),
        propa_df_list,
    ).set_index("GO_ID")
    group_AllDF[grpcol] = group_name

    ### FOR NONE DATAFRAME

    for count2 in GONoneCount:
        GONoneCount[count2] = str(GONoneCount[count2])

    for count4 in GONoneScCount:
        GONoneScCount[count4] = str(GONoneScCount[count4])

    if unique == False and all_ranks == False:
        for count5 in GONoneScspCount:
            GONoneScspCount[count5] = str(GONoneScspCount[count5])

    ## get counts of how many taxIDs are associated with the accession in None Files
    GONoneTaxCounts = {}
    for Nonetax in GONoneTax:
        GONoneTaxCounts[Nonetax] = str(len(GONoneTax[Nonetax]))
        ## get the string equivalent of the list of taxIDs for that accession; sort list first
        GONoneTax[Nonetax] = ", ".join(sorted(GONoneTax[Nonetax]))

    ## theoretically, if accession number has no GO, there is only one of 2 reasons:
    # accession has no go or has a go that is obsolete (not in go_obo of go consortium)
    for reason in GONoneAcc:
        ## removed checks because above loop already checks if an accession has a GO and if not, gives the 'Accession has no GO annotations'
        # if accession is annotated with a go that is obsolete, it can be annotated by more than 1 GO that is obsolete and therefore,
        # would not give a single reason for the 'no GO' condition (e.g. the '{}:Not in go-obo (KeyError)' can have different GOs for {})
        # if len(list(set(GONoneAcc[reason]))) > 1:
        #    sys.stderr.write('Error: MULTIPLE REASON FOR NO GO!!!\n')
        #    sys.exit(1)
        ## if all_ranks, multipled "reasons" would be set to unique reason (theoretically should be just 1 reason anyway)
        GONoneAcc[reason] = ", ".join(list(set(GONoneAcc[reason])))

    GONoneAccDF = pd.DataFrame(
        {
            "Accession": pd.Series(GONoneAcc, dtype=object).index,
            "Reason": pd.Series(GONoneAcc, dtype=object).values,
        }
    )
    GONoneTaxDF = pd.DataFrame(
        {
            "Accession": pd.Series(GONoneTax, dtype=object).index,
            "Associated_TaxIDs": pd.Series(GONoneTax, dtype=object).values,
        }
    )
    GONoneTaxCountsDF = pd.DataFrame(
        {
            "Accession": pd.Series(GONoneTaxCounts, dtype=object).index,
            "Number_of_Associated_TaxIDs": pd.Series(
                GONoneTaxCounts, dtype=object
            ).values,
        }
    )
    GONoneCountDF = pd.DataFrame(
        {
            "Accession": pd.Series(GONoneCount, dtype=object).index,
            reads_summed: pd.Series(GONoneCount, dtype=object).values,
        }
    )
    GONoneScCountDF = pd.DataFrame(
        {
            "Accession": pd.Series(GONoneScCount, dtype=object).index,
            reads_scaled: pd.Series(GONoneScCount, dtype=object).values,
        }
    )

    ## if non-unique, species there is an extra column
    if unique == False and all_ranks == False:
        GONoneScspCountDF = pd.DataFrame(
            {
                "Accession": pd.Series(GONoneScspCount, dtype=object).index,
                reads_spscaled: pd.Series(GONoneScspCount, dtype=object).values,
            }
        )
        Nonelist = [
            GONoneAccDF,
            GONoneTaxDF,
            GONoneTaxCountsDF,
            GONoneCountDF,
            GONoneScCountDF,
            GONoneScspCountDF,
        ]
    else:
        Nonelist = [
            GONoneAccDF,
            GONoneTaxDF,
            GONoneTaxCountsDF,
            GONoneCountDF,
            GONoneScCountDF,
        ]

    ## gather none dfs into 1 dataframe
    NoneDF = reduce(
        lambda left, right: pd.merge(left, right, on="Accession", how="outer"), Nonelist
    )  # .set_index("Accession")
    NoneDF[grpcol] = group_name

    if NoneDF.empty:
        sys.stderr.write(
            "Warning: NoneDF is an empty dataframe for group/sample {}\n".format(
                group_name
            )
        )
    else:
        taxNoneemptydf = NoneDF.select_dtypes(exclude=["object"])
        if not taxNoneemptydf.empty:
            sys.stderr.write(
                "Error for group/sample {}: Unexpected dtype in no GO tables\n".format(
                    group_name
                )
            )
            sys.exit(1)
        ## check that there are no na columns
        if NoneDF.isna().any().any():
            sys.stderr.write(
                "Error for group/sample {}: Unexpected NaN values in no GO tables\n".format(
                    group_name
                )
            )
            sys.exit(1)
    ## depending again on whether unique was chosen or not:
    rdscale = list(group_AllDF.head(0))[5]
    rdsum = list(group_AllDF.head(0))[4]
    ## if non-unique, there is an extra column
    if unique == False and all_ranks == False:
        rdspscale = list(group_AllDF.head(0))[6]

    if group_AllDF.empty:
        sys.stderr.write(
            "Warning: No GOs found for {}. Empty Data Frame\n".format(group_name)
        )
        ## need even an empty dataframe for gathering percentages
        if unique == False and all_ranks == False:
            group_AllDF = pd.DataFrame(
                columns=[
                    "GO_ID",
                    "Description",
                    "GO",
                    rdsum,
                    rdscale,
                    "Percent",
                    rdspscale,
                    "sp_Percent",
                    "GO_Depth",
                    "Accession_Associated",
                    "Number_of_proteins",
                    "Associated_TaxIDs",
                    "Number_of_TaxIDs",
                    grpcol,
                ]
            ).set_index("GO_ID")
        else:
            group_AllDF = pd.DataFrame(
                columns=[
                    "GO_ID",
                    "Description",
                    "GO",
                    rdsum,
                    rdscale,
                    "Percent",
                    "GO_Depth",
                    "Accession_Associated",
                    "Number_of_proteins",
                    "Associated_TaxIDs",
                    "Number_of_TaxIDs",
                    grpcol,
                ]
            ).set_index("GO_ID")
    else:
        ## get namespace percents [add columns]
        subc = 0
        for NS in ["biological_process", "molecular_function", "cellular_component"]:
            subdf = group_AllDF.loc[group_AllDF["GO"] == NS].copy()
            if subdf.empty:
                subc += 1
                sys.stderr.write(
                    "Warning: No GOs found for {} in {}.\n".format(NS, group_name)
                )

            group_AllDF.loc[group_AllDF["GO"] == NS, "Percent"] = (
                group_AllDF.loc[group_AllDF["GO"] == NS, rdscale].astype(float)
                / group_AllDF.loc[group_AllDF["GO"] == NS, rdscale].astype(float).sum()
                * 100
            )
            if unique == False and all_ranks == False:
                group_AllDF.loc[group_AllDF["GO"] == NS, "sp_Percent"] = (
                    group_AllDF.loc[group_AllDF["GO"] == NS, rdspscale].astype(float)
                    / group_AllDF.loc[group_AllDF["GO"] == NS, rdspscale]
                    .astype(float)
                    .sum()
                    * 100
                )

        group_AllDF["Percent"] = group_AllDF["Percent"].astype(object)
        if unique == False and all_ranks == False:
            group_AllDF["sp_Percent"] = group_AllDF["sp_Percent"].astype(object)
        ##reorder columns
        # if non-unique, species...there are extra columns
        if unique == False and all_ranks == False:
            group_AllDF = group_AllDF[
                [
                    "Description",
                    "GO",
                    rdsum,
                    rdscale,
                    "Percent",
                    rdspscale,
                    "sp_Percent",
                    "GO_Depth",
                    "Accession_Associated",
                    "Number_of_proteins",
                    "Associated_TaxIDs",
                    "Number_of_TaxIDs",
                    grpcol,
                ]
            ]
        else:
            group_AllDF = group_AllDF[
                [
                    "Description",
                    "GO",
                    rdsum,
                    rdscale,
                    "Percent",
                    "GO_Depth",
                    "Accession_Associated",
                    "Number_of_proteins",
                    "Associated_TaxIDs",
                    "Number_of_TaxIDs",
                    grpcol,
                ]
            ]

        ### CHECKS For Percentage addition
        if subc == 0:
            pct = 300
        elif subc == 1:
            pct = 200
        elif subc == 2:
            pct = 100
        ## pct should not equate to 3 because then df would be empty
        else:
            sys.stderr.write(
                "Error: Namespaces of dataframe of {} anomalous\n".format(group_name)
            )
            sys.exit(1)

        if round(group_AllDF["Percent"].astype(float).sum()) != pct:
            sys.stderr.write(
                "Error: Percent column in {} GO dataframe not equating to expected Percentage.\n".format(
                    group_name
                )
            )
            sys.exit(1)
        if "sp_Percent" in group_AllDF.columns.values:
            if round(group_AllDF["sp_Percent"].astype(float).sum()) != pct:
                sys.stderr.write(
                    "Error: sp_Percent column in {} GO dataframe not equating to expected Percentage.\n".format(
                        group_name
                    )
                )
                sys.exit(1)

        ## check dtypes of all tables; no point if empty dataframe
        taxemptydf = group_AllDF.select_dtypes(exclude=["object"])
        if not taxemptydf.empty:
            sys.stderr.write(
                "Error for group/sample {}: Unexpected dtype in GO tables\n".format(
                    group_name
                )
            )
            sys.exit(1)
        ## check that there are no na columns
        if group_AllDF.isna().any().any():
            sys.stderr.write(
                "Error for group/sample {}: Unexpected NaN values in GO tables\n".format(
                    group_name
                )
            )
            sys.exit(1)

    ## sort dataframes by 'Description'
    # group_AllDF = group_AllDF.sort_values(by="Description")
    # NoneDF = NoneDF.sort_index()

    if checkGO != "None":
        return group_AllDF, NoneDF, check_AllDF, withGO, withoutGO, readsGO, readsnotGO
    else:
        return group_AllDF, NoneDF, withGO, withoutGO, readsGO, readsnotGO


if __name__ == "__main__":
    sys.exit(main())
