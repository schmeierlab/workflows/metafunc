#!/usr/bin/env python

"""
NAME: Extract_MetaInfo.py
=========

DESCRIPTION
===========

This script takes Kaiju's raw output and creates two tables (see below).

1) Taxonomy ID-based tables with the following columns:
a. TaxID: The Taxonomy ID that reads are classified to based on their protein matches.
b. Taxon: Taxonomy Name of the Taxonomy ID.
c. Rank: (of TaxID/Taxon)
d. RootTaxon: Root Kingdom/Taxon of the classification. Note that this is based on a user-specified root taxon/kingdom list. 
e. AccessionNumber: Accession numbers of the protein matches of the reads classified under the TaxID.
f. Counts_per_Accession (debug option only): Number of times a particular accession number in e has matched to the reads under the TaxID; positionally congruent with e.
g. UniqueAccessions (debug option only): Total number of the different accession numbers that the reads under the TaxID have matched to; if items of e are counted, should equate to this number.
h. Number_of_reads: number of reads that matched to proteins under that TaxID (note that Kaiju uses protein matches for taxonomy classification).
i. Number_of_reads(scaled): Number of reads divided by total classified reads, multiplied by 100 (equivalent to Percent). Classified reads refer to any read that has a Taxonomy ID classification under Kaiju AND belongs to the user-specified taxon list of root taxon/kingdom.
j. Number_of_reads(sp_scaled): Refers to reads of species level only, divided by total number of species level reads, multiplied by 100.
k. Number_of_uniquely_matching_reads: Kaiju may match to more than 1 accession number (even if these multiple accession numbers match to just 1 taxon). This refers to reads that matched to only 1 protein for that TaxID.
l. Number_of_uniquely_matching_reads(scaled):Species level only. Sum of all uniquely matching reads for the species level, divided by the total number of uniquely matching reads for species level, multiplied by 100.
m. Protein_Counts_per_Read (debug option only): For each read classified to the Taxonomy ID, count of how many proteins that read has matched to.

Note on last rows of the tables:
a. Classified_Total: Sum of all reads that have been classified by Kaiju AND belong to the user-specified taxon list of root taxon/kingdom.
b. Others: All reads that have a Kaiju classification, but does not belong to the user-specified taxon list of root taxon/kingdom. 
c. Unclassified: Sum of all reads that Kaiju cannot classify.

2) Accession Number-based table with the following columns:
a. Protein Accession: Protein Accession Number. Note that there could be "identical" accession numbers but under different ranks.
b. Number_of_reads: Number of reads that matched to the accession number.
c. Number_of_reads(scaled): Number of reads divided by total classified reads, multiplied by 100. Since some reads match to more than 1 protein, will not add up to 100 in total.
d. Number_of_reads(sp_scaled): Refers to reads of species level only, divided by total number of species level reads, multiplied by 100.
e. Proportional_Reads: each read is divided by the number of protein accessions it has matched to. This count is added to towards all "proportional" read counts of each accession number.
f. Proportional_Reads(scaled): Proportional Reads divided by total classified reads, multiplied by 100 (All Proportional Reads should add up to total number of classified reads).
g. Proportional_Reads(sp_scaled): Refers to proportional reads of species level only, divided by total number of species level reads, multiplied by 100.
h. Number_of_uniquely_matching_reads: Number of reads that matched to that accession number uniquely.
i. Number_of_uniquely_matching_reads(scaled): Species level only. Sum of all uniquely matching reads under the species level, divided by the total number of uniquely matching reads for species level, multiplied by 100.
j. Associated_TaxIDs: The taxonomy IDs the accession number is associated with.
k. Associated_TaxNames: Taxonomy names of IDs in column *j*.
l. RootTaxon: root Kingdom/Taxa of the Taxonomy ID.
m. Rank: Rank of the tax ID classification of the read based on the accession numbers.

VERSION HISTORY
===============

July 09, 2019 - v.1.0.0

LICENCE
=======
2019, copyright Arielle Sulit, (a.sulit@massey.ac.nz)

"""

import csv
import pandas as pd
from collections import Counter
import argparse
import sys
import os  ##check for makedirs
from functools import reduce
import pickle as pkl
import time

# from ete3 import NCBITaxa

## If SettingwithCopyWarning is ever generated, raise error instead of warning; makes code more strict
pd.set_option("mode.chained_assignment", "raise")


def main():

    proteintable = argparse.ArgumentParser(
        description="construct 2 tables with read number information per a) protein accession and b) taxonomy IDs"
    )
    proteintable.add_argument(
        "--kaiju_file", action="store", type=str, help="Kaiju verbose output"
    )
    proteintable.add_argument(
        "--tax_config",
        action="store",
        type=str,
        help="configuration file containing taxa of interest; kaiju-taxonlistEuk.tsv or subset of; NOTE: NO HEADERS",
    )
    proteintable.add_argument(
        "--tax_dict",
        action="store",
        type=str,
        help="dictionary file containing all taxa of interest and their information",
    )
    proteintable.add_argument(
        "--str2sp",
        action="store",
        type=str,
        help="dictionary file containing all strain2species of taxa of interest",
    )
    proteintable.add_argument(
        "--taxFile",
        action="store",
        type=str,
        help="output filename for taxID based table",
    )
    proteintable.add_argument(
        "--protFile",
        action="store",
        type=str,
        help="output filename for protein based table",
    )
    proteintable.add_argument(
        "--debug",
        action="store_true",
        help="if set, will output additional informative columns",
    )

    args = proteintable.parse_args()

    overalltime1 = time.time()
    ## check if there are input and output given

    if args.kaiju_file is None:
        proteintable.error("Error: Please indicate input kaiju verbose file")
    else:
        ## reference the main input file
        csv_Kaiju = args.kaiju_file
        ## will give reasonable file name, gives the file at the end of the path, and then gives the first item when split with '.'
        filename = csv_Kaiju.split("/")[-1].split(".")[0]

    ##MAY BE OBSOLETE
    # if args.NCBITaxDB is None:
    #    proteintable.error('Please indicate sqlite database for taxonomy information. This is produced using ETE3 on taxdump.tar.gz')
    # else:
    #    try:
    ## initialize ete3 ncbitaxa package
    #        ncbi=NCBITaxa(dbfile=args.NCBITaxDB)
    #        sys.stdout.write('ncbi initialized from {}\n'.format(args.NCBITaxDB))
    #    except:
    #        sys.stderr.write('Error: problems initializing ncbi\n')
    #        sys.exit(1)

    ##Replace with taxonlist (or other list)
    if args.tax_config is None:
        proteintable.error(
            "Error: Please indicate config file with paths to taxa of interest"
        )
    else:
        ## parse through file containing path to infraspecies dictionaries
        TaxonID = {}  ## list of taxon IDs from the configuration file (integers)
        errcount = 0
        taxonfile_o = open(args.tax_config, "r")
        taxonfile = csv.reader(taxonfile_o, delimiter="\t")
        sys.stderr.write(
            "Getting Information from the classifications under the following taxa:\n"
        )
        try:
            for taxon_num in taxonfile:
                TaxonID[str(taxon_num[0])] = None
                sys.stderr.write("{}\n".format(taxon_num[1].strip()))
        except IndexError:
            errcount += 1
            sys.stderr.write(
                "IndexError: {}. Possibly an empty line in input taxon file. This is ignored; please check stderr file to confirm that all taxa wanted was included.\n".format(
                    errcount
                )
            )
        taxonfile_o.close()
    # print(TaxonID)
    ## Open groups.pickles (Kingdom)
    if args.tax_dict is None:
        proteintable.error(
            "Error: Please indicate dictionary with Information on Taxonomies"
        )
    else:
        ## open dictionary of taxon information
        kingt1 = time.time()
        kingsO = open(args.tax_dict, "rb")
        Kingdom = pkl.load(kingsO)
        kingsO.close()
        kingt2 = time.time()
        sys.stdout.write(
            "Loaded Kingdoms Dictionary in {} secs\n".format(kingt2 - kingt1)
        )
        sys.stdout.flush()

    if args.str2sp is None:
        proteintable.error("Error: Please indicate dictionary of strain to species")
    else:
        t1 = time.time()
        ## load dictionary containing taxids of species and infraspecies
        infraspO = open(args.str2sp, "rb")
        infrasp_dicts = pkl.load(infraspO)
        infraspO.close()
        t2 = time.time()
        sys.stdout.write(
            "Loaded strain2species dictionary in {} secs\n".format(t2 - t1)
        )
        sys.stdout.flush()

    if args.taxFile is None and args.protFile is None:
        try:
            if not os.path.exists("ProtTaxFiles"):
                os.makedirs("ProtTaxFiles")
        except OSError:
            sys.stderr.write("Error creating ProtTaxFiles directory\n")
        taxout = "ProtTaxFiles/" + filename + ".taxID.tsv"
        protout = "ProtTaxFiles/" + filename + ".protacc.tsv"
    elif args.taxFile is None and args.protFile is not None:
        proteintable.error("Error: please indicate output file for taxID based tables")
    elif args.taxFile is not None and args.protFile is None:
        proteintable.error("Error: please indicate output file for protein based table")
    else:
        taxout = args.taxFile
        protout = args.protFile

    ## call by_protein function which will parse through the kaiju raw file to generate final tables wanted
    TaxID_AllDF, Prot_AllDF, Percentage = by_protein(
        csv_Kaiju, infrasp_dicts, Kingdom, TaxonID
    )
    # print(TaxID_AllDF)
    # print(Prot_AllDF)
    if not args.debug:
        TaxID_AllDF = TaxID_AllDF.drop(
            columns=[
                "Counts_per_Accession",
                "UniqueAccessions",
                "Protein_Counts_per_Read",
            ]
        ).copy()

    sys.stdout.write(
        "For {}, percentage of unique reads for species matches is {}%\n".format(
            filename, Percentage
        )
    )
    sys.stdout.write("----------\n")
    TaxID_AllDF.to_csv(taxout, sep="\t")
    sys.stdout.write("write {} TaxIDs to {}\n".format(filename, taxout))
    Prot_AllDF.to_csv(protout, sep="\t")
    sys.stdout.write("write {} Protein Accessions to {}\n".format(filename, protout))

    overalltime2 = time.time()
    sys.stdout.write(
        "Time to run {} for protein and taxID tables: {} secs\n".format(
            filename, overalltime2 - overalltime1
        )
    )

    # print(TaxID_AllDF)
    # print(Prot_AllDF)
    return


## kingdom should be changed to Kingdom (although since this is function, can be retained)
def by_protein(kaiju, infrasp, kingdom, TaxonID):

    ## open and read Kaiju input file
    KaijuFile = open(kaiju, "r")
    Kaiju = csv.reader(KaijuFile, delimiter="\t")  ## Kaiju raw output has no header
    # filename=kaiju.split('/')[-1].split('.')[0]

    ## separate each read (line) by their ranks; will give dictionary such as {'species':[[line1], [line2]], 'phylum':[[line4], [line8]], etc}
    ## IMPORTANT TO SEPARATE AND GROUP READS BY RANK: protein based table will have accession numbers as "index" on dictionaries used for pandas, but accession numbers can have different ranks:
    ## depends on final taxID they are classified to, and if that taxID has many accession matching to it, it is likely to be a higher rank (and different ranks at that depending on the other matches) while the same accession ID could be
    ## matched to a final taxID that is species/strain level (and if it is, would be the proteins we actually do want)
    ## for TaxID based table, even if by rank, no TaxID should be doubled because ranks are unique to the TaxIDs (i.e. a TaxID cannot be present in two different ranks)

    ## initialize by_rank dictionary with species rank
    by_rank = {"species": []}
    ## initialize dictionary to contain Others (not taxon of interest) and Unclassified reads, and the total classified
    NoProt = {"Classified_Total": 0, "Others": 0, "Unclassified": 0}
    ## initialize to get count of total input reads
    allreads = 0
    # count=0
    for read in Kaiju:  ## because each line in Kaiju is a read
        ## add to total input reads
        allreads += 1
        ## only get classified reads
        if read[0] == "C":
            # count+=1
            ## only get reads which are under taxa of interest given above (in kingdom/Kingdom dictionary); from csv, read[2] is type string, Kingdom keys are also string
            if read[2] in kingdom and kingdom[read[2]][3] in TaxonID:
                # print(read)
                ## count as classified (technically anything under C is classified but if not under taxa of interest, bin it into the count of 'Others')
                NoProt["Classified_Total"] = NoProt["Classified_Total"] + 1
                ## everything species and below are classified under species (these should be included in infrasp dictionary)
                if read[2] in infrasp:
                    ## append entire row to rank (species) because will parse through this value by value later
                    by_rank["species"].append(read)
                ## if not species level:
                else:
                    ## get rank of the taxID that the read matched to; will always initialize rank variable if not species
                    level = kingdom[read[2]][1]
                    ## if that particular rank is not yet in the by_rank dictionary as key, initialize rank with empty list; if present, then skip to append
                    if level not in by_rank:
                        by_rank[level] = []
                    ## append the row to its rank; if new rank, will initialize ranks first as above, then append row
                    by_rank[level].append(read)
            else:
                ## bin counts of everything else not under taxa of interest into 'Others'
                NoProt["Others"] = NoProt["Others"] + 1
                ## these are known higher levels that our reads may match to (1 is root, 131567 is cellular organisms, 2759 is eukaryota, and 33154 is opisthokonta)
                # if read[2] not in ['1', '131567', '2759', '33154']:
                #    sys.stderr.write('UNEXPECTED OTHERS TAXID: {}'.format(read[2]))
                #    sys.stderr.flush()
        elif read[0] == "U":
            ## get counts of unclassified proteins
            NoProt["Unclassified"] = NoProt["Unclassified"] + 1
        ## should Kaiju have a first column that is not C or U, will give an error
        else:
            sys.stderr.write(
                "Error: Kaiju first column is giving out unexpected string (i.e. not C or U). Exiting...\n"
            )
            sys.exit(1)

    ## close opened KaijuFile
    KaijuFile.close()

    Scaling_Total = NoProt["Classified_Total"]
    ## Scaling_Total is an integer, to be safe convert to float in an operation
    if int(Scaling_Total) != int(0):
        SF = (1 / float(Scaling_Total)) * 100
    else:
        sys.stderr.write("Error: You have no classified reads\n")
        sys.exit(1)

    ##convert integer values of NoProt dictionary to string
    NoProtSr = {}
    for noprot in NoProt:
        NoProtSr[noprot] = str(NoProt[noprot])

    ## initialize series and dataframe for 'Classified_Total', 'Others' and 'Unclassified' reads; will be used later on in final data frame
    # NoProtSr=pd.Series(NoProt)
    ## not done for protein table anymore because noninformative
    # NoProtDF=pd.DataFrame({'ProteinAccession':pd.Series(NoProtSr).index, 'Number_of_reads':pd.Series(NoProtSr).values})
    # NoProtDF=NoProtDF.set_index('ProteinAccession')

    ## initialize series and dataframe for 'Classified_Total', 'Others' and 'Unclassified' reads; will be used later on in final data frame
    OthersDF = pd.DataFrame(
        {
            "TaxID": pd.Series(NoProtSr, dtype=object).index,
            "Number_of_reads": pd.Series(NoProtSr, dtype=object).values,
        }
    )
    OthersDF = OthersDF.set_index("TaxID")

    ## NoProt dictionaries not necessary anymore
    del (NoProt, NoProtSr)

    Prot_AllDF = pd.DataFrame()
    TaxID_AllDF = pd.DataFrame()
    # count=0
    ## will go through each dictionary item by its ranks, each value of a rank is a list of lists that are the indiviudal rows of reads in Kaiju output
    # dictionaries used for the dataframes are reinitialized each time rank is changed
    # a dataframe per rank is constructed and concatenated to the dataframe of another rank
    for rank in by_rank:  ##by_rank[rank] is READ
        ### initialize dictionaries of the following:
        ## number of reads per taxon ID identified; key is TaxonID, value is number of reads classified as that Taxon ID)
        ReadCounts = {}
        ## individual reads that matched to that taxon ID and number of proteins that the translated read has matched to; key is Taxon ID and value
        #  is list of [Read#: #proteins matched to] (e.g. R1:1, R2:1, etc)
        ReadList = {}
        ## all proteins associated with a particular TaxonID; key is Taxon ID and value is a list of the proteins
        ProteinList = {}
        ## a subset of the TaxIDs with the number of reads uniquely matching to 1 protein from the TaxID;
        # key is Taxon ID, value is the number of reads classified to the taxon ID protein (any protein) uniquely
        ReadsUniqueProt = {}
        ## list of all TaxIDs in a rank (may be duplicated, will be list(set()) later on)
        Taxonomies = []
        ### dictionaries and list above for TaxID table, dictionaries below are for protein table
        ## number of reads per protein accession identified;
        # key is protein accession number and value is the number of reads that matched to that accession number (1 read may match to multiple accessions)
        ProtReads = {}
        ## number of reads that matched uniquely to that protein; like ReadsUniqueProt with accession number as key and number of uniquely matching reads as value
        UniqueReads = {}
        ## depending on the number of proteins read has matched to, divide 1/number. Note, if all prop reads is added, should still be equal to total reads
        PropReads = {}
        ## depends on the rank (dictionary key) to segregate our protein matches by their rank (rank is also TaxID dependent so will be done separately for TaxID based table)
        # key is protein accession, value is rank (same for all accession numbers in a rank key)
        Ranks = {}
        ## TaxID of the read matching to that protein accession number
        # usually just 1 because a read has only 1 taxID; if it has multiple accession numbers however, all these accession numbers are annotated with the TaxID
        TaxIDs = {}
        ## add scaling factors at the species level (variables should only be set at species rank in loop)
        if rank == "species":
            ## each item in READ is equivalent to a read in kaiju;
            # if we are at species ranking, the number of items in READ should be equivalent to the number of reads under species ranking
            SFsp = len(by_rank[rank])  ## should be an integer
            SF_unique = 0
        ## equivalent to for line in kaiju_file
        for kjrow in by_rank[rank]:
            ## initialize variable ID for each read
            ## any taxon ID that is under species level is matched to its species instead (species:self, strain:species);
            # kjrow[2] is a string so ID is a string
            if kjrow[2] in infrasp:
                ID = str(infrasp[kjrow[2]])
                ## should a taxonomy ID under a non-species rank be found in infrasp, an error occurs; should not happen because filtered above but to be safe...
                if rank != "species":
                    sys.stderr.write(
                        "Error: Taxonomy ID {} was found in infra-species dictionary but under rank{}\n".format(
                            kjrow[2], rank
                        )
                    )
                    sys.exit(1)
            ## otherwise is self
            else:
                ID = str(kjrow[2])
            ## populate the TaxID list
            Taxonomies.append(ID)
            ## populate ReadCounts, ProteinList, and ReadList dictionaries
            # since all started empty will be populated simultaneously;
            # i.e. if it wasn't in ReadCounts shouldn't be in others either and if it was in ReadCounts, should also be in others
            if ID not in ReadCounts:
                ReadCounts[ID] = 0
                ProteinList[ID] = []
                ReadList[ID] = []
            ## increase count for every matching TaxID
            ReadCounts[ID] = ReadCounts[ID] + 1
            ## set variable for ith read (i.e. R1, R2...R17) depending on how many taxIDs have been found (dependent on number of read counts so far for that ID)
            ReadNum = "R" + str(ReadCounts[ID])
            ## get a list of all proteins under the 6th column (becomes a new list each read)
            #  since list of accessions in this column ends in ',', will give a '' as protein and not to be used or counted at all
            Accession = [p.strip() for p in kjrow[5].split(",") if p.strip() != ""]
            ## get number of proteins that read matched to
            proteincount = len(Accession)
            for x in Accession:
                if x == "":
                    sys.stderr.write('Error: " " is listed as protein. Exiting\n')
                    sys.exit(1)
                else:
                    ## get a list of all accession numbers that have matched to that taxID (may have doubled accession numbers, will be counted later)
                    ProteinList[ID].append(x)
                    ## for the protein based table meanwhile, initialize dictionaries
                    # populate ProtReads, TaxIDs, and Ranks simultaneously
                    # since all started empty will be populated simultaneously;
                    if x not in ProtReads:
                        ## initialize tax IDs that matched to protein
                        TaxIDs[x] = []
                        ## initialize counts of reads matching to particular protein
                        ProtReads[x] = 0
                        ## this is dependent on which rank we are in; but the same answer for all proteins under this loop's rank
                        # if accession number is found more than once in a rank, will not be added anymore because information is already there
                        Ranks[x] = rank
                        ## initialize proportional read count
                        PropReads[x] = 0
                    ##get proportional read count for each protein
                    propcount = 1 / float(proteincount)
                    ## add proportional read count for the accession number
                    PropReads[x] = PropReads[x] + propcount
                    ## increase count for every accession number the read matched to in a rank
                    ProtReads[x] = ProtReads[x] + 1
                    ## for the accession number, append taxIDs it matched to under a rank;
                    # should theoretically be only 1 because an accession number is tied to a strain/species which should only have 1 lineage (except perhaps some instances of 'no rank' as 'no rank' can be any level in that lineage)
                    TaxIDs[x].append(ID)
            ## if there is only one valid protein in the Accession (read is uniquely matched to a protein), we want to get this information
            if proteincount == 1:
                ## count how many reads at species level there are that matched to only 1 protein
                # note that there are higher ranking reads that could have matched to 1 protein (most probably this protein is classified to a higher ranking TaxID based on Kaiju's algorithm) but this will be disregarded
                # will get scaling factor of uniquely matching reads at species level
                if rank == "species":
                    SF_unique += 1
                ## populate separate dictionary of TaxIDs:Counts and these are only for those that matched to only 1 protein
                if ID not in ReadsUniqueProt:
                    ReadsUniqueProt[ID] = 0
                ReadsUniqueProt[ID] = ReadsUniqueProt[ID] + 1
                for prot in Accession:
                    ## since list of accession in this column ends in ',', will give a '' as protein and not to be used or counted at all
                    if prot == "":
                        sys.stderr.write('Error: " " is listed as protein. Exiting\n')
                        sys.exit(1)
                    else:
                        ## populate separate Dictionary of accession number: read counts for those reads that matched to only 1 protein
                        if prot not in UniqueReads:
                            UniqueReads[prot] = 0
                        UniqueReads[prot] = UniqueReads[prot] + 1
            ## get the number of proteins that ith read matched to
            ProteinNum = str(proteincount)
            ## populate ReadList dictionary with the number of proteins ith read has matched to
            ReadList[ID].append(ReadNum + ":" + ProteinNum)

        ## at this point, dictionaries are populated by information on each read in a rank; will use dictionaries to end up with dataframes per rank

        ## for rank, READ in by_ranks.items():

        ### the following is for TaxID based protein table

        ## get unique values for TaxID
        Taxonomies = list(set(Taxonomies))

        TaxInfo = {}
        info = 0
        for taxono in Taxonomies:
            ##get name, rank, root (except for root taxID)
            inform = kingdom[taxono][:-1]
            inform.append(taxono)
            info += 1
            TaxInfo[info] = inform

        ## translate TaxID to name; will give dictionary; note that Taxonomies are a list of strings
        # NCBI taxa appears to be able to use "strings" of taxonomy IDs but to be safe (as it gives int to string dictionaries anyway):
        # TaxName=ncbi.get_taxid_translator([int(y) for y in Taxonomies])
        ## initialize to set keys of TaxName dictionary to string (having problems with DF if integer format)
        # TaxName2={}
        # for taxono in TaxName:
        ## convert integer key to string
        #    TaxName2[str(taxono)]=TaxName[taxono]

        ##TaxName no longer necessary; will be re-initialized on the next rank loop
        # del(TaxName)

        ## dependent on which rank we are in in the loop; same answer for all
        ## Taxonomies is a unique set of IDs in a particular rank, and this just puts it into a taxID: rank format to facilitate change to dataframe
        # TaxRanks={}
        # for elem in Taxonomies:
        #    TaxRanks[elem]=rank

        ##get the root kingdom/superkingdom of each TaxonID and place in dictionary (string : string)
        # TaxLine={}
        # for Id in Taxonomies:
        #    TaxLine[Id]=kingdom[int(Id)]

        ## Taxonomies list not necessary anymore, will re-initialize next loop
        del Taxonomies

        ## For each TaxID, multiple reads give different and even multiple accession numbers; want to count how many of these there are
        ProteinCount = {}
        ## ProteinList above has TaxID as key and list of accession as values; note that accession may be replicated
        for num in ProteinList:
            ## get unique list of accession and count how many instances there are;
            # note that number of reads compared to number of proteins may not add up because 1 read may have multiple proteins
            # Counter (therefore valcount) gives a "counter" of accession: and number of times accession was found (integer)
            valcount = dict(Counter(ProteinList[num]))
            ## convert counter to dictionary format
            # valcount=dict(valcount)
            ## populate ProteinCount dictionary with the TaxID as keys, and dictionary of accession:count as their values
            ProteinCount[num] = valcount

        ## ProteinList not necessary, will re-initialize next loop
        del ProteinList

        ### separate protein count into dictionaries
        ## create dictionaries to map taxID to its accessions and another dictionary to map taxIDs to the corresponding counts of the accessions
        TaxIDAcc = {}
        ## initialize TaxID to counts of each accession number
        TaxIDCount = {}
        ## iterate through ProteinCount dictionary
        for org in ProteinCount:
            ## for each TaxID (org [key]); note, as this was a key in a previous dictionary, should be unique
            # since both started empty, populate TaxIDAcc and TaxIDCount simultaneously
            if org not in TaxIDAcc:
                TaxIDAcc[org] = []
                TaxIDCount[org] = []
            else:
                sys.stderr.write("Error: Double Keys for ProteinCount dictionary\n")
                sys.exit(1)
            ## for each dictionary value of the ProteinCount dictionary
            for acc, account in ProteinCount[org].items():
                ## TaxIDAcc and TaxIDCount values (list) should be corresponding (Is there a way to check this? manually done but not sure if will always happen)
                TaxIDAcc[org].append(acc)
                ##Use unique accession (key2) as value for each TaxID
                TaxIDCount[org].append(str(account))

        ## get counts of how many unique accession numbers there are per TaxID
        AccessionCount = {}
        for orga in TaxIDAcc:
            ## populate AccessionCount dictionary
            AccessionCount[orga] = str(len(TaxIDAcc[orga]))
            ## make a string out of the list of values of the sorted TaxIDAcc dictionary
            TaxIDAcc[orga] = ", ".join(sorted(TaxIDAcc[orga]))

        ## just make a string out of the list of values of the dictionaries
        for o in TaxIDCount:
            TaxIDCount[o] = ", ".join(TaxIDCount[o])
        for tid in ReadList:
            ReadList[tid] = ", ".join(ReadList[tid])

        ## ProteinCount dictionary not necessary anymore, will be re-initialized on next loop
        del ProteinCount

        ### create dictionaries of the scaled counts
        ## scaled_reads are reads scaled to the total number of reads classified under taxa of interest
        scaled_reads = {}
        ## scaled_sp are species level reads scaled to total number of reads at the species level
        scaled_sp = {}
        for TID in ReadCounts:
            ## for rank at species, get a species scaled value for the read count
            # if empty division will not occur
            if rank == "species":
                scaled_sp[TID] = str((ReadCounts[TID] / float(SFsp)) * 100)
            ## species scaling is not important for other ranks
            else:
                ## always 'NA' because ReadCount is always present. Just would not scale if not species
                scaled_sp[TID] = "NA"
            ## get scaled reads (against total reads classified under taxa of interest)
            # if no classified (divison won't happen)
            scaled_reads[TID] = str(ReadCounts[TID] * SF)
            ## ReadCounts dictionary's values are integers; transform to string for dataframe
            ReadCounts[TID] = str(ReadCounts[TID])

        ## same for the subset of reads matching to only 1 protein
        scaled_unique = {}
        for i_d in ReadsUniqueProt:
            ## only scale species level uniquely matching reads (scaling factor only considers species anyway, otherwise scaling factor is 0)
            # if empty, no division to occur
            if rank == "species":
                scaled_unique[i_d] = str(
                    (ReadsUniqueProt[i_d] / float(SF_unique)) * 100
                )
            else:
                ## if not species, not interested in scaling; SF_unique at any rate only considers species
                # possible to have some TaxIDs with no uniquely matching reads which will give NaN for both uniquely matching reads column and its scaled counterpart
                # will fill with 0 (fillna) because uniquely matching reads column should have 0s for read count (will also give 0s for scaled counterpart);
                # since I don't want to mix up 'NAs' or '0s' in a column, convert '0s' of non-species ranks later on for scaled uniquely matching reads
                scaled_unique[i_d] = "0"
            ReadsUniqueProt[i_d] = str(ReadsUniqueProt[i_d])
        ## make data frames out of the dictionaries  (convert these to series first); up to now everything is in string type
        # NamesDF=pd.DataFrame({'TaxID':pd.Series(TaxName2).index, 'Taxon':pd.Series(TaxName2).values})
        # RankDF=pd.DataFrame({'TaxID':pd.Series(TaxRanks).index, 'Rank':pd.Series(TaxRanks).values})
        # LinesDF=pd.DataFrame({'TaxID':pd.Series(TaxLine).index, 'Kingdom':pd.Series(TaxLine).values})
        InfoDF = pd.DataFrame.from_dict(
            TaxInfo,
            orient="index",
            columns=["Taxon", "Rank", "RootTaxon", "TaxID"],
            dtype=object,
        )
        AccessionDF = pd.DataFrame(
            {
                "TaxID": pd.Series(TaxIDAcc, dtype=object).index,
                "AccessionNumber": pd.Series(TaxIDAcc, dtype=object).values,
            }
        )
        CountsDF = pd.DataFrame(
            {
                "TaxID": pd.Series(TaxIDCount, dtype=object).index,
                "Counts_per_Accession": pd.Series(TaxIDCount, dtype=object).values,
            }
        )
        AccCountDF = pd.DataFrame(
            {
                "TaxID": pd.Series(AccessionCount, dtype=object).index,
                "UniqueAccessions": pd.Series(AccessionCount, dtype=object).values,
            }
        )
        ReadCountsDF = pd.DataFrame(
            {
                "TaxID": pd.Series(ReadCounts, dtype=object).index,
                "Number_of_reads": pd.Series(ReadCounts, dtype=object).values,
            }
        )
        ScaledReadsDF = pd.DataFrame(
            {
                "TaxID": pd.Series(scaled_reads, dtype=object).index,
                "Number_of_reads(scaled)": pd.Series(scaled_reads, dtype=object).values,
            }
        )
        SpScaledReadsDF = pd.DataFrame(
            {
                "TaxID": pd.Series(scaled_sp, dtype=object).index,
                "Number_of_reads(sp_scaled)": pd.Series(scaled_sp, dtype=object).values,
            }
        )
        ReadProtDF = pd.DataFrame(
            {
                "TaxID": pd.Series(ReadList, dtype=object).index,
                "Protein_Counts_per_Read": pd.Series(ReadList, dtype=object).values,
            }
        )
        UniqueDF = pd.DataFrame(
            {
                "TaxID": pd.Series(ReadsUniqueProt, dtype=object).index,
                "Number_of_uniquely_matching_reads": pd.Series(
                    ReadsUniqueProt, dtype=object
                ).values,
            }
        )
        ScaledUniqueDF = pd.DataFrame(
            {
                "TaxID": pd.Series(scaled_unique, dtype=object).index,
                "Number_of_uniquely_matching_reads(scaled)": pd.Series(
                    scaled_unique, dtype=object
                ).values,
            }
        )

        ## write all dataframes into a list
        df_list = [
            InfoDF,
            AccessionDF,
            CountsDF,
            AccCountDF,
            ReadCountsDF,
            ScaledReadsDF,
            SpScaledReadsDF,
            UniqueDF,
            ScaledUniqueDF,
            ReadProtDF,
        ]

        ##since dictionaries have now been made into dataframes, delete them; will re-initialize in next loop
        del (
            TaxInfo,
            TaxIDAcc,
            TaxIDCount,
            AccessionCount,
            ReadCounts,
            scaled_reads,
            scaled_sp,
            ReadList,
            ReadsUniqueProt,
            scaled_unique,
        )

        ## merge all dataframes into 1 dataframe
        TaxIDDF = reduce(
            lambda left, right: pd.merge(left, right, on="TaxID", how="outer"), df_list
        )

        ## check for unexpected NAs; if empty (i.e. no species, should still be okay because no NaNs would be detected)
        # should not have for 'TaxID', 'Number_of_reads', ''Number_of_reads(scaled)', 'Number_of_reads(sp_scaled)', 'Rank', 'AccessionNumber', 'Taxon', 'Kingdom', 'Counts_per_Accession', 'UniqueAccessions', 'Protein_Counts_per_Read'
        taxnonacolumns = [
            "TaxID",
            "Number_of_reads",
            "Number_of_reads(scaled)",
            "Number_of_reads(sp_scaled)",
            "Rank",
            "AccessionNumber",
            "Taxon",
            "RootTaxon",
            "Counts_per_Accession",
            "UniqueAccessions",
            "Protein_Counts_per_Read",
        ]
        taxnacolumns = TaxIDDF.columns[TaxIDDF.isna().any()].tolist()
        for taxcol in taxnacolumns:
            if taxcol in taxnonacolumns:
                sys.stderr.write(
                    "Error: Unexpected NAs in {} column of taxID file\n".format(taxcol)
                )
                sys.exit(1)
        ## set_index to TaxID and fill all NA's with 0
        TaxIDDF = TaxIDDF.set_index("TaxID").fillna("0")

        ## for each rank, merge into 1 complete dataframe
        TaxID_AllDF = pd.concat([TaxID_AllDF, TaxIDDF])

        ## check for dataframe dtype expectations (I used strings for all elements in dataframes so should be object)
        taxemptydf = TaxID_AllDF.select_dtypes(exclude=["object"])
        if not taxemptydf.empty:
            sys.stderr.write("Error: Unexpected dtype in taxID table\n")
            sys.exit(1)

        ##This ends what's necessary to get taxID based protein table

        ##the following commands are necessary for the protein based table

        ## for rank, READ in by_ranks.items():

        ## get kingdom name and taxon name for each unique taxID a protein has matched to in a rank
        TaxNames = {}
        Taxkingdom = {}
        for accessions in TaxIDs:
            ## first, convert TaxID list of an accesion to unique values (should theoretically be 1 for most, esp species level but could have some that are not, especially for no rank)
            # sort the unique values
            TaxIDs[accessions] = sorted(list(set(TaxIDs[accessions])))
            ## expected (and necessary) that for an accession, it should only have 1 TaxID at the species level (if it "matched" to both species and a strain in Kaiju, only species level is given)
            # give an error if this is not met
            if rank == "species":
                if len(TaxIDs[accessions]) > 1:
                    sys.stderr.write(
                        "Error: Accession {} belongs to more than 1 species: {}\n".format(
                            accessions, ", ".join(TaxIDs[accessions])
                        )
                    )
                    sys.exit(1)
            ## names is a dictionary of taxonomy ID: name, integer:string
            # not using NCBI taxa anymore: NCBI taxa appears to be able to use "strings" of taxonomy IDs but to be safe (as it gives int to string dictionaries anyway):
            names = []
            for entry in TaxIDs[accessions]:
                # theoretically, same order as TaxIDs[accessions]
                names.append(kingdom[entry][0])
            ## populate TaxNames dictionary with protein accession numbers as key and the corresponding names of the TaxIDs the proteins belong to (convert the list to string)
            TaxNames[accessions] = ", ".join(names)
            ## get a list of the root kingdoms of each of the Taxonomy IDs (get unique ones and raise an error if more than 1 value; it is not expected that 1 accession would belong to two different kingdoms)
            # even if there is more than 1 taxID (esp no rank, should be under 1 kingdom only)
            kingdoms = []
            for c in TaxIDs[accessions]:
                kingdoms.append(kingdom[c][2])
            kingdoms = list(set(kingdoms))
            if len(kingdoms) > 1:
                sys.stderr.write(
                    "Error: Accession {} is under more than 1 root kingdom.\n".format(
                        accessions
                    )
                )
                sys.exit(1)
            else:
                ## give string of the kingdoms
                Taxkingdom[accessions] = ", ".join(kingdoms)
            # after everything else with TaxIDs dictionary has been used, convert the list values into a string
            TaxIDs[accessions] = ", ".join(TaxIDs[accessions])

        ## from the prot reads dictionary, make a dictionary converting read counts to scaled counts by multiplying with scaling factor
        ProtReads_Scaled = {}
        ProtReads_Scaledsp = {}
        for protina in ProtReads:
            if rank == "species":
                ## scale species level read counts to species level read count total
                ProtReads_Scaledsp[protina] = str(
                    (ProtReads[protina] / float(SFsp)) * 100
                )
            else:
                ## if not at species level, no values
                # always 'NA' because ReadCount is always present. Just would not scale if not species
                ProtReads_Scaledsp[protina] = "NA"
            ## scale for all reads classified to taxa of interest
            ProtReads_Scaled[protina] = str(ProtReads[protina] * SF)
            ## after done with the read counts per accession, convert to string
            ProtReads[protina] = str(ProtReads[protina])

        # same reasoning as above for read counts scaling (theoretically PropReads are always present )
        scaledProp = {}
        scaled_spProp = {}
        for propo in PropReads:
            ## for rank at species, get a species scaled value for the prop count
            # if empty won't divide
            if rank == "species":
                scaled_spProp[propo] = str((PropReads[propo] / float(SFsp)) * 100)
            ## species scaling is not important for other ranks
            else:
                ## always 'NA' because proportional count is always present. Just would not scale if not species
                scaled_spProp[propo] = "NA"
            # get scaled reads (against total reads classified under taxa of interest)
            # if empty no scaling will happen
            scaledProp[propo] = str(PropReads[propo] * SF)
            ## from PropReads dictionary, convert values to string
            PropReads[propo] = str(PropReads[propo])

        ## from unique reads dictionary, make dictionary converting read counts to scaled counts (of species level, uniquely matching reads)
        UniqueReads_Scaled = {}
        for protin in UniqueReads:
            ## only scale uniquely matching reads at the species level; scaling factor should only include species level anyway
            # if empty no scaling will happen
            if rank == "species":
                UniqueReads_Scaled[protin] = str(
                    (UniqueReads[protin] / float(SF_unique)) * 100
                )
            else:
                ## if not species, not interested in scaling; SF_unique at any rate only considers species
                # possible to have some accessions with no uniquely matching reads which will give NaN for both uniquely matching reads column and its scaled counterpart
                # will fill with 0 (fillna) because uniquely matching reads column should have 0s for read count (will also give 0s for scaled counterpart);
                # since I don't want to mix up 'NAs' or '0s' in a column, convert '0s' of non-species ranks later on for scaled uniquely matching reads
                UniqueReads_Scaled[protin] = "0"
            ## after done with the uniquely matching reads count, convert to string
            UniqueReads[protin] = str(UniqueReads[protin])

        ## make data frames out of the dictionaries; convert to series first
        PropReadsDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(PropReads, dtype=object).index,
                "Proportional_Reads": pd.Series(PropReads, dtype=object).values,
            }
        )
        scaledPropDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(scaledProp, dtype=object).index,
                "Proportional_Reads(scaled)": pd.Series(
                    scaledProp, dtype=object
                ).values,
            }
        )
        scaled_spPropDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(scaled_spProp, dtype=object).index,
                "Proportional_Reads(sp_scaled)": pd.Series(
                    scaled_spProp, dtype=object
                ).values,
            }
        )
        ProtReadsDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(ProtReads, dtype=object).index,
                "Number_of_reads": pd.Series(ProtReads, dtype=object).values,
            }
        )
        ProtRdScDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(ProtReads_Scaled, dtype=object).index,
                "Number_of_reads(scaled)": pd.Series(
                    ProtReads_Scaled, dtype=object
                ).values,
            }
        )
        ProtRdScSpDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(ProtReads_Scaledsp, dtype=object).index,
                "Number_of_reads(sp_scaled)": pd.Series(
                    ProtReads_Scaledsp, dtype=object
                ).values,
            }
        )
        UniqueReadsDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(UniqueReads, dtype=object).index,
                "Number_of_uniquely_matching_reads": pd.Series(
                    UniqueReads, dtype=object
                ).values,
            }
        )
        UniqueRdScDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(UniqueReads_Scaled, dtype=object).index,
                "Number_of_uniquely_matching_reads(scaled)": pd.Series(
                    UniqueReads_Scaled, dtype=object
                ).values,
            }
        )
        RanksDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(Ranks, dtype=object).index,
                "Rank": pd.Series(Ranks, dtype=object).values,
            }
        )
        TaxIDsDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(TaxIDs, dtype=object).index,
                "Associated_TaxIDs": pd.Series(TaxIDs, dtype=object).values,
            }
        )
        TaxNamesDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(TaxNames, dtype=object).index,
                "Associated_TaxNames": pd.Series(TaxNames, dtype=object).values,
            }
        )
        TaxKingdomDF = pd.DataFrame(
            {
                "ProteinAccession": pd.Series(Taxkingdom, dtype=object).index,
                "RootTaxon": pd.Series(Taxkingdom, dtype=object).values,
            }
        )

        ## gather all dataframes to a list that will be merged
        DF_list = [
            ProtReadsDF,
            ProtRdScDF,
            ProtRdScSpDF,
            PropReadsDF,
            scaledPropDF,
            scaled_spPropDF,
            UniqueReadsDF,
            UniqueRdScDF,
            TaxIDsDF,
            TaxNamesDF,
            TaxKingdomDF,
            RanksDF,
        ]

        ##since dictionaries have now been made into dataframes, delete them; will re-initialize in next loop
        del (
            PropReads,
            ProtReads,
            ProtReads_Scaled,
            ProtReads_Scaledsp,
            UniqueReads,
            UniqueReads_Scaled,
            Ranks,
            TaxIDs,
            TaxNames,
            Taxkingdom,
        )
        ## merge all dataframes to DF_name
        DF_name = reduce(
            lambda left, right: pd.merge(
                left, right, on="ProteinAccession", how="outer"
            ),
            DF_list,
        )

        ## check for unexpected NAs; if empty (i.e. species is empty, empty list for NAs is given)
        # should not have for 'ProteinAccession', 'Number_of_reads', ''Number_of_reads(scaled)', 'Number_of_reads(sp_scaled)', Rank', 'Associated_TaxIDs', 'Associated_TaxNames', 'Associated_Kingdom'
        protnonacolumns = [
            "ProteinAccession",
            "Proportional_Reads",
            "Proportional_Reads(scaled)",
            "Proportional_Reads(sp_scaled)",
            "Number_of_reads",
            "Number_of_reads(scaled)",
            "Number_of_reads(sp_scaled)",
            "Rank",
            "Associated_TaxIDs",
            "Associated_TaxNames",
            "RootTaxon",
        ]
        protnacolumns = DF_name.columns[DF_name.isna().any()].tolist()
        for protcol in protnacolumns:
            if protcol in protnonacolumns:
                sys.stderr.write(
                    "Error: Unexpected NAs in {} column of protfile\n".format(protcol)
                )
                sys.exit(1)

        ## set index of DF Name
        DF_name = DF_name.set_index("ProteinAccession").fillna("0")

        ## for each rank, merge into 1 complete dataframe
        Prot_AllDF = pd.concat([Prot_AllDF, DF_name])

        ## check for dataframe dtype expectations (I used strings for all elements in dataframes so should be object)
        protemptydf = Prot_AllDF.select_dtypes(exclude=["object"])
        if not protemptydf.empty:
            sys.stderr.write("Error: Unexpected dtype in protfile table\n")
            sys.exit(1)

    Prot_AllDF = Prot_AllDF.sort_values(
        by=["ProteinAccession", "Rank"]
    )  ##sort the dataframe by 'ProteinAccession' column (so those with same accessions will be next to each other), should have different ranks'; to keep order, sort by rank after sorting by accession
    TaxID_AllDF = TaxID_AllDF.sort_values(
        by=["Taxon", "Rank"]
    )  ## sort dataframe by 'Taxon' Names and to keep order, sort by rank after sorting by accession
    ## convert uniquely matching reads (scaled) for all non-species ranks to 'NA' from '0'; should have already been done for Number_of_reads(sp_scaled) above
    # (Prot_AllDF['Rank'] != 'species') & (Prot_AllDF['Number_of_uniquely_matching_reads(scaled)']=='0') gives "True" to rows fitting the condition, 'Number_of_uniquely_matching_reads(scaled)' is the column of interest of these rows
    # .loc "grabs" these cells and replaces the values inside with 'NA
    Prot_AllDF.loc[
        (Prot_AllDF["Rank"] != "species")
        & (Prot_AllDF["Number_of_uniquely_matching_reads(scaled)"] == "0"),
        "Number_of_uniquely_matching_reads(scaled)",
    ] = "NA"
    ##not done anymore because done above
    # Prot_AllDF.loc[(Prot_AllDF['Rank'] != 'species') & (Prot_AllDF['Number_of_reads(sp_scaled)']=='0'), 'Number_of_reads(sp_scaled)']='NA'

    ##concatenate with NoProtDF (adding classified total, others, and unclassified); filling spaces with 'NA' string will change number columnts to float [not anymore as changed string values for NoProtDF]
    ## not necessary anymore because classified total, others, and unclassified reads are not informative at the protein level; Number of reads won't add up to classified total, for instance
    # Prot_AllDF=pd.concat([Prot_AllDF, NoProtDF], sort=False).fillna('NA')

    ## convert uniquely matching reads (scaled) for all non-species ranks to 'NA' from '0'; should have already been done for Number_of_reads(sp_scaled) above
    # (TaxID_AllDF['Rank'] != 'species') & (TaxID_AllDF['Number_of_uniquely_matching_reads(scaled)']=='0') gives "True" to rows fitting the condition, 'Number_of_uniquely_matching_reads(scaled)' is the column of interest of these rows
    # .loc "grabs" these cells and replaces the values inside with 'NA
    TaxID_AllDF.loc[
        (TaxID_AllDF["Rank"] != "species")
        & (TaxID_AllDF["Number_of_uniquely_matching_reads(scaled)"] == "0"),
        "Number_of_uniquely_matching_reads(scaled)",
    ] = "NA"
    ##not done anymore because done above
    # TaxID_AllDF.loc[(TaxID_AllDF['Rank'] != 'species') & (TaxID_AllDF['Number_of_reads(sp_scaled)']=='0'), 'Number_of_reads(sp_scaled)']='NA'

    ## get subset for species only (will be needed to calculate percentage of uniquely matching reads)
    ##Species_sub=TaxID_AllDF.loc[TaxID_AllDF['Rank']=='species'].copy() ## was done prior because of dtype problems
    ##concatenate with OthersDF(adding classified total, others, and unclassified); note, filling spaces with 'NA' string will change number columnts to float [not anymore as changed string values for OthersDF]
    TaxID_AllDF = pd.concat([TaxID_AllDF, OthersDF], sort=False).fillna("NA")

    ##by_rank dictionary not necessary anymore
    del by_rank

    ##Error_Checks##

    ##get sum of all Proportional Scaled Reads
    PropScaled = Prot_AllDF["Proportional_Reads(scaled)"].astype(float).sum()
    ##get sum of all Proportional SPScaled Reads
    PropSpScaled = (
        Prot_AllDF.loc[Prot_AllDF["Rank"] == "species", "Proportional_Reads(sp_scaled)"]
        .astype(float)
        .sum()
    )
    ##get sum of all PropReads in Prot_AllDF
    Proportional = round(Prot_AllDF["Proportional_Reads"].astype(float).sum())
    ## get sum of all uniquely matching reads at species level from protein table; integer as uniquely matching reads are whole numbers
    Prot_Uniquely = (
        Prot_AllDF.loc[
            Prot_AllDF["Rank"] == "species", "Number_of_uniquely_matching_reads"
        ]
        .astype(int)
        .sum()
    )
    ## get sum of scaled uniquely matching reads at species level of protein table; since scaled, treat as float
    Prot_UniScale = (
        Prot_AllDF.loc[
            Prot_AllDF["Rank"] == "species", "Number_of_uniquely_matching_reads(scaled)"
        ]
        .astype(float)
        .sum()
    )
    ## get sum of uniquely matching reads at species level of taxID table; integer as whole numbers
    TaxID_Uniquely = (
        TaxID_AllDF.loc[
            TaxID_AllDF["Rank"] == "species", "Number_of_uniquely_matching_reads"
        ]
        .astype(int)
        .sum()
    )
    ## get sum of species scaled reads for taxID table (not applicable to protein because of multiple accession matches for 1 read)
    TaxID_Spcale = (
        TaxID_AllDF.loc[TaxID_AllDF["Rank"] == "species", "Number_of_reads"]
        .astype(int)
        .sum()
    )
    ## get sum of scaled uniquely matching reads at species level of taxID table; treat as float
    TaxID_UniScale = (
        TaxID_AllDF.loc[
            TaxID_AllDF["Rank"] == "species",
            "Number_of_uniquely_matching_reads(scaled)",
        ]
        .astype(float)
        .sum()
    )
    ## get sum of all scaled reads classified to taxa of interest; treat as float
    Tax_Reads = (
        TaxID_AllDF.drop(["Classified_Total", "Others", "Unclassified"])[
            "Number_of_reads(scaled)"
        ]
        .astype(float)
        .sum()
    )
    ## get sum of reads of species level taxIDs scaled to total number of reads at species level; since scaled, treat as float
    Tax_RDSp = (
        TaxID_AllDF.loc[TaxID_AllDF["Rank"] == "species", "Number_of_reads(sp_scaled)"]
        .astype(float)
        .sum()
    )
    ## get sum of all reads classified under a taxon of interest
    Reads_Classified = (
        TaxID_AllDF.drop(["Classified_Total", "Others", "Unclassified"])[
            "Number_of_reads"
        ]
        .astype(int)
        .sum()
    )
    ## get sum of Classified Total, Others and Unclassified (should be equal to allreads above)
    TOTAL = (
        int(TaxID_AllDF.loc["Classified_Total", "Number_of_reads"])
        + int(TaxID_AllDF.loc["Others", "Number_of_reads"])
        + int(TaxID_AllDF.loc["Unclassified", "Number_of_reads"])
    )

    if Proportional != int(TaxID_AllDF.loc["Classified_Total", "Number_of_reads"]):
        sys.stderr.write(
            "Error: Proportional Reads is not equating to Classified Reads. Prop Reads: {}, Classified_Total: {}\n".format(
                Proportional, TaxID_AllDF.loc["Classified_Total", "Number_of_reads"]
            )
        )
        sys.exit(1)
    elif Prot_Uniquely != TaxID_Uniquely:
        sys.stderr.write(
            "Error: Protein Table and TaxID Table unique reads do not match\n"
        )
        sys.exit(1)
    elif TaxID_Uniquely != SF_unique:
        sys.stderr.write(
            "Error: Scaling for unique reads at species level do not add up\n"
        )
        sys.exit(1)
    elif TaxID_Spcale != SFsp:
        sys.stderr.write("Error: Scaling for species level reads do not add up\n")
        sys.exit(1)
    elif int(round(Prot_UniScale)) != 100:
        if int(round(Prot_UniScale)) == 0:
            sys.stderr.write(
                "Warning: Protein Table Unique Reads (Scaled) sum to 0. No species classified?\n"
            )
        else:
            sys.stderr.write(
                "Error: Protein Table Unique Reads (Scaled) do not sum to 100. Sum is {}\n".format(
                    Prot_UniScale
                )
            )
            sys.exit(1)
    elif int(round(TaxID_UniScale)) != 100:
        if int(round(TaxID_UniScale)) == 0:
            sys.stderr.write(
                "Warning: TaxID Table Unique Reads (Scaled) sum to 0. No species classified?\n"
            )
        else:
            sys.stderr.write(
                "Error: TaxID Table Unique Reads (Scaled) do not sum to 100. Sum is {}\n".format(
                    TaxID_UniScale
                )
            )
            sys.exit(1)
    elif int(round(Tax_Reads)) != 100:
        sys.stderr.write(
            "Error: TaxID Reads (Scaled) do not sum to 100. Sum is {}\n".format(
                Tax_Reads
            )
        )
        sys.exit(1)
    elif int(round(Tax_RDSp)) != 100:
        if int(round(Tax_RDSp)) == 0:
            sys.stderr.write(
                "Warning: TaxID Reads (sp_Scaled) sum to 0. No species classified?\n"
            )
        else:
            sys.stderr.write(
                "Error: TaxID Reads (sp_scaled) do not sum to 100. Sum is {}\n".format(
                    Tax_RDSp
                )
            )
            sys.exit(1)
    elif int(round(PropScaled)) != 100:
        sys.stderr.write(
            "Error: Proportional Reads (scaled) do not sum to 100. Sum is {}\n".format(
                PropScaled
            )
        )
        sys.exit(1)
    elif int(round(PropSpScaled)) != 100:
        if int(round(PropSpScaled)) == 0:
            sys.stderr.write(
                "Warning: Proportional Reads (sp_Scaled) sum to 0. No species classified?\n"
            )
        else:
            sys.stderr.write(
                "Error: Proportional Reads (sp_scaled) do not sum to 100. Sum is {}\n".format(
                    PropSpScaled
                )
            )
            sys.exit(1)
    ## not done for protein, because 1 read (even at sp level) may have multiple proteins so it's scaled counts would be multiple as well and would not add up to 100 (except for scaled unique species reads)
    ## not done for scaled because scaling factors only available during rank runs, however not predictable when these would be and if not ran last in the sequence, would have values of 0
    # check for scaling to 100 should be enough
    elif Reads_Classified != Scaling_Total:
        sys.stderr.write(
            "Error: Classfied Reads do not add up. Reads_Classified: {}, Scaling_Total: {}\n".format(
                Reads_Classified, Scaling_Total
            )
        )
        sys.exit(1)
    elif TOTAL != allreads:
        sys.stderr.write(
            "Error: Reads in Kaiju File not adding up to Total of dataframe. Kaiju Reads: {}, Dataframe: {}\n".format(
                allreads, TOTAL
            )
        )
        sys.exit()
    else:
        ## get the total number of reads at species level
        AllReads = (
            TaxID_AllDF.loc[TaxID_AllDF["Rank"] == "species", "Number_of_reads"]
            .astype(int)
            .sum()
        )
        # UniqueRead=TaxID_AllDF.loc[TaxID_AllDF['Rank']=='species']['Number_of_uniquely_matching_reads'].astype(int).sum() ##may be replaced by TaxID_Uniquely
        ## get percentage of how many of the species level reads have unique protein matches
        ## only do this if there are species classified
        if int(AllReads) != int(0):
            Percentage = (TaxID_Uniquely / float(AllReads)) * 100
        else:
            Percentage = "no species classified"

    ## theoretically, TaxID dataframe should not have duplicates (Protein dataframe is expected to have duplicates) because rank is TaxID based
    # e.g. you can't have TaxID in both species and phylum
    taxIDdup = TaxID_AllDF[TaxID_AllDF.index.duplicated(keep=False)]
    if not taxIDdup.empty:
        sys.stderr.write("Error: Duplicated TaxID values in TaxID table\n")
        sys.exit(1)

    ## delete variables for error checks
    del (
        Proportional,
        PropScaled,
        PropSpScaled,
        Prot_Uniquely,
        TaxID_Uniquely,
        Prot_UniScale,
        TaxID_UniScale,
        Tax_Reads,
        Tax_RDSp,
        Reads_Classified,
        Scaling_Total,
        AllReads,
        allreads,
        TOTAL,
        TaxID_Spcale,
        SFsp,
        SF_unique,
    )

    return TaxID_AllDF, Prot_AllDF, Percentage


if __name__ == "__main__":
    sys.exit(main())

