import pandas as pd
import numpy as np

# read a edgeR table
df = pd.read_table(snakemake.input[0], header=0)
df = df.dropna(subset=["PValue"])
df["rank"] = -np.log10(df["PValue"]) * np.sign(df["logFC"])
df = df.sort_values(by=["rank"], ascending=False)
df = df[["gene", "rank"]]

fout = open(snakemake.output[0], "w")
d = {}
for a in df.values.tolist():
    # remove version
    idx = a[0].split(".")[0]
    value = a[1]
    # use the first gene id with best rank
    # do not use repeating ids
    if idx not in d:
        fout.write(f"{idx}\t{value}\n")
        d[idx] = None

fout.close()
