# buildSQLiteDB_sm.R
log <- file(snakemake@log[[1]], open="wt")
sink(log)
sink(log, type="message")
options(stringsAsFactors = FALSE)
library(RSQLite)
library(DBI)
library(tidyverse)
con <- dbConnect(RSQLite::SQLite(), snakemake@output[[1]])
datapaths<-snakemake@params[["inp"]]
#datapaths<-"/Users/18042898/Desktop/uni/METAFUNC-MAY212020/metafunc-shiny/results"
#datapaths<-"/Users/18042898/Desktop/arielle-ex-1-group/results" #1 grp#
#datapaths<-"/Users/18042898/Desktop/arielle-ex-nogroups/results" #no groups
#datapaths<-"/Users/18042898/Desktop/arielle-ex-many-grps/results" #many groups
allfiles<-list.files(datapaths,recursive=T,full=T)
functionalfiles<-allfiles[grep("function",allfiles,)]
functionalfiles<-functionalfiles[grep("ALL/",functionalfiles,ignore.case=TRUE,invert=TRUE)]
taxidfiles<-allfiles[grep("taxonomy",allfiles,)]
taxidfiles<-taxidfiles[grep("ALL/",taxidfiles,ignore.case=TRUE,invert=TRUE)]
taxidfiles2<-taxidfiles[grep("all_sptable.tsv",taxidfiles,ignore.case = TRUE,invert = TRUE)]
taxidfiles3<-taxidfiles2[grep("DA/",taxidfiles2,ignore.case = TRUE,invert = TRUE)]
if(length(allfiles[grep("all_grouped/group_all_prop.gofile.tsv",allfiles,)]) >0){
  golinksgrouped<-allfiles[grep("all_grouped/group_all_prop.gofile.tsv",allfiles,)] #"/Users/18042898/Desktop/uni/metafunc-beta/results/microbiome/function/per_group/all_grouped_goPercent.tsv"
}
golinkspersample<-allfiles[grep("per_sample/all_samples/samples_all_prop.gofile.tsv",allfiles,)] #"/Users/18042898/Desktop/uni/metafunc-beta/results/microbiome/function/per_sample/all_samples_goPercent.tsv"
taxidpersample<-read.csv2(taxidfiles3[grep("per_sample",taxidfiles3)],sep="\t",check.names=FALSE)
taxidpersample2<-taxidpersample
##Info for tax id set
if(length(grep("per_group",taxidfiles3))>0){
taxidgrouped<-read.csv2(taxidfiles3[grep("per_group",taxidfiles3)],sep="\t",check.names=FALSE)
taxidgrouped$URL<-sprintf('<a href=https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=%s target="_blank" "><i class="fas fa-external-link-alt"></i>
                  </a>',taxidgrouped$TaxID)
}
taxID_samples<-taxidpersample2[,10:length(colnames(taxidpersample2))]
tax_id_sample_type_key_table<-data.frame(
  sample_name=colnames(taxID_samples),
  sample_type=as.character(t(taxID_samples[1,]))
)
tax_id_sample_type_key_table$internal_sample_id<-1:length(rownames(tax_id_sample_type_key_table))
#taxidpersample3<-taxidpersample2[-1,]
taxidpersample3<-taxidpersample2
taxidpersample4<-taxidpersample3
taxID_table<-taxidpersample4[,c("TaxID","RootTaxon","Kingdom","Phylum","Class","Order","Family","Genus","Species")]
countsTable<-taxidpersample4[,-(2:9)]
countsTable$URL<-sprintf('<a href=https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=%s target="_blank" "><i class="fas fa-external-link-alt"></i>
                  </a>',countsTable$TaxID)
countsTableCharCols<-c("TaxID","URL")
countsTableNumCols<-setdiff(colnames(countsTable),countsTableCharCols)
countsTable[,countsTableNumCols]<-lapply(countsTableNumCols, function(x) as.numeric(countsTable[[x]]))
dbWriteTable(con, "samples", tax_id_sample_type_key_table)
dbWriteTable(con, "taxids", taxID_table)
dbWriteTable(con, "countsTable", countsTable)
## TAX ID GROUPED: 
if(length(grep("per_group",taxidfiles3))>0){
taxidGroupedTableCharCols<-c("TaxID","Kingdom","Phylum","Class","Order","Family","Genus","Species","RootTaxon","URL")
taxidGroupedTableNumCols<-setdiff(colnames(taxidgrouped),taxidGroupedTableCharCols)
taxidgrouped[,taxidGroupedTableNumCols]<-lapply(taxidGroupedTableNumCols, function(x) as.numeric(taxidgrouped[[x]]))
dbWriteTable(con,"taxidgrouped",taxidgrouped)
} else {
  dbWriteTable(con,"taxidgrouped",taxID_table)
}
####### Functional Files ###########
ffpersample<-functionalfiles[grep("per_sample/all",functionalfiles)]
if(length(functionalfiles[grep("per_group/all",functionalfiles)]>0)){
ffgrouped<-functionalfiles[grep("per_group/all",functionalfiles)]
}
indivdata = tibble(File = ffpersample) %>%
  extract(File, "Kingdom", ".*/Functional/(.*)_GOPercent*", remove = FALSE) %>%
  mutate(Data = lapply(File, read_tsv)) %>%
  unnest(Data) %>%
  select(-File)
indivdata$URL<-sprintf('<a href=http://amigo.geneontology.org/amigo/term/%s target="_blank" "><i class="fas fa-external-link-alt"></i>
                       </a>',indivdata$GO_ID)
if(length(functionalfiles[grep("per_group/all",functionalfiles)]>0)){
groupeddata = tibble(File = ffgrouped) %>%
  extract(File, "Kingdom", ".*/Functional/(.*)_GOPercent*", remove = FALSE) %>%
  mutate(Data = lapply(File, read_tsv)) %>%
  unnest(Data) %>%
  select(-File)
groupeddata$URL<-sprintf('<a href=http://amigo.geneontology.org/amigo/term/%s target="_blank" "><i class="fas fa-external-link-alt"></i>
                                                             </a>',groupeddata$GO_ID)
}
goindivTableCharCols<-c("Kingdom","GO_ID","GO","Description","URL")
goindivTableNumCols<-setdiff(colnames(indivdata),goindivTableCharCols)
indivdata[,goindivTableNumCols]<-lapply(goindivTableNumCols, function(x) as.numeric(indivdata[[x]]))
if(length(functionalfiles[grep("per_group/all",functionalfiles)]>0)){
gogroupedTableCharCols<-c("Kingdom","GO_ID","GO","Description","URL")
gogroupedTableNumCols<-setdiff(colnames(groupeddata),gogroupedTableCharCols)
groupeddata[,gogroupedTableNumCols]<-lapply(gogroupedTableNumCols, function(x) as.numeric(groupeddata[[x]]))
dbWriteTable(con, "GO_grouped", groupeddata)
} else {
  dbWriteTable(con, "GO_grouped", indivdata)
}
dbWriteTable(con, "GO_individual", indivdata)
###
go2taxids_single<-read.csv2(golinkspersample,sep="\t",check.names=FALSE)
dbWriteTable(con, "go2taxids_single", go2taxids_single)
if(length(allfiles[grep("all_grouped/group_all_prop.gofile.tsv",allfiles,)]) >0){
go2taxids_grouped<-read.csv2(golinksgrouped,sep="\t",check.names=FALSE)
colnames(go2taxids_grouped)[grep("group",colnames(go2taxids_grouped))]<-'sample_category'
dbWriteTable(con, "go2taxids_grouped", go2taxids_grouped)
} else {
  dbWriteTable(con, "go2taxids_grouped", go2taxids_single)
}
###HUMAN DATA
human_files<-list.files(file.path(datapaths,'host/expression'),full.names=TRUE)
if(length(human_files)>0){
#For V2
#human_all_counts<-grep('all.counts.tsv$',human_files,value=TRUE)
#human_all_counts_data<-read.csv(human_all_counts,sep="\t")
#dbWriteTable(con, "human_all_counts", human_all_counts_data)
human_all_tpm<-grep('all.tpm.tsv$',human_files,value=TRUE)
human_all_tpm_data<-read.csv(human_all_tpm,sep="\t")
human_all_tpm_data2<-human_all_tpm_data[apply(human_all_tpm_data[,-1], 1, function(x) !all(x==0)),] #remove all all-0 rows
human_all_tpm_data2$URL<-sprintf('<a href=https://asia.ensembl.org/Multi/Search/Results?q=%s;site=ensembl_all target="_blank" "><i class="fas fa-external-link-alt"></i> </a>',human_all_tpm_data2$gene)
dbWriteTable(con, "human_all_tpm", human_all_tpm_data2)
} else {
  nulldb<-data.frame("No Host Data Provided"=0)
  dbWriteTable(con, "human_all_tpm", nulldb)
}
## HUMAN COMPARISONS DATA (FLEXIBLE) for DIFFERENTIAL EXPRESSION For V2
# comparisons_data_path<-"/Users/18042898/Desktop/uni/bacterial-plots/bcVizShinyApp/analysis-data/analyses/results/samples/expression/comparisons"
# comparison_dat<-list.files(comparisons_data_path)
# dbWriteTable(con, "list_of_comparison_tables", data.frame('DE_comparison_name'=comparison_dat)) #Create table of comparisons
# for(i in comparison_dat){
#   DEdatfiles<-list.files(paste0(comparisons_data_path,"/",i),full.names = TRUE)
#   DEdat<-grep('cor.deg-tax.matrix.tsv$',DEdatfiles,value=TRUE)
#   DEdat2<-read.csv(DEdat,sep="\t")
#   dbWriteTable(con, i, DEdat2)
# }
#human_all_norm_vs_cancer<-grep('normal-vs-coloncancer.cor.deg-tax.matrix.tsv$',human_files,value=TRUE)
#human_all_norm_vs_cancer_data<-read.csv(human_all_norm_vs_cancer,sep="\t")
#dbWriteTable(con, "human_all_norm_vs_cancer", human_all_norm_vs_cancer_data)
##Sample queries for testing purposes
#dbListTables(con)
#dbListFields(con, "samples")
#res <- dbSendQuery(con, "SELECT * FROM samples WHERE sample_type = 'normal'")
#dbFetch(res)
#dbGetQuery(con, "SELECT * FROM tktest")
# 
# s<-dbGetQuery(con, "SELECT * FROM samples")
# gg<-dbGetQuery(con, "SELECT * FROM GO_grouped")
# gi<-dbGetQuery(con, "SELECT * FROM GO_individual")
# ct<-dbGetQuery(con, "SELECT * FROM countsTable")
# tig<-dbGetQuery(con, "SELECT * FROM taxidgrouped")
# ti<-dbGetQuery(con, "SELECT * FROM taxids")
# q<-dbGetQuery(con,"SELECT tx.*,ct.* FROM countsTable ct left join taxids tx using(TaxID)")
# go2tax_single<-dbGetQuery(con, "SELECT * FROM go2taxids_single")
# go2tax_grouped<-dbGetQuery(con, "SELECT * FROM go2taxids_grouped")
# 
# hac<-dbGetQuery(con, "SELECT * FROM human_all_counts")
# 
#hatpm<-dbGetQuery(con, "SELECT * FROM human_all_tpm")
# 
# hnvsc<-dbGetQuery(con, "SELECT * FROM human_all_norm_vs_cancer")
# 
# q2<-dbGetQuery(con,"SELECT txg.*,ct.* FROM countsTable ct left join taxidgrouped txg using(TaxID)")
# colnames(dbGetQuery(con,"SELECT * FROM taxids tx join countsTable ct using(TaxID)"))