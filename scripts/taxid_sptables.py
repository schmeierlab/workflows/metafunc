#!/usr/bin/env python

"""
NAME: TaxID_spTables.py
=========

DESCRIPTION
===========

From TaxonID based read counts file, get a joined tsv table of all samples of interest using a configuration file.
Returns a taxonID-taxon name based table of read counts (and read counts scaled to species level) for each sample.

=====

VERSION HISTORY
===============

July 09, 2019 - v.1.0.0

LICENCE
=======
2019, copyright Arielle Sulit, (a.sulit@massey.ac.nz)

"""

import sys
import csv
import os
import argparse
import pandas as pd
import time

## If SettingwithCopyWarning is ever generated, raise error instead of warning; makes code more strict
pd.set_option("mode.chained_assignment", "raise")


def main():

    TaxIDSummary = argparse.ArgumentParser(
        description="construct table of taxID - reads (and scaled reads) in all samples indicated in a configuration file, species level only"
    )
    TaxIDSummary.add_argument(
        "--TaxConfig",
        action="store",
        type=str,
        help="tab separated file giving path to each input file and a grouping it belongs to; note: Line[0] = sample name, Line[1] = protein-based file, Line[2] = tax-ID based file, Line[3] = group; file contains headers",
    )
    TaxIDSummary.add_argument(
        "--taxon",
        action="store",
        type=str,
        help="gather results by particular taxa or all",
    )
    TaxIDSummary.add_argument(
        "--outfile",
        action="store",
        type=str,
        help="optional: output file for  species count table",
    )
    TaxIDSummary.add_argument(
        "--outfileSc",
        action="store",
        type=str,
        help="optional: output file for scaled species count table",
    )
    TaxIDSummary.add_argument(
        "--nogroup", action="store_true", help="indicate if no grouping is required"
    )
    TaxIDSummary.add_argument(
        "--cutoff",
        action="store",
        type=float,
        help="relative abundance cutoff; default is >=0.001%%",
    )
    TaxIDSummary.add_argument(
        "--minsam",
        action="store",
        type=float,
        help="minimum number of samples that a species should be found in at >= cutoff; default is 1",
    )

    args = TaxIDSummary.parse_args()

    overalltime1 = time.time()

    if args.TaxConfig is None:
        TaxIDSummary.error(
            "Error: please indicate where to find configuration for sample read count files"
        )
    else:
        ## make a dictionary of file in its path to list of [0]: filename, and [1] group (unless --nogroup)
        ## line[2] contains path to taxID_prot.tsv file
        Filegroups = {}
        filed = open(args.TaxConfig, "r")
        csv_file = csv.reader(filed, delimiter="\t")
        next(csv_file)
        # if no groups, all group values are 'No Group'
        if args.nogroup:
            # nogroup = True
            for line in csv_file:
                if line[2] in Filegroups:
                    sys.stderr.write(
                        "Error: Please check your configuration file: you may have doubled an input\n"
                    )
                    sys.exit(1)
                else:
                    Filegroups[line[2]] = []
                    Filegroups[line[2]].append(line[0])
                    Filegroups[line[2]].append("NoGroup")
            ## to print out information on how many samples there are and what samples
            sys.stdout.write(
                "No Group Stratification of {} Samples\n".format(len(Filegroups))
            )
            sys.stdout.flush()
            for sam in list(Filegroups.values()):
                sys.stdout.write("{}\n".format(sam[0]))
                sys.stdout.flush()
        else:
            # nogroup = False
            ## to print out information on grouping and how many samples per group, get a group:filename dictionary
            grouping = {}
            for line in csv_file:
                if line[2] in Filegroups:
                    sys.stderr.write(
                        "Error: Please check your configuration file: you may have doubled an input\n"
                    )
                    sys.exit(1)
                else:
                    Filegroups[line[2]] = []
                    Filegroups[line[2]].append(line[0])
                    Filegroups[line[2]].append(line[3])
                ## initialize unique groups as keys
                if line[3] not in grouping:
                    grouping[line[3]] = []
                ## append all the filenames belonging to 1 group
                grouping[line[3]].append(line[0])
            ## get the groups of the files to print out
            sys.stdout.write(
                "Grouping Chosen. Groups are: {}\n".format(
                    ", ".join(list(grouping.keys()))
                )
            )
            sys.stdout.flush()
            ## print out group, and how many samples there are in a group and the corresponding sample names in a group
            for name, vallist in grouping.items():
                sys.stdout.write(
                    'Number of samples in group "{}": {}\n'.format(name, len(vallist))
                )
                sys.stdout.flush()
                for val in vallist:
                    sys.stdout.write("{}\n".format(val))
                    sys.stdout.flush()
            ## grouping dictionary is not necessary anymore
            del grouping
        ## close opened csv file
        filed.close()

    ## specify taxon wanted or all
    if args.taxon is None:
        TaxIDSummary.error("Error: please choose taxon wanted or indicate all")
    else:
        taxon = args.taxon

    if args.cutoff is None:
        cutoff = float(0.001)
    else:
        cutoff = float(args.cutoff)

    if args.minsam is None:
        samsmin = 1  ## this will be percent
    else:
        samsmin = args.minsam

    ##if given # of samples > the total number of samples, error and exit
    if samsmin > len(Filegroups):
        sys.stderr.write(
            "Error: Number of samples for cutoff is > actual number of samples\n"
        )
        sys.exit(1)

    ## get the number of samples to filter by
    # samsmin = round(minsam*len(Filegroups))

    ## specify output file
    if args.outfile is None and args.outfileSc is None:
        direct = "TaxID"
        filesuff = "_spTable.tsv"
        filescsuff = "_spTableScaled.tsv"
        try:
            if not os.path.exists(direct):
                os.makedirs(direct)
        except OSError:
            sys.stderr.write("Error: creating TaxID directory\n")
            sys.exit(1)
        fileout = direct + "/" + taxon + filesuff
        fileout_scaled = direct + "/" + taxon + filescsuff
    ## use outfiles for  output files
    elif args.outfile is None and args.outfileSc is not None:
        TaxIDSummary.error(
            "Error: please indicate output file for read count species table"
        )
    elif args.outfile is not None and args.outfileSc is None:
        TaxIDSummary.error(
            "Error: please indicate output file for scaled read count species table"
        )
    else:
        fileout = args.outfile
        fileout_scaled = args.outfileSc

    ## run all files through TaxID_Summary function

    TAXID_DF, TAXID_scaledDF, otherdf = TaxID_Summary(
        Filegroups, taxon, cutoff, samsmin
    )

    TAXID_DF.rename(columns={"Taxon": "Species"}, inplace=True)
    TAXID_scaledDF.rename(columns={"Taxon": "Species"}, inplace=True)

    # print(TAXID_DF)
    # print(TAXID_scaledDF)
    TAXID_DF.to_csv(fileout, sep="\t")
    sys.stdout.write("write species count table  to {}\n".format(fileout))
    TAXID_scaledDF.to_csv(fileout_scaled, sep="\t")
    sys.stdout.write("write scaled species count table to {}\n".format(fileout_scaled))

    overalltime2 = time.time()
    sys.stdout.write(
        "Time to write {} samples to table: {} secs\n".format(
            len(Filegroups), overalltime2 - overalltime1
        )
    )

    # otherdf=pd.concat([addotherdf, otherdf])
    otherdf.rename_axis("Basic_Information", inplace=True)
    sys.stdout.write("\n{}\n".format(otherdf.to_string()))
    sys.stdout.flush()

    return


def TaxID_Summary(Filegroups, taxon, cutoff, samsmin):

    ##define Other_Taxa variable (not picked args.taxon above)
    Other_Taxa = "Not_{}".format(taxon)
    ## initialize dataframes with columns (necessary when merging)
    TAXID_DF = pd.DataFrame(columns=["TaxID", "Taxon", "RootTaxon"])  ## added RootTaxon
    TAXID_scaledDF = pd.DataFrame(
        columns=["TaxID", "Taxon", "RootTaxon"]
    )  ## added RootTaxon

    ## for key in Filegroups (key are filepaths to taxID-based read count files)
    for files in Filegroups:
        ## get filename
        filename = Filegroups[files][0]
        ## set a small dataframe containing 'group' as index and group name as value with filenames as headers
        # GROUP = {"group": Filegroups[files][1]}
        # groupingDF = pd.DataFrame(
        #    {"TaxID": pd.Series(GROUP).index, filename: pd.Series(GROUP).values}
        # ).set_index("TaxID")
        # print(groupingDF)
        # sys.exit()
        ##from here groupingDF is used, GROUP not necessary anymore; will re-initialize next loop
        # del GROUP

        ## open each file as dataframe, all columns are dtype objects, and choose only columns to be used
        df = pd.read_csv(
            files,
            sep="\t",
            dtype=object,
            usecols=[
                "TaxID",
                "Taxon",
                "Rank",
                "RootTaxon",
                "Number_of_reads",
                "Number_of_reads(sp_scaled)",
            ],
        ).set_index("TaxID")
        ## check that loaded dataframe has object for all columns
        emptydf = df.select_dtypes(exclude=["object"])
        if not emptydf.empty:
            sys.stderr.write("Error: Unexpected dtype in taxID table\n")
            sys.exit(1)
        ## get value of classified total and all reads for summation checks later
        class_total = int(df.loc["Classified_Total", "Number_of_reads"])
        ## get a subset slice from the original data frame containing unclassified and all other Microbial Eukaryotes, convert NaN values with 'NA'
        EElse = df.loc[["Others", "Unclassified"]].copy().fillna("NA")
        ## drop unnecessary rows including Classified_Total (this is a total of all classified regardless of rank; will get a 'Higher Taxon count later on)
        df = df.drop(["Classified_Total", "Others", "Unclassified"]).copy()

        ## Only Number_of_reads(sp_scaled) should have any NaNs
        scaled_na = ["Number_of_reads(sp_scaled)"]
        nacols = df.columns[df.isna().any()].tolist()
        if scaled_na != nacols:
            sys.stderr.write(
                "Error: Unexpected NaN values upon opening {} table\n".format(filename)
            )
            sys.exit(1)

        ##if user wants a specific taxa
        if taxon != "all":
            ## get species level of taxon of interest (create copy into sp dataframe);
            # get rows of species and taxon wanted
            sp = df.loc[(df["Rank"] == "species") & (df["RootTaxon"] == taxon)].copy()
            ## columns TaxID, Taxon, Rank, RootTaxon, Number_of_reads, and Number_of_reads(sp_scaled) is expected to have no NaNs at species level
            # so list of columns with any NA should be empty
            # note that in some circumstances, sp could be empty and these checks would not mean anything
            if not sp.empty:
                if sp.isna().any().any():
                    # spnacols=sp.columns[sp.isna().any()].tolist()
                    # if len(spnacols)>0:
                    sys.stderr.write(
                        "Error: NaN columns found in Species Ranked dataframe subset of {} when there should be none\n".format(
                            filename
                        )
                    )
                    sys.exit(1)
            ## get taxon of interest, NOT species level
            # get rows of NOT species but taxon of interest
            notsp = df.loc[
                (df["Rank"] != "species") & (df["RootTaxon"] == taxon)
            ].copy()
            ##notsp, number of reads scaled is expected to be NA for ALL
            # unless notsp is empty (this will mean list is empty)
            if not notsp.empty:
                if (
                    "Number_of_reads(sp_scaled)"
                    not in notsp.columns[notsp.isna().all()].tolist()
                ):
                    sys.stderr.write(
                        "Error: Unexpected values in species scaled column of {}\n".format(
                            filename
                        )
                    )
                    sys.exit(1)
            ## not taxon of interest, mixed species and higher taxa
            nottax = df.loc[df["RootTaxon"] != taxon].copy()

            ## get sum of all higher taxa (not species) of taxon of interest and add "Higher_Taxon_Levels" row
            notsp.at["Higher_Taxon_Levels", "Number_of_reads"] = (
                notsp["Number_of_reads"].astype(int).sum().astype(object)
            )
            ## subset dataframe to just the sum (note would have NaN)
            # notsp would have NaN in Number_of_reads(sp_scaled) columns but subsetting below will only leave 1 row for this
            notsp = notsp.loc[["Higher_Taxon_Levels"]].copy()
            ## get sum of all others that are not in taxon of interest; add Other_Taxa row
            nottax.at[Other_Taxa, "Number_of_reads"] = (
                nottax["Number_of_reads"].astype(int).sum().astype(object)
            )
            ## get subset dataframe of just the sum (note would have NaN)
            nottax = nottax.loc[[Other_Taxa]].copy()

            ## to check if classified total still adds up
            class_total2 = (
                sp["Number_of_reads"].astype(int).sum()
                + notsp["Number_of_reads"].astype(int).sum()
                + nottax["Number_of_reads"].astype(int).sum()
            )
            if class_total != class_total2:
                sys.stderr.write(
                    "Error: Total classified reads not adding up in sample {}\n".format(
                        filename
                    )
                )
                sys.exit(1)

            ## join everything and fill NaN with 'NA'
            taxID_df = pd.concat([sp, notsp, nottax, EElse]).fillna("NA")
            ## drop unnecessary columns
            taxID_df = taxID_df.drop(
                columns=["Rank", "Number_of_reads(sp_scaled)"]
            )  ##remove drop of RootTaxon
            ## rename column to the filename
            taxID_df.rename(columns={"Number_of_reads": filename}, inplace=True)

            ## for scaled, drop unneccessary columns; will not need "Others" dataframes because these do not have scaled values
            taxIDscaled_df = sp.drop(
                columns=["Rank", "Number_of_reads"]
            ).copy()  ## remove drop of RootTaxon
            ## rename column to filename
            taxIDscaled_df.rename(
                columns={"Number_of_reads(sp_scaled)": filename}, inplace=True
            )

        else:
            ## user wants ALL taxon	so only get species level
            sp = df.loc[df["Rank"] == "species"].copy()
            ## columns TaxID, Taxon, Rank, RootTaxon, Number_of_reads, and Number_of_reads(sp_scaled) is expected to have no NaNs at species level
            # so list of columns with any NA should be empty
            # note that in some circumstances, sp could be empty and these checks would not mean anything
            if not sp.empty:
                if sp.isna().any().any():
                    # spnacols=sp.columns[sp.isna().any()].tolist()
                    # if len(spnacols)>0:
                    sys.stderr.write(
                        "Error: NaN columns found in Species Ranked dataframe subset of {} when there should be none\n".format(
                            filename
                        )
                    )
                    sys.exit(1)
            ## get everything at not species level [since all taxa are wanted, there are no 'other taxa']
            #  (and exclude 'Others' and 'Unclassified' which are already excluded from above)
            notsp = df.loc[df["Rank"] != "species"].copy()
            ##notsp, number of reads scaled is expected to be NA for ALL
            # except if notsp is empty, and then there are no NaNs at all
            if not notsp.empty:
                if (
                    "Number_of_reads(sp_scaled)"
                    not in notsp.columns[notsp.isna().all()].tolist()
                ):
                    sys.stderr.write(
                        "Error: Unexpected values in species scaled column of {}\n".format(
                            filename
                        )
                    )
                    sys.exit(1)

            ## get sum of all higher taxon levels; as we are interested in all taxon, these are everything not species; add 'Higher_Taxon_Levels' row
            notsp.at["Higher_Taxon_Levels", "Number_of_reads"] = (
                notsp["Number_of_reads"].astype(int).sum().astype(object)
            )
            ## get subset dataframe of just the sum (note would have NaN)
            # notsp would have NaN in Number_of_reads(sp_scaled) columns but subsetting below will only leave 1 row for this
            notsp = notsp.loc[["Higher_Taxon_Levels"]].copy()
            ## to check if classified total still adds up
            class_total2 = (
                sp["Number_of_reads"].astype(int).sum()
                + notsp["Number_of_reads"].astype(int).sum()
            )
            if class_total != class_total2:
                sys.stderr.write(
                    "Error: Total classified reads not adding up in sample {}\n".format(
                        filename
                    )
                )
                sys.exit(1)
            ## join everything (no nottax because everything is in taxon of interest) and fillna('NA')
            taxID_df = pd.concat([sp, notsp, EElse]).fillna("NA")
            ##drop unnecessary columns
            taxID_df = taxID_df.drop(
                columns=["Rank", "Number_of_reads(sp_scaled)"]
            )  ## remove drop of RootTaxon
            ##rename column to the filename
            taxID_df.rename(columns={"Number_of_reads": filename}, inplace=True)
            ## drop unnecessary columns for scaled; will still not need the notsp dataframes because these do not have scaled values
            taxIDscaled_df = sp.drop(
                columns=["Rank", "Number_of_reads"]
            ).copy()  ## remove drop of RootTaxon
            ## rename column to filename
            taxIDscaled_df.rename(
                columns={"Number_of_reads(sp_scaled)": filename}, inplace=True
            )

        ##class_total and class_total2 are not necessary anymore
        del (class_total, class_total2)

        ## don't need dfs above anymore as complete dataframe is what's necessary now; will re-initialize next loop
        del (df, sp, notsp, EElse)
        ## if taxon != all, then nottax is present and can be removed too
        if taxon != "all":
            del nottax
        ## add the groupingDF from above, these dataframes should have the same index = 'TaxID'; reset index so easier to merge later on with everything else
        # taxID_df = (
        #    pd.concat([taxID_df, groupingDF], sort=False).fillna("NA").reset_index()
        # )
        taxID_df = taxID_df.reset_index()
        ## for each file in group, merge all tables obtained from them; fillna with 0; should be the ones present in other samples
        TAXID_DF = pd.merge(
            TAXID_DF, taxID_df, how="outer", on=["TaxID", "Taxon", "RootTaxon"]
        ).fillna(
            "0"
        )  ##add RootTaxon

        ## add the groupingDF from above, these dataframes should have the same index = 'TaxID'; reset index so easier to merge later on with everything else
        # taxIDscaled_df = (
        #    pd.concat([taxIDscaled_df, groupingDF], sort=False)
        #    .fillna("NA")
        #    .reset_index()
        # )
        taxIDscaled_df = taxIDscaled_df.reset_index()
        ## for each file in group, merge all tables obtained from them; fillna with 0; should be the ones present in other samples
        TAXID_scaledDF = pd.merge(
            TAXID_scaledDF,
            taxIDscaled_df,
            how="outer",
            on=["TaxID", "Taxon", "RootTaxon"],
        ).fillna(
            "0"
        )  ##add RootTaxon

    ## to check that there is only 1 unique combination of TaxID and Taxon, there should not be any duplicated values in these columns
    # drop Higher_Taxon_Levels, Other_Taxa, Others, Unclassified, group because these would have doubled Taxon due to NAs (no more group)
    # how to check Kingdom???
    if taxon != "all":
        check_df = (
            TAXID_DF.set_index("TaxID")
            .drop(["Higher_Taxon_Levels", Other_Taxa, "Others", "Unclassified"])
            .copy()
            .reset_index()
        )
    else:
        check_df = (
            TAXID_DF.set_index("TaxID")
            .drop(["Higher_Taxon_Levels", "Others", "Unclassified"])
            .copy()
            .reset_index()
        )
    checksc = TAXID_scaledDF.set_index("TaxID").copy().reset_index()
    duptaxon = check_df[check_df.duplicated(subset="Taxon", keep=False)].copy()
    duptaxid = check_df[check_df.duplicated(subset="TaxID", keep=False)].copy()
    dupsctax = checksc[checksc.duplicated(subset="Taxon", keep=False)].copy()
    dupscid = checksc[checksc.duplicated(subset="TaxID", keep=False)].copy()
    if (
        not duptaxon.empty
        or not duptaxid.empty
        or not dupsctax.empty
        or not dupscid.empty
    ):
        sys.stderr.write("Error: Wrong combination of Taxon and TaxID detected\n")
        sys.exit(1)

    ##checks done, check dataframes are not necessary
    del (check_df, checksc, duptaxid, duptaxon, dupscid, dupsctax)

    ## set_index to 'TaxID'
    TAXID_DF = TAXID_DF.set_index("TaxID")
    TAXID_scaledDF = TAXID_scaledDF.set_index("TaxID")
    # print(TAXID_DF.dtypes, TAXID_scaledDF.dtypes)

    if taxon == "all":
        dropgroups = ["Higher_Taxon_Levels", "Others", "Unclassified"]
    else:
        dropgroups = ["Higher_Taxon_Levels", Other_Taxa, "Others", "Unclassified"]

    ## get total species
    # TAXID_DF.loc[taxon + "_Species"] = (
    #    TAXID_DF.drop(columns=["Taxon", "RootTaxon"])
    #    .drop(dropgroups1)
    #    .astype(int)
    #    .sum()
    #    .astype(object)
    # )
    TAXID_DF = TAXID_DF.fillna("NA")

    ## subset "others" rows
    # if taxon == "all":
    #    dropgroups2 = [
    #        "Higher_Taxon_Levels",
    #        "Others",
    #        "Unclassified",
    #    ]
    # else:
    #    dropgroups2 = [
    #        "Higher_Taxon_Levels",
    #        Other_Taxa,
    #        "Others",
    #        "Unclassified",
    #    ]

    otherdf = TAXID_DF.loc[dropgroups].copy()
    otherdf = otherdf.drop(columns=["Taxon", "RootTaxon"])
    TAXID_DF = TAXID_DF.drop(dropgroups)

    ## sort entire dataframe by Taxon
    TAXID_DF = TAXID_DF.sort_values(by="Taxon")
    TAXID_scaledDF = TAXID_scaledDF.sort_values(by="Taxon")

    ##now that we don't need other rows output with the main tables, no need for reindexing

    if taxon == "all":
        ## check that scaledDF is adding up to 100
        TAXID_scaledDF.loc["Total"] = (
            TAXID_scaledDF.drop(columns=["Taxon", "RootTaxon"])
            .astype(float)
            .sum()
            .astype(object)
        )
        ##add RootTaxon as dropped
        ## if all, total of species scaled counts should be 100
        float_total = round(
            TAXID_scaledDF.loc[["Total"]]
            .drop(columns=["Taxon", "RootTaxon"])
            .astype(float)
        )  ##add RootTaxon as dropped
        if not (float_total == 100).all().all():
            ## IT MIGHT BE POSSIBLE FOR A SAMPLE TO HAVE NO SPECIES LEVEL (NOT PROBABLE BUT POSSIBLE) so would have to be 0
            for col in float_total.columns.to_list():
                if float_total.loc["Total", col] != 100:
                    ## could be no species:
                    if float_total.loc["Total", col] != 0:
                        sys.stderr.write(
                            "Error: Scaled table for all taxa do not add up to 100 but it has species values for sample {}\n".format(
                                col
                            )
                        )
                        sys.exit(1)
                    else:
                        sys.stderr.write(
                            "Warning: sum of scaled reads for {} column is 0. No species detected?\n".format(
                                col
                            )
                        )
        ## float_total not necessary anymore
        del float_total
        ## Scaled total also not necessary anymore (it was just checks that were needed)
        TAXID_scaledDF = TAXID_scaledDF.drop("Total")
    # else:
    ## no total needed

    ##sort columns
    TAXID_DF = TAXID_DF.sort_index(axis=1)
    TAXID_scaledDF = TAXID_scaledDF.sort_index(axis=1)

    ## check if both scaled reads and reads table have the same columns
    if sorted(list(TAXID_DF.columns.values)) != sorted(
        list(TAXID_scaledDF.columns.values)
    ):
        sys.stderr.write("Error: Columns of Read Table and Scaled Table not the same\n")
        sys.exit(1)

    ## reorder columns with Taxon at the start
    newcols = ["Taxon", "RootTaxon"]  ## add RootTaxon to list
    ## cols is a list of all columns of TAXID_DF in order of group
    # append to newcols to reorder dataframes
    cols = list(TAXID_DF.columns.values)
    for c in cols:
        if c != "Taxon" and c != "RootTaxon":  ## add RootTaxon to c!
            newcols.append(c)
    ## use newcols to reorder dataframe; since same columns for both scaled and non-scaled tables, can just use columns of non-scaled table
    TAXID_DF = TAXID_DF[newcols]
    TAXID_scaledDF = TAXID_scaledDF[newcols]
    ## newcols not needed

    ##only get IDs above a cutoff
    sys.stdout.write(
        "Gathering IDs for species >= {} relative abundance in at least {} sample(s)\n".format(
            cutoff, samsmin
        )
    )

    ## create a tempdf from scaled df dropping taxon and root taxon columns
    tempdf = TAXID_scaledDF.iloc[:, 2:].copy().apply(pd.to_numeric, errors="raise")
    tempdf = tempdf[(tempdf >= cutoff).sum(axis=1) >= samsmin]

    ## get counts of reads that matched to species that did not make the filter cuts
    addotherdf = TAXID_DF.loc[~TAXID_DF.index.isin(list(tempdf.index.values))]
    addotherdf = addotherdf.iloc[:, 2:].copy()
    addotherdf = addotherdf.apply(pd.to_numeric, errors="raise")
    addotherdf.loc["FilteredSpecies"] = addotherdf.sum()
    addotherdf = addotherdf.loc[["FilteredSpecies"]].copy()

    ## subset output dfs to species that made the filter cuts
    TAXID_DF = TAXID_DF.reindex(list(tempdf.index.values))
    TAXID_scaledDF = TAXID_scaledDF.reindex(list(tempdf.index.values))

    ## get counts of species that made the filter cuts
    TAXID_DF.loc["{}_Species".format(taxon)] = (
        TAXID_DF.drop(columns=["Taxon", "RootTaxon"]).astype(int).sum().astype(object)
    )
    ## add this to other df for basic information statistics
    addotherdf = pd.concat(
        [TAXID_DF.loc[["{}_Species".format(taxon)]].copy(), addotherdf], sort=False
    ).drop(columns=["Taxon", "RootTaxon"])
    ## drop this in the TAXID_DF dataframe
    TAXID_DF = TAXID_DF.drop(["{}_Species".format(taxon)])
    ## add the additional df to the final other df and get total
    otherdf = pd.concat([addotherdf, otherdf], sort=False)
    otherdf.loc["Total"] = otherdf.astype(int).sum().astype(object)

    del (newcols, cols)

    return TAXID_DF, TAXID_scaledDF, otherdf


if __name__ == "__main__":
    sys.exit(main())
