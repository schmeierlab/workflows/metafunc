#!/usr/bin/env python3
"""
NAME: make_matrix
=================

DESCRIPTION
-----------

INSTALLATION
------------

USAGE
-----

VERSION HISTORY
---------------

0.1.0    2020    Initial version.

LICENCE
-------
2020, copyright Sebastian Schmeier
s.schmeier@pm.me // https://www.sschmeier.com

template version: 2.2 (2020/02/08)
"""
__version__ = "0.1.0"
__date__ = "2020"
__email__ = "s.schmeier@pm.me"
__author__ = "Sebastian Schmeier"

# Generate your own AsciiArt at:
__banner__ = r"""
             _                    _       _     
  _ __  __ _| |_____   _ __  __ _| |_ _ _(_)_ __
 | '  \/ _` | / / -_) | '  \/ _` |  _| '_| \ \ /
 |_|_|_\__,_|_\_\___|_|_|_|_\__,_|\__|_| |_/_\_\
                   |___|                        
        by Sebastian Schmeier
"""

import sys
import os
import argparse
import csv
import gzip
import bz2
import zipfile
import time
import logging
import subprocess

# set up logging
logger = logging.getLogger()

_programpath = os.path.realpath(__file__)


def print_logo(filehandle=sys.stderr):
    text = f"{__banner__}\n"
    filehandle.write("{}\n".format("*" * 60))
    filehandle.write(text)
    filehandle.write("version: {}  date: {}\n".format(__version__, __date__))
    filehandle.write("Using executable at: {}\n".format(_programpath))
    filehandle.write("{}\n\n".format("*" * 60))


def parse_cmdline():
    """ Parse command-line args. """
    # parse cmd-line ----------------------------------------------------------
    description = "Read delimited file."
    version = "version {}, date {}".format(__version__, __date__)
    epilog = "Copyright {} ({})".format(__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument("--version", action="version", version="{}".format(version))

    parser.add_argument(
        "str_file",
        metavar="FILE",
        help='Delimited file. [use "-" or "stdin" to read from standard in]',
    )
    parser.add_argument(
        "--header",
        action="store_true",
        default=False,
        help='Header in file.  [default: "False"]',
    )
    parser.add_argument(
        "-d",
        "--delimiter",
        metavar="STRING",
        dest="delimiter_str",
        default="\t",
        help='Delimiter used in file.  [default: "tab"]',
    )
    parser.add_argument(
        "-o",
        "--out",
        metavar="STRING",
        dest="outfile_name",
        default=None,
        help='Out-file. [default: "stdout"]',
    )

    matrix = parser.add_argument_group("matrix arguments")
    matrix.add_argument(
        "--rows",
        type=int,
        nargs="+",
        metavar="INT",
        dest="row",
        default=1,
        help='Column 1 to use for rows. [default: "1"]',
    )
    matrix.add_argument(
        "--cols",
        type=int,
        nargs="+",
        metavar="INT",
        dest="col",
        default=2,
        help='Column 2 to use for cols. [default: "2"]',
    )
    matrix.add_argument(
        "--values",
        type=int,        
        metavar="INT",
        dest="value",
        default=3,
        help='Column 3 to use for matrix values. [default: "3"]',
    )
    matrix.add_argument(
        "--missing",
        metavar="STRING",
        dest="missing",
        default="0",
        help='Missing value for matrix. [default: "0"]',
    )
    
    log = parser.add_argument_group("logging arguments")
    log.add_argument(
        "--log",
        metavar="STRING",
        dest="loglevel",
        default="INFO",
        help='Logging level. [default: "INFO"]',
    )

    # if no arguments supplied print help
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args, parser


def yield_line_gz_file(filename):
    """
    :param filename: String (absolute path)
    :return: GeneratorFunction (yields String)

    Reads gzip files line by line much faster then gzip.open()
    Use:
         for line in yield_line_gz_file(filename):
             do_something(line)
    """
    ph = subprocess.Popen(["zcat", filename], stdout=subprocess.PIPE)
    for line in ph.stdout:
        yield line


def open_infile(filename):
    """ OPEN FILES FOR READING """
    if filename in ["-", "stdin"]:
        filehandle = sys.stdin
    elif filename.split(".")[-1] == "gz":
        filehandle = gzip.open(filename, "rt")
    elif filename.split(".")[-1] == "bz2":
        filehandle = bz2.open(filename, "rt")
    elif filename.split(".")[-1] == "zip":
        filehandle = zipfile.ZipFile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def get_outfile(filename):
    """ Create outfile object """
    if not filename:
        filehandle = sys.stdout
    elif filename in ["-", "stdout"]:
        filehandle = sys.stdout
    elif filename.split(".")[-1] == "gz":
        filehandle = gzip.open(filename, "wt")
    elif filename.split(".")[-1] == "bz2":
        filehandle = bz2.BZ2File(filename, "wt")
    else:
        filehandle = open(filename, "w")
    return filehandle


def main():
    """ The main funtion. """
    # comment out to remove logo printing
    print_logo(filehandle=sys.stderr)
    # parse commandline args
    args, parser = parse_cmdline()

    # set up logger
    numeric_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f"Invalid log level: {args.loglevel}")
    logging.basicConfig(
        level=numeric_level,
        format="%(asctime)s [%(levelname)8s] (%(filename)s:%(funcName)15s():%(lineno)4s): %(message)s ",
        datefmt="%Y%m%d-%H:%M:%S",
    )
    logger.info("Program start.")
    # test the logging system
    # logger.debug("Debug event")
    # logger.critical("Critical event")

    try:
        fileobj = open_infile(args.str_file)
    except FileNotFoundError as e:
        logger.critical(f'Could not load file "{args.str_file}". EXIT.')
        raise
    
    # delimited file handler
    csv_reader_obj = csv.reader(fileobj, delimiter=args.delimiter_str)
    # get header
    if args.header:
        header = next(csv_reader_obj)
        
    # collect
    d = {}
    cols = {}
    for a in csv_reader_obj:
        rowitem = "|".join([a[y] for y in [x - 1 for x in args.row]])
        colitem = "|".join([a[y] for y in [x - 1 for x in args.col]])
        value = a[args.value-1]
        #print(a)
        #print(rowitem, colitem, value)
        if rowitem not in d:
            d[rowitem] = {}
            
        assert colitem not in d[rowitem]
        
        d[rowitem][colitem] = value
        cols[colitem] = None
        

    rows = list(d.keys())
    rows.sort()
    cols = list(cols.keys())
    cols.sort()

    # outfileobj = get_outfile(args.outfile_name)
    # contextmanager better, no closing file required
    with get_outfile(args.outfile_name) as outfileobj:
        outfileobj.write("ID\t{}\n".format("\t".join(cols)))
        # For printing to stdout
        # SIGPIPE is throwing exception when piping output to other tools
        # like head. => http://docs.python.org/library/signal.html
        # use a try - except clause to handle
        try:
            for r in rows:
                a = [r]
                for c in cols:
                    if c in d[r]:
                        v = d[r][c]
                    else:
                        v = args.missing
                    a.append(v)
                outfileobj.write("{}\n".format("\t".join(a)))
                
            # flush output here to force SIGPIPE to be triggered
            # while inside this try block.
            sys.stdout.flush()
        except BrokenPipeError:
            # Python flushes standard streams on exit; redirect remaining output
            # to devnull to avoid another BrokenPipeError at shut-down
            devnull = os.open(os.devnull, os.O_WRONLY)
            os.dup2(devnull, sys.stdout.fileno())
            sys.exit(1)  # Python exits with error code 1 on EPIPE

    # ------------------------------------------------------
    fileobj.close()
    return


if __name__ == "__main__":
    sys.exit(main())
