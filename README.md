
[![version](https://img.shields.io/static/v1?label=version&message=v0.3.0&color=blueviolet)](https://gitlab.com/schmeierlab/workflows/metafunc)

# MetaFunc

A metatranscriptomics/genomics workflow to study microbe abundance and community functional capacity.

![MetaFuncWorkflow](img/metafunc_workflow.png)

*MetaFunc Workflow*

- AUTHOR: Arielle Sulit (iel_sulit@yahoo.com)
- AUTHOR: Sebastian Schmeier
- AUTHOR (MetaFunc Shiny App): Tyler Kolisnik (tkolisnik@gmail.com)

## Details


### Complete Documentation

This **README** is only an overview of the capabilities of the pipeline. Please see [MetaFunc Docs](https://metafunc.readthedocs.io/) for a more complete description including workflow, usage, and results.

### Default Usage

In microbiome studies, it is useful to know functional processes contributed by the microbiome as well as the identity of the microbes that are present in a sample. The [Kaiju](http://kaiju.binf.ku.dk) package uses protein matches to identify which microorganisms are present in a sequenced sample. This workflow takes this a step further by summarizing the identification of microbes into a table, and also binning the protein accession numbers into gene ontology annotations to give users an image of what functions are taking place in a microbiome environment. A web page/application is created at the end for the user to easily view results of samples or groups run through the pipeline.

This workflow takes as input sequencing reads (any combinations of Paired-end, Single-end, or both) in FASTA or FASTQ format, runs these through ***Kaiju***, and parses the raw verbose results through custom scripts to give the following:

1. **Taxonomy ID : Read Count *(per sample)* table**. Taxonomy IDs could be any species belonging to the taxa specified in [kaiju-taxonlistEuk.tsv](https://raw.githubusercontent.com/bioinformatics-centre/kaiju/master/util/kaiju-taxonlistEuk.tsv). Lineage *(Kingdom, Phylum, Class, Order, Family, Genus)* of the TaxIDs are also included as columns. A cutoff (*Default: 0.001%*) may be implemented such that only taxids with percent abundance greater than or equal to the cutoff in at least 1 sample is included. 
2. **Taxonomy ID : Scaled Read Count *(per sample)* table**. Taxonomy IDs are as above in ***1***. Per sample, scaled counts are obtained by dividing read counts for each taxonomy ID by the total number of reads identifed at the species level and multiplying this quotient by 100. Lineage *(Kingdom, Phylum, Class, Order, Family, Genus)* of the TaxIDs are also included as columns.  A cutoff (*Default: 0.001%*) may be implemented such that only taxids with percent abundance greater than or equal to the cutoff in at least 1 sample is included.
3. **Taxonomy ID : Average Scaled Reads *(per group/condition)* table**. From **2**, the average of the scaled read counts matching to a taxonomy ID are obtained from samples belonging to 1 group. Lineage *(Kingdom, Phylum, Class, Order, Family, Genus)* of the TaxIDs are also included as columns.
4. **Species Differential Abundance *(between groups)***. [edgeR](https://bioconductor.org/packages/release/bioc/html/edgeR.html) is used to obtain differentially abundant species. 
5. **Gene Ontologies : %Reads *(per sample or group/condition)* table**. In a sample/group, reads are classified by matches to protein accession numbers. “Proportional” counts are obtained if a read matched to more than 1 protein by dividing 1 read count by the number of proteins it has matched to. Within a group, these counts per protein accession are averaged across samples belonging to that group. These proteins are gathered per taxon of interest and their gene ontology **(GO)** annotations are obtained. Proteins are then binned into their GO annotations by having their (scaled) reads added up under the GO ID. Per namespace ***(Biological Process, Molecular Function, Cellular Component)***, percentage **(%)** of a GO ID is obtained by dividing the (scaled) reads covering a GO ID by the total number of (scaled) reads in that namespace, multiplied by 100. **IMPORTANT:** Only reads at the species level are considered. Scaling is performed by dividing the proportional read count by the total number of reads that matched to a species then multiplied by 100. Only proteins of taxids passing the cutoff are included. 
6. **R Shiny Visualisation Application**. MetaFunc creates an interactive application that allows for exploration of the above results.

### Additional Features

Microbiome studies commonly involve host organisms. This pipeline allows the user to first map their dataset to a host genome. Unmapped reads will be used as input to the default usage of this pipeline. Mapped reads will be used to give the following outputs:

1. **Gene : Read Count *(per sample)* table**. Gene IDs are rows and samples are columns.
2. **Gene : Tags per million *(per sample)* table**. Gene IDs are rows and samples are columns.
3. **Differential Gene Expression Analysis *(between groups)***. ***edgeR*** is used to obtain differentially abundant genes.
4. **Gene Set Enrichment Analysis *(between groups)***. Gene Set enrichment is performed on the genes analyzed in the dgea step (step **3**).


Host-Microbiome relationships can also be obtained using Spearman correlation. Differentially expressed genes (DGEs) and differentially abundant (DA) species are used, and the user can further filter this to only include a certain number (*Default: 100*) of the most significant genes and microbes.  The following outputs are given:

1. **Spearman Correlation Analysis Table**. Correlation table between the DGEs and DAs, with significance levels.
2. **Spearman Correlation Analysis Matrix**. Pairwise rho/correlation values between DGEs and DAs. **NOTE**: all values are included in the matrix, not only the significant correlations.
3. **Clustered Matrix Link**. [clustergrammer](http://amp.pharm.mssm.edu/clustergrammer/) is used to hierarchichally cluster the relationships found in **2**. A link is given to open the results in a webpage. 


## Usage with nr_euk

> **_NOTE:_**  This section instructs how to run the pipeline, in detail. See [Testing](https://gitlab.com/schmeierlab/workflows/metafunc/#testing) section below for instructions on how to perform a test run with the pipeline. This comes with a bash script to install provided databases for the microbiome run, and human databases for a host run.

### 1. Install miniconda and snakemake

Miniconda and Snakemake are necessary to run this workflow. Singularity is also a highly recommended alternative to miniconda.

#### 1.1 Install miniconda

```bash
# LINUX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh

# MacOS:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh
```

#### 1.2 Install snakemake

create a conda environment and install snakemake

```bash
conda create -n snakemake snakemake=5.9.1
```

### 2. Getting necessary databases

Users may either download databases pre-packaged for this workflow (section ***2.1***), or build their own databases (section ***2.2***)

#### 2.1 Download Databases

Pre-built databases are downloadable from [Zenodo](https://zenodo.org). The data is quite big (~56 GB) so download times can vary.

```bash
wget https://zenodo.org/record/5602157/files/nrgo_202001_updated.tar.bz2
wget https://zenodo.org/record/5602157/files/202001_nrgo_md5sums.txt
wget https://zenodo.org/record/5602178/files/kaijudb_nreuk_202001.tar.bz2
wget https://zenodo.org/record/5602178/files/202001_kaijudb_md5sums.txt
```

To check if download was successful:

```bash
md5sum -c 202001_nrgo_md5sums.txt
nrgo_202001_updated.tar.bz2: OK

md5sum -c 202001_kaijudb_md5sums.txt
kaijudb_nreuk_202001.tar.bz2: OK
```

Uncompress the tarballed, bzip'ed-files:

```bash
tar -xvjf kaijudb_nreuk_202001.tar.bz2
tar -xvjf nrgo_202001_updated.tar.bz2
```

#### 2.2 Build Databases

#### 2.2.1 Building Kaiju Databases

One  needs to build an `nr_euk` [Kaiju](Http://Kaiju.Binf.Ku.Dk) database. Create a directory where these database files will be found. Then run the following command in that created directory:

```bash
conda create -n kaiju kaiju=1.7.3
conda activate kaiju
kaiju-makedb -s nr_euk -t 12
# -t refers to the number of cores to be used in creating the database and can be changed depending on user's machine's capacity
```

Do not delete just yet any files created by `kaiju-makedb`, even if they are not
necessary to run `kaiju`. We will use some of those files to build necessary databases for our purposes. Take note of the directory path. This is necessary for section ***3.2 (Adjust config.yaml)***

#### 2.2.2 Building nrgo Databases

Run the [nrgo](https://gitlab.com/schmeierlab/nrgo?nav_source=navbar) workflow to obtain the following databases:

- groups.pickle
- strain2species.pickle
- nrgo.sqlite
- go-basic.obo
- taxon configuration file

Take note of the path to the `results` directory. This is necessary for section ***3.2 (Adjust config.yaml)***

### 3. Create a mapping reference index of the host (optional, e.g. if you do not have a host)

We suggest to first remove reads from the input that belong to the host, however this is optional. 
The workflow can map all reads to a reference genome index.
This index needs to be built before running the workflow.
For example for the human host:

```bash
# download host genome
wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/GRCh38.primary_assembly.genome.fa.gz
gzip -d GRCh38.primary_assembly.genome.fa.gz

# download corresponding gene annotation
wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/gencode.v33.primary_assembly.annotation.gtf.gz
gzip -d gencode.v33.primary_assembly.annotation.gtf.gz

# build STAR index
conda create --yes -n star star=2.7.3a
conda activate star
mkdir genome
STAR --runMode genomeGenerate --runThreadN 12 --genomeDir genome --genomeFastaFiles GRCh38.primary_assembly.genome.fa
```
#### 3.1 Download ‘.gmt’ file for gsea analysis (optional, if gsea is to be carried out)

Currently, we recommend downloading the gmt file from [msigdb v 7.0](https://software.broadinstitute.org/cancer/software/gsea/wiki/index.php/MSigDB_v7.0_Release_Notes).

**IMPORTANT**: Ensure that the gene IDs in your gmt file are of the same format as your gtf annotation file.

### 4. Install and Execute Workflow

#### 4.1 Install workflow

```bash
git clone https://gitlab.com/schmeierlab/workflows/metafunc.git
```

#### 4.2 Adjust config.yaml

Currently it is required to submit config parameters via `--configfile config.yaml`.
Change `config.yaml` to:

1. ``resultdir``: Point to the directory where results of this workflow  will be saved
2. ``samples``: Point to the sample file or keep the default `samples.txt`. See section ***4.3(Sample Sheet Format)*** 

3. ``trimming``: 
    
    a. ``perform``: Set to **True** if trimming by [fastp](https://github.com/OpenGene/fastp) is required
    
    b. ``extra``: Specify any extra parameters for fastp

4. ``mapping``:

    a. ``perform``: Set to **True** if mapping with [STAR](https://github.com/alexdobin/STAR) to a host genome
    
    b. ``index``: Path to host genome index generated with STAR
    
    c. ``strandedness``: Strandedness of RNA-seq data. Can be one of *None*, *yes*, and *reverse*
    
    d. ``star_extra``: Specify any extra parameters for STAR

    e. ``gsea_gmt``: Path to *gmt* file for use in gsea. If empty string *""*, gsea will not be performed

    *if groups are specified (see ***5a*** below), the following options will be used:*
            
    f. ``dgea_mincount``: Minimum count threshold to retain a gene in DGEA. Default is *1*
    
    g. ``cor_pv``: Maximum FDR for a gene or species to be used for correlation analysis. Default is *0.05*
        
    h. ``cor_top``: Use top *n* DEG/DA based on FDR for correlation analysis (e.g. if *n* is set to 100, use top 100 DEG/DA for correlation analysis). Default is *100*

5. ``groups``: 
            
    a. ``use``: Indicate **True** if samples belong to groups that may be compared (e.g. disease vs normal).
    
    b. ``contrasts``: 

            - contrast1-vs-contrast2 (Label referring to group comparisons to be made) 

                - contrast 1 (baseline group in comparisons (e.g. normal))

                - contrast 2 (effect group in comparisons (e.g. with disease))

    c. ``DA_mincount``: Minimum count threshold to retain a species for DA. Default is *1*.

6. ``kaijuDB``: 
      
    a. ``dir``: Point to the correct kaiju database directory. See ***section 2.2.1***
    
    b. ``fmi``: Specify the exact *.fmi* file in the *dir* directory

7. ``kaiju_NRGO``: Point to the correct [nrgo](https://gitlab.com/schmeierlab/metafunc/metafunc-nrgo?nav_source=navbar) results directory. See ***section 2.2.2***
8. ``KaijuRun``:

    a. ``kaijuOptions``: Specify additional (non-default) |kaiju| parameters as string. 

9. ``Filters``: 

    a. ``abundance``: Percent abundance cutoff for a taxid to be included in analysis (i.e. taxid should be at least equal to cutoff in at least 1 of the samples to be analyzed). Default is *0.001%*.

10. ``TaxChoices``: Give a list of all taxon names from [kaiju-taxonlistEuk.tsv](https://raw.githubusercontent.com/bioinformatics-centre/kaiju/master/util/kaiju-taxonlistEuk.tsv) for which the user would like to obtain *Functional/Gene Ontology summaries*. Default is *Viruses*.
    - Taxon 1
    - Taxon 2
    - ...
    - Taxon N

#### 4.3 Sample Sheet Format

The sample sheet should have the following format:

| sample        | file_mate1                 | file_mate2                 | file_singles                               | group               |
| ------------- | -------------------------- | -------------------------- | ------------------------------------------ | ------------------- |
| *Sample Name* | *Paired-end, forward read* | *Paired-end, reverse read* | *Single Read (or singleton from trimming*) | *Condition to test* |
| ...           | ...                        | ...                        | ...                                        | ...                 |
| ...           | ...                        | ...                        | ...                                        | ...                 |

***NOTES: the workflow considers different combinations of input reads. Columns 2 and 3 MUST be filled together. Column 4 can exist on its own or in combination with columns 2 and 3. Either columns 2 and 3, or column 4 is required (or all 3). If column 5 is not specified, a default value of "NoGroup" is given to all samples***

#### 4.4 Execute workflow

The workflow can be executed in different "modes", depending if certain 3rd party software is installed in the system.
In particular ***Singularity*** (virtualisation software) makes the workflow more reproducibile but might not be readily installed in all systems.
In that case use the "conda-only mode".

```bash
### activate the snakemake conda environment
conda activate snakemake

### Do a dryrun of the workflow, show rules, order, and commands
snakemake -np --configfile config.yaml

### Recommended singularity-only mode for reproducibility
# No tool download needed. Singularity container will be pulled and used for the workflow.
# Only works on systems where Singularity has been installed.
snakemake -p --use-singularity --cores 12 --resources const=1 --configfile config.yaml > run.log 2>&1

# If necessary bind more folders for singularity outside of your home.
snakemake -p --use-singularity --cores 12 --singularity-args "--bind /mnt/disk2" --resources const=1 --configfile config.yaml > run.log 2>&1

### Conda-only run
# Will download tools and store them in environment files in the .snakemake/conda dir
snakemake -p --use-conda --cores 12 --resources const=1 --configfile config.yaml > run.log 2>&1
```

## Testing

### Download the workflow

```bash
git clone https://gitlab.com/schmeierlab/workflows/metafunc.git && cd metafunc
```
> **_NOTE:_** Make sure miniconda and snakemake are installed. See [Section 1](https://gitlab.com/schmeierlab/workflows/metafunc/#1-install-miniconda-and-snakemake) of [Usage with nr_euk](https://gitlab.com/schmeierlab/workflows/metafunc/#usage-with-nr_euk) section.

### Download provided databases for microbiome run, and human host genome databases for host run. This also builds indices for human host genome mapping.

```bash
bash -i test_dbs.sh
```

### Run the workflow

We provide some sub-sampled data in the ``.test`` directory to test the workflow. The config-file is setup to work with the test-data, using only ***Viruses*** for microbiome functional output. 

Run the workflow either in conda-only or singularity-only mode:

```bash
# conda-mode
snakemake -p --use-conda --cores 12 --resources const=1 --configfile config.yaml > run.log 2>&1

# singularity-mode
snakemake -p --use-singularity --cores 12 --resources const=1 --configfile config.yaml > run.log 2>&1
```

### MetaFunc Shiny App Instructions

#### Usage Instructions:
The MetaFunc App is designed for interactive display and exploration of host and microbial abundance data, as well as gene ontology (GO) data and microbial taxonomy data generated from the MetaFunc Pipeline.
Full usage instructions with examples are located at: https://metafunc.readthedocs.io/en/latest/rshiny.html
Latest MetaFunc code is available at: https://gitlab.com/schmeierlab/workflows/metafunc/-/tree/master/metafunc-shiny

#### Home Page:

- Select which database to load and press the load database button. Databases are stored in the `/data/` folder of the application and when running MetaFunc are created and placed there automatically after being built by the buildSQLiteDB_sm.R script on results stored in the `/results/` folder of the MetaFunc pipeline.
- The app includes a demo database: `small_demo_dataset_with_groups.db`, this dataset is a small subset of results data from MetaFunc pipeline run on the colorectal cancer RNA-seq data used in the MetaFunc gut microbiome publication.
- In the demo dataset when the `Individual` toggle is selected columns beginning with 'SRR' denote samples from different patients, when the `Grouped` toggle is selected 'coloncancer' and 'normal' columns represent matched sample groupings.
- Full MetaFunc output for the demo dataset's original run available at: https://doi.org/10.1017/gmb.2022.12, and raw sequence reads at: https://www.ncbi.nlm.nih.gov/bioproject/PRJNA413956
- Use the Workflow Diagram below to familiarize yourself with the analysis steps of the MetaFunc Pipeline.


#### Microbiome Tab:

- Explore microbial abundance data (displayed in %) for each sample or group under the 'Abundances' sub-tab.
- Use the 'Gene Ontology' sub-tab to view GO terms by (% reads mapped to GO ID) for each sample or group, with options to filter by Biological Process, Cellular Component, or Molecular Function.
- In the 'GO to TaxIDs' sub-tab, investigate relationships between GO terms and microbial taxa by selecting GO terms in the top table to view the associated taxonomy data in the table below.
- In the 'TaxIDs to GO' sub-tab, similar to 'Go to TaxIDs', but reversed - investigate relationships between microbial taxa and GO terms by selecting microbes in the top table to view the associated GO data in the table below.

#### Host Tab:

- Examine host gene expression abundances TPM (Transcripts-Per-Million) data under the 'Abundances' sub-tab.
- Similar to the Microbiome tab, this tab allows filtering, sorting, and searching of human RNA-seq count data.

#### Individual vs. Grouped Data:

- The MetaFunc App can show both individual and grouped data in the Abundances, Gene Ontology, GO to TaxIDs, and TaxIDs to GO tabs. If grouped analysis was specified in the MetaFunc pipeline, switch the toggle to grouped to view your data.

#### GO and TaxID Data Inclusivity:

- In the GO to TaxIDs and TaxIDs to GO tabs, the user can toggle 'In Any' vs 'Only in All' to view associated terms (in the bottom table) that are either in any of the highlighted rows in the top table, or only present in all of the rows.

#### Exporting Data:

- After filtering or selecting data of interest, use the download options provided to export the tables in .csv or .excel formats for further analysis.

### Author Information:
If you use the MetaFunc pipeline or MetaFunc App in your analysis, please cite our [paper](https://doi.org/10.1017/gmb.2022.12):

`Sulit AK, Kolisnik T, Frizelle FA, Purcell R, Schmeier S. MetaFunc: taxonomic and functional analyses of high throughput sequencing for microbiomes. Gut Microbiome. 2023;4:e4. doi:10.1017/gmb.2022.12`
